DL_MONTE-2.0 test case - FED for two LJ particles

Approximate reproduction of Lennard-Jones potential by potential of mean-force (PMF/POMF) between two particles,
using free energy difference Wang-Landau and Expanded Ensemble methods in NVT simulation.


AUTHOR

Andrey V. Brukhno


PHILOSOPHY

Evaluating free energy difference(s) (FED) in Monte Carlo simulation is a powerful advanced simulation approach
that allows for obtaining crucial information about both the underlying free energy landscape(s) and thermodynamic 
behaviour(s) of the system under study. One particularly interesting FED profile is the Potential of Mean-Force (PMF 
or POMF), W(r) = -kT ln{g(r)}, where g(r) is a specifically renormalised pair correlation function, the so-called 
radial distribution function (RDF). Both PMF and RDF can be defined for a pair of interacting particles or groups 
thereof (in the latter case separation, r, is defined for the corresponding centers of mass, COM:s). 
Clearly, since W(r) tends to zero as the separation increases infinitely, PMF is essentially the work done upon 
bringing the interacting pair at a given finite separation r. 

For a complex system obtaining g(r), and generally W(r), in simulation is often hindered by poor sampling of regions 
in the configuration space that are high in energy and/or low in entropy (i.e. rarely sampled substates). FED MC schemes 
help to resolve, or at least relieve, this problem by introducing a "smart bias" in configuration or thermodynamic state 
sampling so that all relevant substates are sampled more or less uniformly. To this end, the perfect biasing potential 
along r would be -W(r), if it was known in advance (which is generally not the case). Therefore, normally a FED/PMF 
evaluation is started with some initial guess for the biasing potential (often merely setting it to zero in the entire 
reaction-coordinate range), which is then refined in the course of simulation by using some sort of automated feedback-
update scheme.


TEST DESCRIPTION

This test set is to validate the FED routines implemented in DL_MONTE-2.0 by evaluating W(r) for two LJ particles in vacuum. 
That is, the setup with two particles in sufficiently large simulation box approximates the conditions of infinite dilution, 
under which W(r) -> u(r) as rho -> 0, allowing for numerically reproducing the underlying VdW (LJ) potential.

The general simulation setup is as follows:

Two structureless/point particles interacting via truncated-and-shifted Lennard-Jones potential,

u(r) = 4 epsilon (sigma/r)^6 ( (sigma/r)^6 - 1.0 ) [-u(r_cut)], for r < r_cut (0 for r >= r_cut).

epsilon = 1.0 (reduced units; 1.0 K at T = 1.0 K, see FIELD file)
sigma   = 3.0  Angstrom
r_cut   = 12.0 Angstrom = 4 sigma (cutoff distance)
cubic V = L^3 
where L = 60.0 Angstrom = 20 sigma

Due to different arrangements for this system possible in FIELD, CONFIG and CONTROL files, 
a number of simulation setups are present in the following directories:

LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/ - 1 molecule consisting of 2 non-bonded atoms, atom moves applied
LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/ - 2 molecules of 1 atom each, atom moves applied
LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/ - 2 molecules of 1 atom each, molecule moves applied
LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_both-moves-VdWdir/ - 2 molecules of 1 atom each, atom and molecule moves applied

Every directory contains three input files: FIELD, CONFIG, CONTROL;
and two subdirectories 'short' and 'long' containing the benchmark output files, 
corresponding to FED-MC runs of 2 and 16 million MC cycles, respectively.

In all the above cases the benchmark results were produced with DL_MONTE-2.0 executable compiled using 
vdw_direct_module.f90 which implements direct calculation of VdW (LJ) interactions, as opposed to using
tabulated iterpolated potential representation. For the latter case an extra benchamrk is provided in directory:

LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWint/

Additionally, a variation of the FED procedure with periodic intermediate smoothing (3 cycles of 3-point running average) 
of the FED profile upon refining feedback updates can be benchmarked against the results in

LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir-smooth/

PERFORMING TESTS

Test runs can be carried out manually on the command line from within each directory.
See the provided CONTROL files for the default length of test runs - either 2 or 16 million MC cycles;
and amend 'steps <frequency>' directive, if necessary.

Three short test cases prepared for autoimatic running are specified in Makefile found in the (outer) directory 'tests':

fed_lj-one_mol_atms-VdWdir: fed_lj test 1 in LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/
fed_lj-two_mol_atms-VdWdir: fed_lj test 2 in LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/
fed_lj-two_mol_mols-VdWdir: fed_lj test 3 in LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/

EXAMPLES

In order to run the first test use the following command (from within 'tests' directory):

make fed_lj-one_mol_atms-VdWdir

In order to perform this test along with other FED tests use the following command:

make fed_lj-short

In order to run all short tests available:

make short

