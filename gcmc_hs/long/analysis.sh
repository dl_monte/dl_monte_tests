# Author: Tom L Underwood

# This script does some post-processing on the DL_MONTE output files to
# verify that the simulation gives the correct result (see 'description.txt' in
# the above directory), namely an average of 512 atoms in the unit cell.

# Firstly we make a file 'N_vs_t.dat' which contains the number of particles
# in the unit cell (denoted N) every 100 steps. We extract this information from
# 'PTFILE.000' using awk. Note that every 100 steps 11 lines of data are
# output to 'PTFILE.000', and that line 6 of the 11 contains the number of
# particles in the unit cell. We thus extract such lines:

awk '{if(NR%11==6){print $1}}' PTFILE.000 > N_vs_t.dat

# We now use the data in 'N_vs_t.dat' to calculate the mean N, and an
# associated uncertainty. We use block analysis: the data is split into
# blocks corresponding to 100000 steps - the equilibration period is
# ignored. The average for each block is computed. The 'average of the
# averages' is equivalent to the average over the whole simulation.
# An uncertainty in this is given by the standard error in this 'average
# of the averages'. Again, we use awk. Note that each line in 'N_vs_t.dat'
# corresponds to 100 steps, and we assume that the equilibration time is
# 10000. The result should be 512, taking into account the uncertainty.

awk '{ 
         if(NR > 100){ 
             sum = sum + $1; 
             counts = counts + 1; 
             if(counts % 1000 == 0){
                 mean = sum / counts; 
                 sum = 0; 
                 counts = 0; 
                 Sum = Sum + mean; 
                 Sum2 = Sum2 + mean * mean; 
                 Counts = Counts + 1;
             }
         }
     } 
     END{
         Mean = Sum / Counts;
         Mean2 = Sum2 / Counts;
         Var = Sum2 / Counts - Mean * Mean;
         Stderr =  sqrt(Var) / sqrt(Counts);
         print "Result of block analysis: blocks, mean, stderror = ", Counts, Mean, Stderr; 
     }' \
 N_vs_t.dat

