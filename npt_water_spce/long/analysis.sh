# Author: Tom L Underwood

# Creates a file 'v_vs_t.dat' containing a list of the volume per molecule every 1000 moves,
# then performs block averaging on this file to determine the average volume per molecule
# and its uncertainty.

grep volume: YAMLDATA.000 | awk '{print $2/512}' > v_vs_t.dat
#awk '{if(NR%11==4){print $1/512}}' PTFILE.000 > v_vs_t.dat

# Data file has 180,000 points. Use block size 10,000 and equilibration time 10,000
awk '{ 
         if(NR > 10000){ 
             sum = sum + $1; 
             counts = counts + 1; 
             if(counts % 10000 == 0){
                 mean = sum / counts; 
                 sum = 0; 
                 counts = 0; 
                 Sum = Sum + mean; 
                 Sum2 = Sum2 + mean * mean; 
                 Counts = Counts + 1;
             }
         }
     } 
     END{
         Mean = Sum / Counts;
         Var = Sum2 / Counts - Mean * Mean;
         Stdev = sqrt(Var*Counts/(Counts-1))
         Stderr =  Stdev / sqrt(Counts);
         print "Result of block analysis: blocks, mean, stderror = ", Counts, Mean, Stderr; 
     }' \
 v_vs_t.dat

