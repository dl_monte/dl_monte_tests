
.PHONY: TEST-CASE
TEST-CASE:
	@echo ;\
	echo "********************************************************************"; \
	echo " TEST-CASE "; \
	echo "********************************************************************"; \
	echo " in PLACE/CASE/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	cd PLACE/CASE/; \
	if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'TEST-CASE'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	fi ; \
	echo ;\
	echo "Returning"; \
	cd - ; \
	echo ;\
	echo ;\

.PHONY: clean_TEST-CASE
clean_TEST-CASE:
	@echo ; \
	echo "WARNING: removing the data for test 'TEST-CASE'"; \
	echo ; \
	echo "Entering directory 'PLACE/CASE/'"; \
	cd PLACE/CASE/; \
	echo ; \
	if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      rm -fv $$file ; \
	    fi ; \
	  done ; \
	else \
	    echo "Nothing to clean - skipping"; \
	fi; \
	if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	if [ -f FIELD ];      then rm -fv FIELD; fi; \
	if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	echo ; \
	echo "Returning"; \
	cd -; \
	echo ; \

.PHONY: clear_TEST-CASE
clear_TEST-CASE: clean_TEST-CASE
	@if ls PLACE/CASE/* 2>/dev/null 1>/dev/null; then \
	  if [ -e PLACE/TEST-CASE.tgz ]; then \
	    echo "Package 'TEST-CASE.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'PLACE/CASE/'"; \
	    cd PLACE/CASE/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	  else \
	    echo "Package 'TEST-CASE.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'TEST-CASE' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_TEST-CASE
update_TEST-CASE:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'TEST-CASE'"; \
	echo ; \
	echo "Entering directory 'PLACE/CASE/'"; \
	cd PLACE/CASE/; \
	echo ; \
	if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	fi ; \
	mv -fv CONTROL input/; \
	mv -fv CONFIG* input/; \
	mv -fv FIELD   input/; \
	if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	for file in *.[0-9][0-9][0-9]* ; do \
	  if [ -f "$$file" ]; then \
	    mv -fv $$file benchmark/; \
	  fi ; \
	done ; \
	echo ; \
	echo "Returning"; \
	cd -; \
	echo ; \

.PHONY: pack_TEST-CASE
pack_TEST-CASE:
	@echo ; \
	echo "Packing benchmark 'TEST-CASE'" ; \
	echo ; \
	echo "Entering directory 'PLACE/'"; \
	cd PLACE/ ; \
	echo ; \
	if ls ./CASE/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf TEST-CASE.tgz ./CASE/* --remove-files ; \
	else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	fi; \
	echo ; \
	echo "Returning"; \
	cd -; \
	echo ; \

.PHONY: unpack_TEST-CASE
unpack_TEST-CASE:
	@echo ; \
	echo "Unpacking benchmark 'TEST-CASE'" ; \
	echo ; \
	echo "Entering directory 'PLACE/'"; \
	cd PLACE/ ; \
	echo ; \
	if [ -e TEST-CASE.tgz ]; then \
	    tar -xvf TEST-CASE.tgz ; \
	else \
	    echo "Package 'TEST-CASE.tgz' not found - skipping"; \
	fi; \
	echo ; \
	echo "Returning"; \
	cd -; \
	echo ; \

