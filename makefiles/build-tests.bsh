#!/bin/bash

##############################################
#                                            #
# Simple workaround for building DL_MONTE-2  #
# new tests & including them in Makefile(s)  #
#                                            #
# Author: Andrey Brukhno (c) August 2016     #
#                                            #
##############################################

if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
cat <<EOF

////////////////////////////////////////////////////////////////////////
//                                                                    //
// DL_MONTE-2 test(s) building script, version 2.02 (alpha)           //
// Author - Dr Andrey Brukhno (DL/STFC), August 2016                  //
//                                                                    //
////////////////////////////////////////////////////////////////////////

 - Generates multiple test cases (benchmarks) based on two arguments -

===========
Main usage: 
===========

./${0##*/} <pattern> <subdir>

<pattern> = pattern for directories contatining the input (& possibly the output)
<subdir>  = target subdirectory (common target suffix) for 'input' & 'benchmark'

Targets will be appended to Makefile: '*<pattern>*-subdir'

At least three DL_MONTE-2 input files are to be provided: CONTROL, CONFIG, FIELD [& FEDDAT.###]

NOTE1: <pattern> may include a few (sub)directory levels (not recommended!)

NOTE2: DL_MONTE benchmark executable path/name is set within the script!
       Check & amend variable 'EXE' if needed!

==========
Otherwise:
==========

./${0##*/} -h
./${0##*/} --help

- output this help message

EOF
   exit 0
fi

echo
echo "////////////////////////////////////////////////////////////"
echo "//                                                        //"
echo "// DL_MONTE-2 tests building script, version 2.02 (alpha) //"
echo "// Author - Dr Andrey Brukhno (DL/STFC), August 2016      //"
echo "//                                                        //"
echo "////////////////////////////////////////////////////////////"
echo

echo "Tests will be attempted in the following directories ('*$1*'):"
echo

find ./ -maxdepth 1 -type d -name "*$1*"

echo
echo
echo "Proceed? [Yes/No]"
read

if [[ $REPLY == "y" || $REPLY == "yes" || $REPLY == "Y" || $REPLY == "YES" || $REPLY == "Yes" ]]
then

   echo
   echo "Operation confirmed"
   echo

elif [[ $REPLY == "n" || $REPLY == "no" || $REPLY == "N" || $REPLY == "NO" || $REPLY == "No" ]]
then
   echo
   echo "Operation cancelled!"
   echo
   exit 0
else
   echo
   echo "Need to know: [y]es or [n]o"
   echo
   exit 0
fi

for dir in $(find ./ -maxdepth 1 -type d -name "*$1*") ; do 

   if [ -d ${dir} ]; then

      if ls ${dir}/$2/* 2>/dev/null 1>/dev/null; then

         echo "Target directory '${dir}/$2/' is not empty - skipping..."
         echo

      else

         echo "Found test directory '${dir}/' - building benchmark..."
         echo

         ./build-test: "${dir}" "$2"
      fi

   else

      echo "Test directory '${dir}/' not found - skipping..."
      echo

   fi

done

exit 0

