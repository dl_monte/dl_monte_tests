# Author: Tom L Underwood

# This script does some post-processing on the DL_MONTE output files to
# verify that the simulation gives the correct results (see 'description.txt' in
# the above directory), namely:
# 1) an average of 320 particles in the unit cell;
# 2) a histogram in the density, or equivalently the number of particles (the
#    volume is fixed) which is 'M-shaped'.

# Firstly we make a file 'N_vs_t.dat' which contains the number of particles
# in the unit cell (denoted N) every 1000 steps. We extract this information from
# 'PTFILE.000' using awk. Note that every 1000 steps 11 lines of data are
# output to 'PTFILE.000', and that line 6 of the 11 contains the number of
# particles in the unit cell. We thus extract such lines:

awk '{if(NR%11==6){print $1}}' PTFILE.000 > N_vs_t.dat

# Making a histogram of the data in 'N_vs_t.dat' should yield an M-shaped
# curve. It won't be perfect: an even longer simulation is required to gather
# enough statistics to yield a 'perfect' histogram. However the histogram
# obtained from this simulation should look reasonably M-shaped.

# We now use the data in 'N_vs_t.dat' to calculate the mean N, and an
# associated uncertainty. We use block analysis: the data is split into
# blocks corresponding to 1000000 steps - the equilibration period is
# ignored. The average for each block is computed. The 'average of the
# averages' is equivalent to the average over the whole simulation.
# An uncertainty in this is given by the standard error in this 'average
# of the averages'. Again, we use awk. Note that each line in 'N_vs_t.dat'
# corresponds to 1000 steps, and we assume that the equilibration time is
# 100000. The result should be 320, taking into account the uncertainty.

awk '{ 
         if(NR > 100){ 
             sum = sum + $1; 
             counts = counts + 1; 
             if(counts % 1000 == 0){
                 mean = sum / counts; 
                 sum = 0; 
                 counts = 0; 
                 Sum = Sum + mean; 
                 Sum2 = Sum2 + mean * mean; 
                 Counts = Counts + 1;
             }
         }
     } 
     END{
         Mean = Sum / Counts;
         Var = Sum2 / Counts - Mean * Mean;
         Stdev = sqrt(Var*Counts/(Counts-1))
         Stderr =  Stdev / sqrt(Counts);
         print "Result of block analysis: blocks, mean, stderror = ", Counts, Mean, Stderr; 
     }' \
 N_vs_t.dat

