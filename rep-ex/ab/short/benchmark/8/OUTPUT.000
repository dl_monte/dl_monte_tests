
                   ===================================================================
                   * * *                                                         * * *
                   * * *            D L - M O N T E - v e r. 2.0 (3)             * * *
                   * * *                                                         * * *
                   * * *           STFC / CCP5 - Daresbury Laboratory            * * *
                   * * *                 program library  package                * * *
                   * * *                                                         * * *
                   * * *     general purpose Monte Carlo simulation software     * * *
                   * * *                                                         * * *
                   * * *     version:  2.03 (alpha) / February 2017              * * *
                   * * *     author:   J. A. Purton (2007-2015)                  * * *
                   * * *     -----------------------------------------------     * * *
                   * * *     contributors:                                       * * *
                   * * *     -------------                                       * * *
                   * * *     A. V. Brukhno   - Free Energy Diff, planar pore     * * *
                   * * *     T. L. Underwood - Lattice/Phase-Switch MC methods   * * *
                   * * *     R. J. Grant     - extra potentials, optimizations   * * *
                   * * *     -----------------------------------------------     * * *
                   * * *                                                         * * *
                   * * *     current execution:                                  * * *
                   * * *     ==================                                  * * *
                   * * *     number of nodes/CPUs/cores:          8              * * *
                   * * *     number of threads per node:          1              * * *
                   * * *     number of threads employed:          1              * * *
                   * * *                                                         * * *
                   * * * ======================================================= * * *
                   * * * Publishing data obtained with DL_MONTE? - please cite:  * * *
                   * * * ------------------------------------------------------- * * *
                   * * *        Purton, J. A., Crabtree J. C. and Parker, S. C., * * *
                   * * *        Molecular Simulation 2013, 39 (14-15), 1240-1252 * * *
                   * * *                `DL_MONTE: A general purpose program for * * *
                   * * *                         parallel Monte Carlo simulation`* * *
                   * * * ------------------------------------------------------- * * *
                   * * * if using Lattice-Switch method, please also cite:       * * *
                   * * * ------------------------------------------------------- * * *
                   * * *              Bruce A.D., Wilding N.B. and G.J. Ackland, * * *
                   * * *                         Phys. Rev. Lett. 1997, 79, 3002 * * *
                   * * *    `Free Energy of Crystalline Solids: A Lattice-Switch * * *
                   * * *                                      Monte Carlo Method`* * *
                   * * * ------------------------------------------------------- * * *

                   * * * if using Planar Pore (slit) geometry, please also cite: * * *
                   * * * ------------------------------------------------------- * * *
                   * * *    Brukhno A. V., Akinshina A., Coldrick Z., Nelson A., * * *
                   * * *                                             and Auer S. * * *
                   * * *                              Soft Matter. 2011, 7, 1006 * * *
                   * * *               `Phase phenomena in supported lipid films * * *
                   * * *                        under varying electric potential`* * *
                   ===================================================================

 ----------------------------------------------------------------------------------------------------
 FIELD title  (100 symbols max) :
 ----------------------------------------------------------------------------------------------------
'lennard-jones - 2 particles'


 ----------------------------------------------------------------------------------------------------
 atom data :
 ----------------------------------------------------------------------------------------------------
 element           mass             charge
  Lj               1.0000         0.0000


 ----------------------------------------------------------------------------------------------------
 molecule data :
 ----------------------------------------------------------------------------------------------------
 molecule name
 test    


 ----------------------------------------------------------------------------------------------------
 nonbonded (vdw) potentials :
 ----------------------------------------------------------------------------------------------------
 Lj      Lj         lj                1.000000       3.000000       0.000000       0.000000       0.000000

 VdW repulsion capped at D_core(  1) =      1.26981 by E_cap = 100000.00000


 ==============================================
 Initial setup for energy loop(s) distribution:
 ----------------------------------------------
  - first molecule  in loop(s) = idnode + 1  =            1
  - molecule stride in loop(s) = no of nodes =            8
 ==============================================



 --------------------------------------------------
 configuration box No.          1
 --------------------------------------------------

 the maximum no of atoms in this cell is        2

          lattice vectors
    60.00000     0.00000     0.00000
     0.00000    60.00000     0.00000
     0.00000     0.00000    60.00000

          Using: is_simple_orthogonal:    T


          Using: is_orthogonal:    T


          reciprocal lattice vectors
     0.01667     0.00000     0.00000
     0.00000     0.01667     0.00000
     0.00000     0.00000     0.01667


 --------------------------------------------------
 configuration sample (up to 10 atoms):
 --------------------------------------------------

 molecule test    
 Lj        c        1   type   1
      0.0000000      0.0000000      0.0000000     0
 Lj        c        2   type   1
      3.3673861      0.0000000      0.0000000     0

 setup_workgroups :     0    0   (master : T)

 nodes being used for workgroup      0

 workgroup rank      0 node          0         0         3         4
 workgroup rank      1 node          1         0         3         4

 rep exchange : temperature for this workgroup (K)     =    1.00000

 ==============================================
 Working setup for energy loop(s) distribution:
 ----------------------------------------------
  - first molecule  in loop(s) =            1
  - molecule stride in loop(s) =            2
 ==============================================



 ==================================================
                simulation parameters 
 ==================================================

 cell (box) type [ < 0 -> 2D-slit; else 3D-bulk ] =          0 (bulk: XYZ-PBC)

 - no charges, no electrostatics required; pure short-range/VdW/metal(?) force-field

 global cutoff for interactions (Angstroms)       =   12.00000

 cutoff for vdw interactions (Angstroms)          =   12.00000

 Verlet neighbour lists not used                  =   F  F

 system temperature (Kelvin)                      =    1.00000

 constant volume simulation (NVT), sample volume  =   F

 system pressure (katms)                          =    0.00100

 total number of MC steps (cycles)                =       1000

 number of equilibration steps                    =          0

 number of pre-heating steps                      =         -1

 maximum no of non-bonded neighbours              =          0

 maximum no of three body neighbours              =          6

 three body list updated every (cycles)           =   20000000

 stack-size for block averaging                   =        100

 statistics data saved every (cycles) -> PTFILE*  =       1000

 energy frames printed every (cycles) -> OUTPUT*  =       1000

 energy checks printed every (cycles) -> OUTPUT*  =       1000

 configurations stored every (cycles) -> REVCON*  =       1000

 trajectories archived every (cycles) -> ARCHIVE* =       1000 (DL_MONTE style)

 REVCON configuration data in DL_MONTE style (extended native)

 ARCHIVE* trajectory data in DL_MONTE style (all)

 electrostatics is not used (no Coulomb interactions)

 g-vectors to be distributed over nodes

 atom types to be moved
     Lj       c

 initial maximum for atom displacement            =   10.00000

 displacement distance update  (cycles)           = 1000000000

 acceptance ratio for atom displacement           =    0.37000

 atoms moved with frequency                       =        100

 tolerance for rejction (energy units)            =  100.00000

 replica exchange monte carlo will be employed

 the number of replicas                           =      4

 the temperature increment (K)                    =   -0.20000

 frequency of attempted exchange (iterations)     =     10

 random number seeds (i,j,k,l) =     12    34    56    78 (for workgroup    0)

 total job time and close time =      1000000.00         200.00

 rotation of molecules will use Euler method

 number of moving particles for box    1         2

 energy unit conversion factor (user -> internal) =        0.8314511150

 beta (deca-J/mol) & Bjerrum length (Angstroem)   =        1.2027165301   167100.0026261316

 ==================================================



 --------------------------------------------------
                  initial energies 
 --------------------------------------------------

 break down of energies for box:   1

 total energy                       -0.1000000000E+01
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb                  0.0000000000E+00
 external mfa coulomb                0.0000000000E+00
 nonbonded two body (vdw)           -0.1000000000E+01
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 bonded four body (angle)            0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00
 total virial                        0.0000000000E+00
 volume                              0.2160000000E+06

 components of energies by  molecule: test    

 total energy                       -0.1000000000E+01
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb - other types    0.0000000000E+00
 real space coulomb - same type      0.0000000000E+00
 real space coulomb - self energy    0.0000000000E+00
 real space coulomb - corection     -0.0000000000E+00
 vdw - other types mol               0.0000000000E+00
 vdw - same type mol                -0.1000000000E+01
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00

 RE set-up on node            0  - initialised (4) in workgroup            0



 elapsed time to start of mc (seconds)     0.0010





 cutoffs for lattice summation in box:  1

 eta                                 :    0.26506
 real space cutoff A                 :   12.00000
 reciprocal space cutoff 1/A         :    0.26667
 kmax1, kmax2, kmax3                 :  16 16 16


 the total number of reciprocal lattice vectors:          0


 the number of reciprocal lattice vectors on node          1         0
 the number of reciprocal lattice vectors on node          2         0


 Iteration       1000 - elapsed time (seconds)     0.0330


      step      en-total            h-total             coul-rcp            coul-real
      step      en-vdw              en-three            en-pair             en-angle 
      step      en-four             en-many                                 volume   
      step      cell-a              cell-b              cell-c
      step      alpha               beta                gamma 
      r-av      en-total            h-total             coul-rcp            coul-real
      r-av      en-vdw              en-three            en-pair             en-angle 
      r-av      en-many             vir-tot             volume              pressure 
      r-av      cell-a              cell-b              cell-c
      r-av      alpha               beta                gamma 
 ----------------------------------------------------------------------------------------------------
      1000     0.0000000000E+00    0.1585200678E+04    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.6000000000E+02    0.6000000000E+02    0.6000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.4292506481E-02    0.1585196385E+04    0.0000000000E+00    0.0000000000E+00
              -0.4292506481E-02    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.6000000000E+02    0.6000000000E+02    0.6000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 Lj       c          2.0000          2.0000

 test              1.0000          1.0000
 ----------------------------------------------------------------------------------------------------

 Workgroup    0, box    1 check: U_recalc - U_accum =  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00 (internal, kT, kT/atom, dU/U)

 ----------------------------------------------------------------------------------------------------



 ----------------------------------------------------------------------------
                          averages and fluctuations


 ----------------------------------------------------------------------------
 ----------------------------------------------------------------------------------------------------
      avrg      en-total            h-total             coul-rcp            coul-real
      avrg      en-vdw              en-three            en-pair             en-angle 
      avrg      en-many                                 vir-tot             volume   
      avrg      pressure 
      flct      en-total            h-total             coul-rcp            coul-real
      flct      en-vdw              en-three            en-pair             en-angle 
      flct      en-many                                 vir-tot             volume   
      flct      pressure 
 ----------------------------------------------------------------------------------------------------
      1000    -0.1679425098E-02    0.1570941192E+04    0.0000000000E+00    0.0000000000E+00
              -0.1679425098E-02    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.2160000000E+06
               0.2592709190E-01    0.1496131898E+03    0.0000000000E+00    0.0000000000E+00
               0.2592709190E-01    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00



 average cell parameters and fluctuations

          a     =   60.00000   0.00000
          b     =   60.00000   0.00000
          c     =   60.00000   0.00000
          alpha =   90.00000   0.00000
          beta  =   90.00000   0.00000
          gamma =   90.00000   0.00000
    
 Lj       c          2.0000          0.0000

 test              1.0000          0.0000


 ----------------------------------------------------------------------------
                          final energies 


 ----------------------------------------------------------------------------

 break down of energies for box:   1

 total energy                        0.0000000000E+00
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb                  0.0000000000E+00
 external mfa coulomb                0.0000000000E+00
 nonbonded two body (vdw)            0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 bonded four body (angle)            0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00
 total virial                        0.0000000000E+00
 volume                              0.2160000000E+06

 components of energies by  molecule: test    

 total energy                        0.0000000000E+00
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb - other types    0.0000000000E+00
 real space coulomb - same type      0.0000000000E+00
 real space coulomb - self energy    0.0000000000E+00
 real space coulomb - corection     -0.0000000000E+00
 vdw - other types mol               0.0000000000E+00
 vdw - same type mol                 0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00


 ----------------------------------------------------------------------------
                          processing data 


 ----------------------------------------------------------------------------

 total no of atom moves         :      1000
 successful no of atom moves    :       998      0.99800000

 displacement (Angstroms) for Lj       c :  10.0000

 attempted replica exchange moves   =         52
 accepted swaps in temperature down =          0
 accepted swaps in temperature up   =         52
 rejected swaps by master process   =          0

 attempted replica exchange moves   (tot) =        152
 accepted swaps in temperature down (tot) =        152
 accepted swaps in temperature up   (tot) =        152
 rejected swaps by master processes (tot) =          0

 ==============================================
  Stats for MPI calls & their time footprints :
 ----------------------------------------------
  - world BCAST of integer vectors No & time =          100   1.0180473327636719E-004   1.0180473327636718E-006
  - world SUMMS of integer scalars No & time =         1002   6.8602561950683594E-003   6.8465630689304980E-006
  - world SUMMS of integer vectors No & time =          406   4.5857429504394531E-003   1.1294933375466634E-005
  - world SUMMS of real_wp vectors No & time =          101   4.4775009155273438E-004   4.4331692232943999E-006
  - world SUMMS of logical scalars No & time =           53   1.8763542175292969E-004   3.5402909764703715E-006
  - group SUMMS of integer vectors No & time =           55   3.5214424133300781E-004   6.4026225696910513E-006
  - group SUMMS of real_wp vectors No & time =         2385   3.9517879486083984E-003   1.6569341503599155E-006
  - blocked SENDS of real_wp vectors No & time =           52   3.4809112548828125E-005   6.6940601055438697E-007
  - blocked RECVS of real_wp vectors No & time =           52   8.1777572631835938E-005   1.5726456275353064E-006
 ----------------------------------------------
  total number of MPI calls & total MPI time =         4206   1.6603708267211914E-002  (MPI_WAIT time:    0.0000000000000000      )
 ==============================================


 total elapsed time (seconds)          0.0340
 normal exit 
