# Author: Tom L Underwood

# Uses block averaging to calculate the mean and standard error of the energy per atom
# from the YAMLDATA.000 file.

grep energy: YAMLDATA.000 | awk '{print $2/216}' | \
awk '{ 
         equiltime = 0
         blocksize = 40                     
         if(NR > equiltime){ 
             sum = sum + $1; 
             counts = counts + 1; 
             if(counts % blocksize == 0){
                 mean = sum / counts; 
                 sum = 0; 
                 counts = 0; 
                 Sum = Sum + mean; 
                 Sum2 = Sum2 + mean * mean; 
                 Counts = Counts + 1;
             }
         }
     } 
     END{
         Mean = Sum / Counts;
         Var = Sum2 / Counts - Mean * Mean;
         Stdev = sqrt(Var*Counts/(Counts-1))
         Stderr =  Stdev / sqrt(Counts);
         print "Result of block analysis: blocks, mean, stderror = ", Counts, Mean, Stderr; 
     }'

