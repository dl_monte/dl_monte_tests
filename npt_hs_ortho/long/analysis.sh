# Author: Tom L Underwood

# This script does some post-processing on the DL_MONTE output files to
# verify that the simulation gives the correct result, namely, a density of 0.3.
# The volume vs. simulation time is extracted from the PTFILE.000, from which the
# density is determined. Blocak averaging is used to determine the density and its
# uncertainty: the simulation time is split into 4 blocks.

# Extract the volumes from PTFILE.000
awk 'NR%11==4 {print $1}' PTFILE.000 > voldat

# 'voldat' contains just over 80000 lines, each containing the volume of the system at
# regular intervals. Below we extract the average of 4 blocks of 20000 lines in 'voldat'
# using 'awk', and store the values for each block in a file 'analysis.tmp' 
rm -f analysis.tmp
head -20000 voldat | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' > analysis.tmp
head -40000 voldat | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' >> analysis.tmp
head -60000 voldat | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' >> analysis.tmp
head -80000 voldat | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' >> analysis.tmp

# Here we calculate the mean of the 4 blocks, as well as the standard error in the mean, and
# output the mean and standard error
awk '{sum2=sum2+$1*$1; sum=sum+$1} END{mean=sum/NR; stdev=sqrt((sum2/NR-mean*mean)*(NR/(NR-1))); print "mean density, uncertainty = ",mean,stdev/sqrt(4)}' analysis.tmp

# Clean up
rm -f analysis.tmp

