for d in replica_*; do 
    awk 'BEGIN{hist1=0; hist2=0; counts1=0; counts2=0}; $1>21600000 {if($2==1){counts1=counts1+exp($5); hist1=hist1+1}; if($2==2){counts2=counts2+exp($5); hist2=hist2+1}} END{print hist1,hist2,counts1,counts2,-0.1*log(counts1/counts2)/216}' $d/PSDATA.000; 
done \
| awk '{sum=sum+$5; sum2=sum2+$5*$5; count=count+1} END{mean=sum/count; var=(sum2/count-mean*mean)*count/(count-1); sterr=sqrt(var)/sqrt(count); print count,mean,sterr}'
