#!/bin/bash
#
# Author: Tom Underwood (c) 2019
#
# This script runs the test suite, and searches the output for signs that any tests 
# were failed. The script creates a 'log' file which contains the output from all 
# tests. If any tests were failed then a message is output to stdout saying as such, 
# and the exit status of the script is 1. If all tests are deemed to have passed, 
# then a message is output to stdout, and the exit status of the script is 0.
#
# Note that this script assumes that the test environment has been set up directory,
# i.e. that the variable 'EXE' in Makefile has been correctly set and that the tests
# have already been unpacked with 'make unpack'
#

# Delete the existing 'log' file
if [ -e "log" ]; then rm -f log; fi

# Run the tests
make &> log || exit 1

# Check for failure due to tests not being extracted
if grep -q 'No such file or directory' log; then echo "*** Detected error. Forgot to use 'make unpack'? ***"; exit 1; fi

# Check for any failed tests (note that grep returns exit status 0 if there is a match
# with the string, 1 if there is no match, and 2 if there is an error).
if grep -q 'TEST FAILED' log; then echo "*** Detected one or more TEST FAILED! ***"; exit 1; fi
if grep -q 'TEST SKIPPED' log; then echo "*** Detected one or more TEST SKIPPED! ***"; exit 1; fi

# The following line catches if grep returns error status 2 - not caught by the above
grep -q '' log || exit 1

echo 'No test failures detected'




