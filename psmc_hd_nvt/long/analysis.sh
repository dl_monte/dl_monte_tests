
rm -f replica_analysis.dat replica_analysis.awk.tmp

cat > replica_analysis.awk.tmp << _EOF_
BEGIN{
    # The number of phase 1 data points
    hist1=0 
    # The number of phase 2 data points
    hist2=0 
    # The corrected count for phase 1 (after unfolding the bias)
    counts1=0 
    # The corrected count for phase 2 (after unfolding the bias)
    counts2=0
}
# Ignore first 30000 data points (equilibration period)
NR>30000 {
    # Token 2 in PSDATA.000 is the phase number (1 or 2); token 5 is the bias
    if(\$2==1){
        counts1=counts1+exp(\$5)
        hist1=hist1+1
    }
    if(\$2==2){
        counts2=counts2+exp(\$5)
        hist2=hist2+1
    }
} 
END{
    # Output the counts for each phase, the corrected counts, and the free energy difference
    # ( \Delta F/(Nk_BT) )
    # But only output if enough data points have been sampled in each phase - between 30% and
    # 70% in phase 1 
    if( hist1/(hist1+hist2)>0.05 && hist1/(hist1+hist2)<0.95 ){
        print hist1, hist2, counts1, counts2, -log(counts1/counts2)/864
    }
}
_EOF_

for d in replica_{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}; do 
    
    echo "Analysing $d..."
    awk -f replica_analysis.awk.tmp $d/PSDATA.000 >> replica_analysis.dat 2>/dev/null

done 

# Calculate the mean and standard error of the 5th token (the free energy difference) in 
# 'replica_analysis.dat'
awk '{sum=sum+$5; sum2=sum2+$5*$5; count=count+1} END{mean=sum/count; var=(sum2/count-mean*mean)*count/(count-1); sterr=sqrt(var)/sqrt(count); print count,mean,sterr}' replica_analysis.dat

rm -f replica_analysis.awk.tmp

echo "Done"

