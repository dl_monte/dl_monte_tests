# Author: Tom L Underwood

# This script does some post-processing on the DL_MONTE output file 'VOLDAT.000' to
# verify that the simulation gives the correct result, namely, a density of 0.3.
# Block analysis is used: 'VOLDAT.000' is split into 4 blocks, and the average density
# for each is calculated. Then, using these 4 densities, an 'average of the averages'
# is evaluated - to give the final result. Furthermore, the standard error of the mean
# of the 4 densities is calculated to give an uncertainty on the final density.

# 'VOLDAT.000' contains just over 80000 lines, each containing the volume of the system at
# regular intervals. Below we extract the average of 4 blocks of 20000 lines in 'VOLDAT.000'
# using 'awk', and store the values for each block in a file 'analysis.tmp' 
rm -f analysis.tmp
head -20000 VOLDAT.000 | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' > analysis.tmp
head -40000 VOLDAT.000 | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' >> analysis.tmp
head -60000 VOLDAT.000 | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' >> analysis.tmp
head -80000 VOLDAT.000 | tail -20000 | awk '{sum=sum+512/$1} END{print sum/NR}' >> analysis.tmp

# Here we calculate the mean of the 4 blocks, as well as the standard error in the mean, and
# output the mean and standard error
awk '{sum2=sum2+$1*$1; sum=sum+$1} END{mean=sum/NR; stdev=sqrt((sum2/NR-mean*mean)*(NR/(NR-1))); print "mean density, uncertainty = ",mean,stdev/sqrt(4)}' analysis.tmp

# Clean up
rm -f analysis.tmp

