
                   ===================================================================
                   * * *                                                         * * *
                   * * *            D L - M O N T E - v e r. 2.0 (1)             * * *
                   * * *                                                         * * *
                   * * *           STFC / CCP5 - Daresbury Laboratory            * * *
                   * * *                 program library  package                * * *
                   * * *                                                         * * *
                   * * *     general purpose Monte Carlo simulation software     * * *
                   * * *                                                         * * *
                   * * *     version:  2.01 / February 2016                      * * *
                   * * *     author:   J. A. Purton (2007-2015)                  * * *
                   * * *     -----------------------------------------------     * * *
                   * * *     contributors:                                       * * *
                   * * *     -------------                                       * * *
                   * * *     A. V. Brukhno   - Free Energy Diff, planar pore     * * *
                   * * *     T. L. Underwood - Lattice/Phase-Switch MC method    * * *
                   * * *     -----------------------------------------------     * * *
                   * * *                                                         * * *
                   * * *     current execution:                                  * * *
                   * * *     ==================                                  * * *
                   * * *     number of nodes/CPUs/cores:          1              * * *
                   * * *     number of threads per node:          1              * * *
                   * * *     number of threads employed:          1              * * *
                   * * *                                                         * * *
                   * * * ======================================================= * * *
                   * * * Publishing data obtained with DL_MONTE? - please cite:  * * *
                   * * * ------------------------------------------------------- * * *
                   * * *        Purton, J. A., Crabtree J. C. and Parker, S. C., * * *
                   * * *        Molecular Simulation 2013, 39 (14-15), 1240-1252 * * *
                   * * *                `DL_MONTE: A general purpose program for * * *
                   * * *                         parallel Monte Carlo simulation`* * *
                   * * * ------------------------------------------------------- * * *
                   * * * if using Lattice-Switch method, please also cite:       * * *
                   * * * ------------------------------------------------------- * * *
                   * * *              Bruce A.D., Wilding N.B. and G.J. Ackland, * * *
                   * * *                         Phys. Rev. Lett. 1997, 79, 3002 * * *
                   * * *    `Free Energy of Crystalline Solids: A Lattice-Switch * * *
                   * * *                                      Monte Carlo Method`* * *
                   ===================================================================

 ----------------------------------------------------------------------------------------------------
 FIELD title  (100 symbols max) :
 ----------------------------------------------------------------------------------------------------
'hard-sphere potential with hard-sphere diameter = 1 angstrom'


 ----------------------------------------------------------------------------------------------------
 atom data :
 ----------------------------------------------------------------------------------------------------
 element           mass             charge
  HS               1.0000         0.0000


 ----------------------------------------------------------------------------------------------------
 molecule data :
 ----------------------------------------------------------------------------------------------------
 molecule name
 hs      


 ----------------------------------------------------------------------------------------------------
 nonbonded (vdw) potentials :
 ----------------------------------------------------------------------------------------------------
 HS      HS         hs            1.000000       0.000000       0.000000       0.000000       0.000000

 VdW repulsion capped at D_core(  1) =      0.50000 by E_cap = 100000.00000


++++++
 NOTE: directive 'move volume cubic' assumes diagonal cell matrix and equal cell dimensions!
++++++

 NOTE: cell (box) type [ < 0 -> 2D-slit; else 3D-bulk ] =          0 coultype =          1


 DL_MONTE WARNING:    24


 inconsistency between FIELD and CONTROL files:
 charges are absent but Coulomb calculations are specified (by default)

 NOTE: switching off Coulomb calculations!


 --------------------------------------------------
 configuration box No.          1
 --------------------------------------------------

 the maximum no of atoms in this cell is      512

 WARNING: initial atom position outside the box (           1 ) - molecule #           1 ,  atom #           6 ,  global #           6
   0.0000000000000000        0.0000000000000000       0.62500000000000000     

 WARNING: initial atom position outside the box (           1 ) - molecule #           1 ,  atom #           7 ,  global #           7
   0.0000000000000000        0.0000000000000000       0.75000000000000000     

 WARNING: initial atom position outside the box (           1 ) - molecule #           1 ,  atom #           8 ,  global #           8
   0.0000000000000000        0.0000000000000000       0.87500000000000000     

          lattice vectors
    11.95041     0.00000     0.00000
     0.00000    11.95041     0.00000
     0.00000     0.00000    11.95041

          reciprocal lattice vectors
     0.08368     0.00000     0.00000
     0.00000     0.08368     0.00000
     0.00000     0.00000     0.08368


 --------------------------------------------------
 configuration sample (up to 10 atoms):
 --------------------------------------------------

 molecule hs      
 HS        c        1   type   1
      0.0000000      0.0000000      0.0000000     0
 HS        c        2   type   1
      0.0000000      0.0000000      1.4938018     0
 HS        c        3   type   1
      0.0000000      0.0000000      2.9876035     0
 HS        c        4   type   1
      0.0000000      0.0000000      4.4814053     0
 HS        c        5   type   1
      0.0000000      0.0000000      5.9752071     0
 HS        c        6   type   1
      0.0000000      0.0000000      7.4690088     0
 HS        c        7   type   1
      0.0000000      0.0000000      8.9628106     0
 HS        c        8   type   1
      0.0000000      0.0000000     10.4566124     0
 HS        c        9   type   1
      0.0000000      1.4938018      0.0000000     0
 HS        c       10   type   1
      0.0000000      1.4938018      1.4938018     0


 ==================================================
                simulation parameters 
 ==================================================

 cell (box) type [ < 0 -> 2D-slit; else 3D-bulk ] =          0 (bulk: XYZ-PBC)

 - no charges, no electrostatics required; pure short-range/VdW/metal(?) force-field

 global cutoff for interactions (Angstroms)       =    1.10000

 cutoff for vdw interactions (Angstroms)          =    1.10000

 verlet shell distance (Angstroms)                =    2.00000

 system temperature (Kelvin)                      =    1.00000

 isotropic NPT ensemble for CUBIC cell, move type =          1

 frequency of maximum V-step updates  (MC cycles) =        100

 optimum acceptance for volume moves     (target) =    0.37000

 initial maximum V-step     (type dependent unit) =    0.00100

 V-grid bin size for P(V) distribution       (dV) =    0.00000

 V-grid power for P(V) distribution  (dV*i^power) =    1.00000

 system pressure (katms)                          =    0.08040

 total number of iterations                       =      10000

 number of equilibartion steps                    =          0

 number of pre-heating steps                      =         -1

 maximum no of non-bonded neighbours              =        512

 neighbour list will be used

 nbrlist will be recalculated as necessary

 verlet method used

 maximum no of three body neighbours              =          6

 three body list updated every (cycles)           =   20000000

 stacksize for block averaging                    =       1000

 statistics printed every      (cycles) PTFILE*   =       1000

 energy frame printed every    (cycles) OUTPUT*   =      10000

 energy check printed every    (cycles) OUTPUT*   =      10000

 configuration printed every   (cycles) ARCHIV*   =       1000

 revcon configuration data in dlpoly format

 electrostatics is not used (no Coulomb interactions)

 all g-vectors stored on each node

 atom types to be moved
     HS       c

 initial maximum for atom displacement            =    0.10000

 displacement distance update  (cycles)           =        100

 acceptance ratio for atom displacement           =    0.37000

 atoms moved with frequency                       =         99

 tolerance for rejction (energy units)            =  100.00000

 random number generator seeded from user input

 random number seeds (i,j,k,l) =     12    34    56    78 (for workgroup    0)

 total job time and close time =      1000000.00         200.00

 rotation of molecules will use Euler method

 number of moving particles for box    1       512

 beta & Bjerrum length                            =        1.2027165301   167100.0026261316

 ==================================================



 --------------------------------------------------
                  initial energies 
 --------------------------------------------------

 break down of energies for box:   1

 total energy                        0.0000000000E+00
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb                  0.0000000000E+00
 nonbonded two body (vdw)            0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 bonded four body (angle)            0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00
 total virial                        0.0000000000E+00
 volume                              0.1706667308E+04

 components of energies by  molecule: hs      

 total energy                        0.0000000000E+00
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb - other types    0.0000000000E+00
 real space coulomb - same type      0.0000000000E+00
 real space coulomb - self energy    0.0000000000E+00
 real space coulomb - corection     -0.0000000000E+00
 vdw - other types mol               0.0000000000E+00
 vdw - same type mol                 0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00



 elapsed time to start of mc (seconds)     0.0480





 Iteration      10000 - elapsed time (seconds)     1.1190


      step      en-total            h-total             coul-rcp            coul-real
      step      en-vdw              en-three            en-pair             en-angle 
      step      en-four             en-many                                 volume   
      step      cell-a              cell-b              cell-c
      step      alpha               beta                gamma 
      r-av      en-total            h-total             coul-rcp            coul-real
      r-av      en-vdw              en-three            en-pair             en-angle 
      r-av      en-many             vir-tot             volume              pressure 
      r-av      cell-a              cell-b              cell-c
      r-av      alpha               beta                gamma 
 ----------------------------------------------------------------------------------------------------
     10000     0.0000000000E+00    0.8352057327E+03    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.1702530405E+04
               0.1194075055E+02    0.1194075055E+02    0.1194075055E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


               0.0000000000E+00    0.8354278198E+03    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.1702983120E+04
               0.1194180830E+02    0.1194180830E+02    0.1194180830E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 HS       c        512.0000        512.0000

 hs                1.0000          1.0000
 ----------------------------------------------------------------------------------------------------

 Workgroup    0, box    1 check: U_recalc - U_accum =  0.00000E+00  0.00000E+00  0.00000E+00  0.00000E+00 (internal, kT, kT/atom, dU/U)

 ----------------------------------------------------------------------------------------------------



 ----------------------------------------------------------------------------
                          averages and fluctuations


 ----------------------------------------------------------------------------
 ----------------------------------------------------------------------------------------------------
      avrg      en-total            h-total             coul-rcp            coul-real
      avrg      en-vdw              en-three            en-pair             en-angle 
      avrg      en-many                                 vir-tot             volume   
      avrg      pressure 
      flct      en-total            h-total             coul-rcp            coul-real
      flct      en-vdw              en-three            en-pair             en-angle 
      flct      en-many                                 vir-tot             volume   
      flct      pressure 
 ----------------------------------------------------------------------------------------------------
     10000     0.0000000000E+00    0.8327962026E+03    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.1704274686E+04
               0.0000000000E+00
               0.0000000000E+00    0.5211523523E+02    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.1538782641E+01
               0.0000000000E+00



 average cell parameters and fluctuations

          a     =   11.94483   0.00359
          b     =   11.94483   0.00359
          c     =   11.94483   0.00359
          alpha =   90.00000   0.00000
          beta  =   90.00000   0.00000
          gamma =   90.00000   0.00000
    
 HS       c        512.0000          0.0000

 hs                1.0000          0.0000


 ----------------------------------------------------------------------------
                          final energies 


 ----------------------------------------------------------------------------

 break down of energies for box:   1

 total energy                        0.0000000000E+00
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb                  0.0000000000E+00
 nonbonded two body (vdw)            0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 bonded four body (angle)            0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00
 total virial                        0.0000000000E+00
 volume                              0.1702530405E+04

 components of energies by  molecule: hs      

 total energy                        0.0000000000E+00
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb - other types    0.0000000000E+00
 real space coulomb - same type      0.0000000000E+00
 real space coulomb - self energy    0.0000000000E+00
 real space coulomb - corection     -0.0000000000E+00
 vdw - other types mol               0.0000000000E+00
 vdw - same type mol                 0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy           0.0000000000E+00


 ----------------------------------------------------------------------------
                          processing data 


 ----------------------------------------------------------------------------

 total no of atom moves         :      9907
 successful no of atom moves    :      5435      0.54860200

 displacement (Angstroms) for HS       c :   0.8811

 total no of volume moves       :        93
 successful no of volume moves  :        69      0.74193548


 max volume variations in cell matrix :

 index 1       0.0065
 index 2       0.0010
 index 3       0.0010
 index 4       0.0010
 index 5       0.0010
 index 6       0.0010
 index 7       0.0010
 index 8       0.0010
 index 9       0.0010

 total elapsed time (seconds)          1.1420
 normal exit 
