DL_MONTE test case - NPT simulation of Lennard-Jones critical point


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

This directory contains test cases for DL_MONTE in which the Lennard-Jones fluid is simulated at
the critical point within the NPT ensemble. The aim is to reproduce Fig. 2 in the paper N. B. 
Wilding & K. Binder, Physica A 231 439-447 (1996). Accordingly the number of particles (512), and 
Lennard-Jones parametrisation are identical to that used for Fig. 2 in the aforementioned paper. 
Note that the paper is says that the pressure was 'tuned' to yield critical behaviour, but does 
not specify what that pressure was for any of the system sizes considered. This was problematic
with regards to choosing an appropriate pressure for the simulation. I therefore used T*=1.1876
and p*=0.1093, which gave results similar to Fig. 2 in my own Monte Carlo code ('monteswitch',
v0.9.3, available at https://github.com/tomlunderwood/monteswitch), as well as in Nigel Wilding's 
code, and Andrey Brukhno's code. The results of my own code are included here: 
'v_vs_sweeps_monteswitch.dat' gives the volume per particle every 10 sweeps (1 sweep = 512 moves)
for a 250000-sweep simulation (128000000 moves). 

Note that perfect agreement with the paper is not necessarily expected since Fig. 2 corresponds
to 6000000 sweeps, or 3,072,000,000 moves, which is far more than is done here. A high peak
in the histogram at about v=2, and a lower peak at about v=4.5, should suffice for the test to
be considered to be 'passed', where 'v' denotes the volume per atom.

Note that this simulation utilises neighbour lists and 'lnV' isotropic volume expansion moves.


PHILOSOPHY

Input files are provided for two simulations: a long simulation and a short one. The long
simulation gathers enough statistics to verify that DL_MONTE yields 'Fig. 2' mentioned above.
The purpose of this simulation is to ensure that DL_MONTE reproduces the results of the 
aforementioned paper. The long simulation takes a long time to run, and hence is unsuitable 
and excessive to use for the purposes of regression testing. For this reason there is also 
a short simulation. The short simulation is the same as the long simulation, except that 
it is of a much shorter length: it should take ~ seconds to run. The trajectory from this 
simulation will exactly match that of the start of the long simulation (assuming there are 
no really bad bugs present in the version of DL_MONTE under consideration). Hence the short 
simulation can be used to verify that the version of DL_MONTE under consideration would, if 
the long simulation were used, reproduce the results of the aforementioned paper, without 
actually performing the long simulation. With this in mind, the procedure for testing - 
starting 'from nothing', which will not be the case most of the time (see below) - is 
as follows:

1) Perform the long simulation for the version of DL_MONTE under consideration.
2) Assuming DL_MONTE passes the test, i.e., the long simulation produces the desired results, 
   perform the short simulation, and compare the output files for both simulations to confirm
   that the short simulation gives exactly the same trajectory as at the start of the long simulation.
3) If this is the case, then the short simulation is a suitable regression test, and the output
   files of the short simulation are the benchmark. After every change to DL_MONTE the short
   simulation should be performed, and the resulting output files should be compared with the
   benchmark output files. If they do not match then the recent change in DL_MONTE has broken something
   - the test is failed. 

Note that by 'match' I mean that the trajectories should match: differences to the output format 
are unimportant. However, for ease of comparision one would probably like to use 'diff' to 
compare the output files from the short simulation for the potentially 'unstable' version of 
DL_MONTE to the benchmark output files. This would necessitate making new benchmark output files 
every time DL_MONTE is altered such that the format of the output is changed for use in testing
future versions of DL_MONTE (until another change in the output format). 

Eventually a change in DL_MONTE will be made which expectedly results in a new trajectory compared
to the benchmark output files. I define this as a 'major' change. This could be, e.g., a change to 
how the Lennard-Jones potential is implemented. In this case the existing benchmark is unsuitable, 
and one must start again 'from nothing', i.e., perform the long simulation to ensure that the 
version of DL_MONTE under consideration is accurate, and then perform the short simulation to
generate output files which will act as the benchmark for future 'minor' changes, i.e., changes
which do not alter the trajectory. To summarise: the long simulation must be used as a test case
after all major changes, and the short simulation is suitable as a test case after all minor
changes. 


INCLUDED BENCHMARKS AND RESULTS

The directories 'long' and 'short' contain the imput files for the long and short simulations
respectively. The the benchmark files, to which the output of future versions of DL_MONTE should be 
compared to assess the version's fidelity, can be found in the directory 'short/benchmarks'.

The benchmark files were generated as follows. First, a long simulation was performed using
a version of DL_MONTE from Nov 2017 - specifically a local version by AB before it was pushed to
the master branch, similar to commit da9463dc9885da125f58153632595fbd999b21bb. This commit DL_MONTE
'passes' the test in that it yields the results mentioned above. Note that the script 'analysis.sh' 
in 'long' performs post-processing on the DL_MONTE output files so that the relevant results can 
be more easily checked - read the comments in that file. The 'v_vs_t.dat' created by 'analysis.dat' 
for the long simulation can be found at 'long/v_vs_t_benchmark.dat', while 'long/v_prob_benchmark.dat'
contains the analogous p.d.f. - which should resemble Fig. 2 mentioned above. This simulation took
8.3 hours on one node of the Bath HPC facility Balena.

Confident that the aforementioned version of DL_MONTE is accurate with regards to this test, a 
short simulation was then performed; the output files from this simulation are the benchmark.

