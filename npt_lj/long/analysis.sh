# Author: Tom L Underwood

# This script does some post-processing on the DL_MONTE output files to
# verify that the simulation gives the correct results (see 'description.txt' in
# the above directory), namely, that a histogram in the specific volume matches
# Fig. 2 in Wilding & Binder, Physica A 231, 439 (1996). 


# Firstly we make a file 'v_vs_t.dat' which contains the volume per particle
# (denoted v) every 1000 steps. We extract this information from
# 'PTFILE.000' using awk. Note that every 1000 steps 11 lines of data are
# output to 'PTFILE.000', and that token 6 of line 3 contains the total volume
# of the unit cell. We thus extract such lines:

# Extract specific volume (volume is token 6 of line 3, every 11 lines)
awk '{if(NR%11==4){print $1/512}}' PTFILE.000 > v_vs_t.dat

# Making a histogram of the data in 'v_vs_t.dat' should yield a curve which
# looks very similar to Fig. 2 of the aforementioned paper. It may not be
# perfect, but should be pretty good: an even longer simulation is required 
# to gather enough statistics to get a 'perfect' histogram.

