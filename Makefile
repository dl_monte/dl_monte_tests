SHELL=/bin/sh
#
# Makefile for DL_MONTE-2 test cases (VDW_dir builds)
#
# - auto-generated with the aid of build-test script
# - based on Makefile-header + Makefile-template
# - two versions of Makefile-template are Makefile-temp-gen & Makefile-temp-fed
#
# Authors: Tom L Underwood & Andrey V Brukhno
#

# DL_MONTE executable 
# can be found anywhere, but the search starts in ../bin/ (see PATH below)

#TU: ***** Define the path to the DL_MONTE executable in the variable 'EXE' below ****

#EXE="DLMONTE-SRL-VDW_tab.X"
#EXE="DLMONTE-SRL-VDW_dir.X"
#EXE="DLMONTE-PRL-VDW_tab.X"
#EXE="dlmonte_mpi_4"
EXE="DLMONTE-SRL.X"

# Run ALL tests in a batch ('make' or 'make all')
.PHONY: all
all:    sysrep tests

.PHONY: sysrep
sysrep: 
	@echo ;\
	echo "===================================================================="; \
	echo " System details :"; \
	echo "-----------------"; \
	uname -av; \
	echo "================="; \
	echo " CPUs available :"; \
	echo "-----------------"; \
	lscpu ; \
	echo "===================================================================="; \
	echo ;\
	echo ;\

# Main tests - include only appropriate ('make main') - to be altered manually
.PHONY: main
main:   tests

# Short tests - include only appropriate ('make short') - to be altered manually
.PHONY: short
short:  tests

# Generic targets

#TU: NB: I have 'psmc_sync_ewald_atm-main' from this list because it gives different
#TU: results (i.e. non-empty diffs comparing test simulation output to the benchmarks
#TU: files) on different platforms. When this is resolved the test will be reinstated.
#TU: Note however that 'psmc_sync_ewald_atm-main' has not been removed from the other
#TU: phony targets, e.g. unpack, clear.
.PHONY: tests
tests: \
        nvt_stillingerweber_diamond-main \
        nvt_paracetamol_nonbrlist_clist_paratom-main \
        nvt_paracetamol_nonbrlist_clist-main \
        nvt_paracetamol_nonbrlist-main \
        nvt_paracetamol_clist_paratom-main \
        nvt_paracetamol_clist-main \
        nvt_paracetamol_paratom_distewald-main \
        static_paracetamol-main \
        nvt_paracetamol-main \
        gcmc_lj_tm_clist_ortho-main \
        gibbs_lj_mol_clist_ortho-main \
        gibbs_lj_atom_clist_ortho-main \
        gcmc_lj_clist_ortho-main \
        npt_lj_clist_ortho-main \
        nbrlist_auto_psmc_nvt_spce_water-main \
        nbrlist_auto_npt_spce_water-main \
        nbrlist_auto_nvt_spce_water-main \
        sgcmc_ising_atm-short \
        ewald_nacl_atm_fcc_zyx-main \
        ewald_nacl_atm_fcc_zxy-main \
        ewald_nacl_atm_fcc_yxz-main \
        ewald_nacl_atm_zyx-main \
        ewald_nacl_atm_zxy-main \
        ewald_nacl_atm_yzx-main \
        ewald_nacl_atm_yxz-main \
        ewald_nacl_atm_xzy-main \
        ewald_nacl_atm_useortho-main \
        gibbs_lj_mol-short \
        gibbs_lj_atom-short \
        gcmc_lj_tm-short \
        npt_sds_in_water-short-abh-2017nov-xn \
        orient_water_tip4p2005-short \
        psmc_hd_nvt-short \
        psmc_lj_npt-short \
        psmc_hs_npt-short \
        psmc_hs_nvt-short \
        psmc_sync_mol-main \
        ewald_nacl_mol-main \
        ewald_nacl_atm-main \
        gcmc_lj-short-abw-2017jan-alpha \
        gcmc_hs-short-abw-2017jan-alpha \
        gcmc_co2_zeolite-short-abw-2017jan-alpha \
        nist_spce_water-main \
        npt_water_spce-short-abw-2017jan-alpha \
        npt_lj-short-abw-2017jan-alpha \
        npt_hs_ortho-short-abw-2017jan-alpha \
        npt_hs_cubic-short-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
        fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
        slit2_ljw_2atms-short-abw-2017jan-alpha \
        slit2_ljw_1atom-short-abw-2017jan-alpha \
        slit1_ljw_1atom-short-abw-2017jan-alpha \
        slit_mfa2-short-abh-2017feb-alpha \
        slit_mfa1-short-abh-2017feb-alpha \

# Generic targets
.PHONY: nvt_paracetamol
nvt_paracetamol: \
        nvt_paracetamol_nonbrlist_clist_paratom-main \
        nvt_paracetamol_nonbrlist_clist-main \
        nvt_paracetamol_nonbrlist-main \
        nvt_paracetamol_clist_paratom-main \
        nvt_paracetamol_clist-main \
        nvt_paracetamol_paratom_distewald-main \
        nvt_paracetamol-main

# Generic targets
.PHONY: ewald
ewald: \
        psmc_sync_ewald_atm-main \
        ewald_nacl_mol-main \
        ewald_nacl_atm-main \
        gcmc_co2_zeolite-short-abw-2017jan-alpha \
        fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \

# Generic targets
.PHONY: psmc
psmc: \
        psmc_hd_nvt-short \
        psmc_lj_npt-short \
        psmc_hs_npt-short \
        psmc_hs_nvt-short \
        psmc_sync_mol-main \
        psmc_sync_ewald_atm-main \

# Generic targets
.PHONY: gcmc
gcmc: \
        gcmc_lj-short-abw-2017jan-alpha \
        gcmc_hs-short-abw-2017jan-alpha \
        gcmc_co2_zeolite-short-abw-2017jan-alpha \

# Generic targets
.PHONY: npts
npts: \
        nist_spce_water-main \
        npt_water_spce-short-abw-2017jan-alpha \
        npt_lj-short-abw-2017jan-alpha \
        npt_hs_ortho-short-abw-2017jan-alpha \
        npt_hs_cubic-short-abw-2017jan-alpha \
        npt_sds_in_water-short-abh-2017nov-xn \
        
#        npt_sds_in_water-long-abh-2017nov-xn \

# Generic targets
.PHONY: feds
feds: \
        fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
        fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \

#        fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
#        fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \

# Generic targets
.PHONY: feds_short
feds_short: \
        fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \

# Generic targets
.PHONY: feds_long
feds_long: \
        fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
        fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
        fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
        fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \

# Generic targets
.PHONY: slit
slit: \
        slit2_ljw_2atms-short-abw-2017jan-alpha \
        slit2_ljw_1atom-short-abw-2017jan-alpha \
        slit1_ljw_1atom-short-abw-2017jan-alpha \
        slit_mfa2-short-abh-2017feb-alpha \
        slit_mfa1-short-abh-2017feb-alpha \

# Generic targets
.PHONY: slit_long
slit_long: \
        slit2_ljw_2atms-long-abw-2017jan-alpha \
        slit2_ljw_1atom-long-abw-2017jan-alpha \
        slit1_ljw_1atom-long-abw-2017jan-alpha \

# Updating targets
.PHONY: update
update: \
 update_nvt_stillingerweber_diamond-main \
 update_nvt_paracetamol_nonbrlist_clist_paratom-main \
 update_nvt_paracetamol_nonbrlist_clist-main \
 update_nvt_paracetamol_nonbrlist-main \
 update_nvt_paracetamol_clist_paratom-main \
 update_nvt_paracetamol_clist-main \
 update_nvt_paracetamol_paratom_distewald-main \
 update_static_paracetamol-main \
 update_nvt_paracetamol-main \
 update_gcmc_lj_tm_clist_ortho-main \
 update_gibbs_lj_mol_clist_ortho-main \
 update_gibbs_lj_atom_clist_ortho-main \
 update_gcmc_lj_clist_ortho-main \
 update_npt_lj_clist_ortho-main \
 update_nbrlist_auto_psmc_nvt_spce_water-main \
 update_nbrlist_auto_npt_spce_water-main \
 update_nbrlist_auto_nvt_spce_water-main \
 update_sgcmc_ising_atm-short \
 update_ewald_nacl_atm_fcc_zyx-main \
 update_ewald_nacl_atm_fcc_zxy-main \
 update_ewald_nacl_atm_fcc_yxz-main \
 update_ewald_nacl_atm_zyx-main \
 update_ewald_nacl_atm_zxy-main \
 update_ewald_nacl_atm_yzx-main \
 update_ewald_nacl_atm_yxz-main \
 update_ewald_nacl_atm_xzy-main \
 update_ewald_nacl_atm_useortho-main \
 update_gibbs_lj_mol-short \
 update_gibbs_lj_atom-short \
 update_gcmc_lj_tm-short \
 update_npt_sds_in_water-short-abh-2017nov-xn \
 update_npt_sds_in_water-long-abh-2017nov-xn \
 update_orient_water_tip4p2005-short \
 update_psmc_hd_nvt-short \
 update_psmc_lj_npt-short \
 update_psmc_hs_npt-short \
 update_psmc_hs_nvt-short \
 update_psmc_sync_mol-main \
 update_psmc_sync_ewald_atm-main \
 update_ewald_nacl_mol-main \
 update_ewald_nacl_atm-main \
 update_gcmc_lj-short-abw-2017jan-alpha \
 update_gcmc_hs-short-abw-2017jan-alpha \
 update_gcmc_co2_zeolite-short-abw-2017jan-alpha \
 update_nist_spce_water-main \
 update_npt_water_spce-short-abw-2017jan-alpha \
 update_npt_lj-short-abw-2017jan-alpha \
 update_npt_hs_ortho-short-abw-2017jan-alpha \
 update_npt_hs_cubic-short-abw-2017jan-alpha \
 update_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 update_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 update_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 update_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 update_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 update_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 update_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
 update_slit2_ljw_2atms-short-abw-2017jan-alpha \
 update_slit2_ljw_1atom-short-abw-2017jan-alpha \
 update_slit1_ljw_1atom-short-abw-2017jan-alpha \
 update_slit2_ljw_2atms-long-abw-2017jan-alpha \
 update_slit2_ljw_1atom-long-abw-2017jan-alpha \
 update_slit1_ljw_1atom-long-abw-2017jan-alpha \
 update_slit_mfa2-short-abh-2017feb-alpha \
 update_slit_mfa1-short-abh-2017feb-alpha \

# Updating targets
.PHONY: update_nvt_paracetamol
update_nvt_paracetamol: \
        update_nvt_paracetamol_nonbrlist_clist_paratom-main \
        update_nvt_paracetamol_nonbrlist_clist-main \
        update_nvt_paracetamol_nonbrlist-main \
        update_nvt_paracetamol_clist_paratom-main \
        update_nvt_paracetamol_clist-main \
        update_nvt_paracetamol_paratom_distewald-main \
        update_nvt_paracetamol-main

# Updating targets
.PHONY: update_ewald
update_ewald: \
 update_psmc_sync_ewald_atm-main \
 update_ewald_nacl_mol-main \
 update_ewald_nacl_atm-main \
 update_gcmc_co2_zeolite-short-abw-2017jan-alpha \
 update_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \

# Updating targets
.PHONY: update_psmc
update_psmc: \
 update_psmc_hd_nvt-short \
 update_psmc_lj_npt-short \
 update_psmc_hs_npt-short \
 update_psmc_hs_nvt-short \
 update_psmc_sync_mol-main \
 update_psmc_sync_ewald_atm-main \

# Updating targets
.PHONY: update_gcmc
update_gcmc: \
 update_gcmc_lj-short-abw-2017jan-alpha \
 update_gcmc_hs-short-abw-2017jan-alpha \
 update_gcmc_co2_zeolite-short-abw-2017jan-alpha \

# Updating targets
.PHONY: update_npts
update_npts: \
 update_nist_spce_water-main \
 update_npt_water_spce-short-abw-2017jan-alpha \
 update_npt_lj-short-abw-2017jan-alpha \
 update_npt_hs_ortho-short-abw-2017jan-alpha \
 update_npt_hs_cubic-short-abw-2017jan-alpha \
 update_npt_sds_in_water-short-abh-2017nov-xn \
 update_npt_sds_in_water-long-abh-2017nov-xn \

# Updating targets
.PHONY: update_feds
update_feds: \
 update_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 update_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 update_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 update_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 update_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 update_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 update_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
 update_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \

# Updating targets
.PHONY: update_slit
update_slit: \
 update_slit2_ljw_2atms-short-abw-2017jan-alpha \
 update_slit2_ljw_1atom-short-abw-2017jan-alpha \
 update_slit1_ljw_1atom-short-abw-2017jan-alpha \
 update_slit2_ljw_2atms-long-abw-2017jan-alpha \
 update_slit2_ljw_1atom-long-abw-2017jan-alpha \
 update_slit1_ljw_1atom-long-abw-2017jan-alpha \
 update_slit_mfa2-short-abh-2017feb-alpha \
 update_slit_mfa1-short-abh-2017feb-alpha \

# Packing targets
.PHONY: pack
pack: \
   pack_nvt_stillingerweber_diamond-main \
   pack_nvt_paracetamol_nonbrlist_clist_paratom-main \
   pack_nvt_paracetamol_nonbrlist_clist-main \
   pack_nvt_paracetamol_nonbrlist-main \
   pack_nvt_paracetamol_clist_paratom-main \
   pack_nvt_paracetamol_clist-main \
   pack_nvt_paracetamol_paratom_distewald-main \
   pack_static_paracetamol-main \
   pack_nvt_paracetamol-main \
   pack_gcmc_lj_tm_clist_ortho-main \
   pack_gibbs_lj_mol_clist_ortho-main \
   pack_gibbs_lj_atom_clist_ortho-main \
   pack_gcmc_lj_clist_ortho-main \
   pack_npt_lj_clist_ortho-main \
   pack_nbrlist_auto_psmc_nvt_spce_water-main \
   pack_nbrlist_auto_npt_spce_water-main \
   pack_nbrlist_auto_nvt_spce_water-main \
   pack_sgcmc_ising_atm-short \
   pack_ewald_nacl_atm_fcc_zyx-main \
   pack_ewald_nacl_atm_fcc_zxy-main \
   pack_ewald_nacl_atm_fcc_yxz-main \
   pack_ewald_nacl_atm_zyx-main \
   pack_ewald_nacl_atm_zxy-main \
   pack_ewald_nacl_atm_yzx-main \
   pack_ewald_nacl_atm_yxz-main \
   pack_ewald_nacl_atm_xzy-main \
   pack_ewald_nacl_atm_useortho-main \
   pack_gibbs_lj_mol-short \
   pack_gibbs_lj_atom-short \
   pack_gcmc_lj_tm-short \
   pack_npt_sds_in_water-short-abh-2017nov-xn \
   pack_npt_sds_in_water-long-abh-2017nov-xn \
   pack_orient_water_tip4p2005-short \
   pack_psmc_hd_nvt-short \
   pack_psmc_lj_npt-short \
   pack_psmc_hs_npt-short \
   pack_psmc_hs_nvt-short \
   pack_psmc_sync_mol-main \
   pack_psmc_sync_ewald_atm-main \
   pack_ewald_nacl_mol-main \
   pack_ewald_nacl_atm-main \
   pack_gcmc_lj-short-abw-2017jan-alpha \
   pack_gcmc_hs-short-abw-2017jan-alpha \
   pack_gcmc_co2_zeolite-short-abw-2017jan-alpha \
   pack_nist_spce_water-main \
   pack_npt_water_spce-short-abw-2017jan-alpha \
   pack_npt_lj-short-abw-2017jan-alpha \
   pack_npt_hs_ortho-short-abw-2017jan-alpha \
   pack_npt_hs_cubic-short-abw-2017jan-alpha \
   pack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
   pack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
   pack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
   pack_slit2_ljw_2atms-short-abw-2017jan-alpha \
   pack_slit2_ljw_1atom-short-abw-2017jan-alpha \
   pack_slit1_ljw_1atom-short-abw-2017jan-alpha \
   pack_slit2_ljw_2atms-long-abw-2017jan-alpha \
   pack_slit2_ljw_1atom-long-abw-2017jan-alpha \
   pack_slit1_ljw_1atom-long-abw-2017jan-alpha \
   pack_slit_mfa2-short-abh-2017feb-alpha \
   pack_slit_mfa1-short-abh-2017feb-alpha \

# Packing targets
.PHONY: pack_nvt_paracetamol
pack_nvt_paracetamol: \
        pack_nvt_paracetamol_nonbrlist_clist_paratom-main \
        pack_nvt_paracetamol_nonbrlist_clist-main \
        pack_nvt_paracetamol_nonbrlist-main \
        pack_nvt_paracetamol_clist_paratom-main \
        pack_nvt_paracetamol_clist-main \
        pack_nvt_paracetamol_paratom_distewald-main \
        pack_nvt_paracetamol-main

# Packing targets
.PHONY: pack_ewald
pack_ewald: \
   pack_psmc_sync_ewald_atm-main \
   pack_ewald_nacl_mol-main \
   pack_ewald_nacl_atm-main \
   pack_gcmc_co2_zeolite-short-abw-2017jan-alpha \
   pack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \

# Packing targets
.PHONY: pack_psmc
pack_psmc: \
   pack_psmc_hd_nvt-short \
   pack_psmc_lj_npt-short \
   pack_psmc_hs_npt-short \
   pack_psmc_hs_nvt-short \
   pack_psmc_sync_mol-main \
   pack_psmc_sync_ewald_atm-main \
 
# Packing targets
.PHONY: pack_gcmc
pack_gcmc: \
   pack_gcmc_lj-short-abw-2017jan-alpha \
   pack_gcmc_hs-short-abw-2017jan-alpha \
   pack_gcmc_co2_zeolite-short-abw-2017jan-alpha \

# Packing targets
.PHONY: pack_npts
pack_npts: \
   pack_nist_spce_water-main \
   pack_npt_water_spce-short-abw-2017jan-alpha \
   pack_npt_lj-short-abw-2017jan-alpha \
   pack_npt_hs_ortho-short-abw-2017jan-alpha \
   pack_npt_hs_cubic-short-abw-2017jan-alpha \
   pack_npt_sds_in_water-short-abh-2017nov-xn \
   pack_npt_sds_in_water-long-abh-2017nov-xn \

# Packing targets
.PHONY: pack_feds
pack_feds: \
   pack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
   pack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
   pack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
   pack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
   pack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
   pack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
   pack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \

# Packing targets
.PHONY: pack_slit
pack_slit: \
   pack_slit2_ljw_2atms-short-abw-2017jan-alpha \
   pack_slit2_ljw_1atom-short-abw-2017jan-alpha \
   pack_slit1_ljw_1atom-short-abw-2017jan-alpha \
   pack_slit2_ljw_2atms-long-abw-2017jan-alpha \
   pack_slit2_ljw_1atom-long-abw-2017jan-alpha \
   pack_slit1_ljw_1atom-long-abw-2017jan-alpha \
   pack_slit_mfa2-short-abh-2017feb-alpha \
   pack_slit_mfa1-short-abh-2017feb-alpha \

# Unpacking targets
.PHONY: unpack
unpack: \
 unpack_nvt_stillingerweber_diamond-main \
 unpack_nvt_paracetamol_nonbrlist_clist_paratom-main \
 unpack_nvt_paracetamol_nonbrlist_clist-main \
 unpack_nvt_paracetamol_nonbrlist-main \
 unpack_nvt_paracetamol_clist_paratom-main \
 unpack_nvt_paracetamol_clist-main \
 unpack_nvt_paracetamol_paratom_distewald-main \
 unpack_static_paracetamol-main \
 unpack_nvt_paracetamol-main \
 unpack_gcmc_lj_tm_clist_ortho-main \
 unpack_gibbs_lj_mol_clist_ortho-main \
 unpack_gibbs_lj_atom_clist_ortho-main \
 unpack_gcmc_lj_clist_ortho-main \
 unpack_npt_lj_clist_ortho-main \
 unpack_nbrlist_auto_psmc_nvt_spce_water-main \
 unpack_nbrlist_auto_npt_spce_water-main \
 unpack_nbrlist_auto_nvt_spce_water-main \
 unpack_sgcmc_ising_atm-short \
 unpack_ewald_nacl_atm_fcc_zyx-main \
 unpack_ewald_nacl_atm_fcc_zxy-main \
 unpack_ewald_nacl_atm_fcc_yxz-main \
 unpack_ewald_nacl_atm_zyx-main \
 unpack_ewald_nacl_atm_zxy-main \
 unpack_ewald_nacl_atm_yzx-main \
 unpack_ewald_nacl_atm_yxz-main \
 unpack_ewald_nacl_atm_xzy-main \
 unpack_ewald_nacl_atm_useortho-main \
 unpack_gibbs_lj_mol-short \
 unpack_gibbs_lj_atom-short \
 unpack_gcmc_lj_tm-short \
 unpack_npt_sds_in_water-short-abh-2017nov-xn \
 unpack_npt_sds_in_water-long-abh-2017nov-xn \
 unpack_orient_water_tip4p2005-short \
 unpack_psmc_hd_nvt-short \
 unpack_psmc_lj_npt-short \
 unpack_psmc_hs_npt-short \
 unpack_psmc_hs_nvt-short \
 unpack_psmc_sync_mol-main \
 unpack_psmc_sync_ewald_atm-main \
 unpack_ewald_nacl_mol-main \
 unpack_ewald_nacl_atm-main \
 unpack_gcmc_lj-short-abw-2017jan-alpha \
 unpack_gcmc_hs-short-abw-2017jan-alpha \
 unpack_gcmc_co2_zeolite-short-abw-2017jan-alpha \
 unpack_nist_spce_water-main \
 unpack_npt_water_spce-short-abw-2017jan-alpha \
 unpack_npt_lj-short-abw-2017jan-alpha \
 unpack_npt_hs_ortho-short-abw-2017jan-alpha \
 unpack_npt_hs_cubic-short-abw-2017jan-alpha \
 unpack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 unpack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 unpack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
 unpack_slit2_ljw_2atms-short-abw-2017jan-alpha \
 unpack_slit2_ljw_1atom-short-abw-2017jan-alpha \
 unpack_slit1_ljw_1atom-short-abw-2017jan-alpha \
 unpack_slit2_ljw_2atms-long-abw-2017jan-alpha \
 unpack_slit2_ljw_1atom-long-abw-2017jan-alpha \
 unpack_slit1_ljw_1atom-long-abw-2017jan-alpha \
 unpack_slit_mfa2-short-abh-2017feb-alpha \
 unpack_slit_mfa1-short-abh-2017feb-alpha \

# Unpacking targets
.PHONY: update_nvt_paracetamol
unpack_nvt_paracetamol: \
        unpack_nvt_paracetamol_nonbrlist_clist_paratom-main \
        unpack_nvt_paracetamol_nonbrlist_clist-main \
        unpack_nvt_paracetamol_nonbrlist-main \
        unpack_nvt_paracetamol_clist_paratom-main \
        unpack_nvt_paracetamol_clist-main \
        unpack_nvt_paracetamol_paratom_distewald-main \
        unpack_nvt_paracetamol-main

# Unpacking targets
.PHONY: unpack_ewald
unpack_ewald: \
 unpack_psmc_sync_ewald_atm-main \
 unpack_ewald_nacl_mol-main \
 unpack_ewald_nacl_atm-main \
 unpack_gcmc_co2_zeolite-short-abw-2017jan-alpha \
 unpack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \

# Unpacking targets
.PHONY: unpack_psmc
unpack_psmc: \
 unpack_psmc_hd_nvt-short \
 unpack_psmc_lj_npt-short \
 unpack_psmc_hs_npt-short \
 unpack_psmc_hs_nvt-short \
 unpack_psmc_sync_mol-main \
 unpack_psmc_sync_ewald_atm-main \

# Unpacking targets
.PHONY: unpack_gcmc
unpack_gcmc: \
 unpack_gcmc_lj-short-abw-2017jan-alpha \
 unpack_gcmc_hs-short-abw-2017jan-alpha \
 unpack_gcmc_co2_zeolite-short-abw-2017jan-alpha \

# Unpacking targets
.PHONY: unpack_npts
unpack_npts: \
 unpack_nist_spce_water-main \
 unpack_npt_water_spce-short-abw-2017jan-alpha \
 unpack_npt_lj-short-abw-2017jan-alpha \
 unpack_npt_hs_ortho-short-abw-2017jan-alpha \
 unpack_npt_hs_cubic-short-abw-2017jan-alpha \
 unpack_npt_sds_in_water-short-abh-2017nov-xn \
 unpack_npt_sds_in_water-long-abh-2017nov-xn \

# Unpacking targets
.PHONY: unpack_feds
unpack_feds: \
 unpack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 unpack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
 unpack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
 unpack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
 unpack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \

# Unpacking targets
.PHONY: unpack_slit
unpack_slit: \
 unpack_slit2_ljw_2atms-short-abw-2017jan-alpha \
 unpack_slit2_ljw_1atom-short-abw-2017jan-alpha \
 unpack_slit1_ljw_1atom-short-abw-2017jan-alpha \
 unpack_slit2_ljw_2atms-long-abw-2017jan-alpha \
 unpack_slit2_ljw_1atom-long-abw-2017jan-alpha \
 unpack_slit1_ljw_1atom-long-abw-2017jan-alpha \
 unpack_slit_mfa2-short-abh-2017feb-alpha \
 unpack_slit_mfa1-short-abh-2017feb-alpha \

# Cleaning targets
.PHONY: clean
clean: \
  clean_nvt_stillingerweber_diamond-main \
  clean_nvt_paracetamol_nonbrlist_clist_paratom-main \
  clean_nvt_paracetamol_nonbrlist_clist-main \
  clean_nvt_paracetamol_nonbrlist-main \
  clean_nvt_paracetamol_clist_paratom-main \
  clean_nvt_paracetamol_clist-main \
  clean_nvt_paracetamol_paratom_distewald-main \
  clean_static_paracetamol-main \
  clean_nvt_paracetamol-main \
  clean_gcmc_lj_tm_clist_ortho-main \
  clean_gibbs_lj_mol_clist_ortho-main \
  clean_gibbs_lj_atom_clist_ortho-main \
  clean_gcmc_lj_clist_ortho-main \
  clean_npt_lj_clist_ortho-main \
  clean_nbrlist_auto_psmc_nvt_spce_water-main \
  clean_nbrlist_auto_npt_spce_water-main \
  clean_nbrlist_auto_nvt_spce_water-main \
  clean_sgcmc_ising_atm-short \
  clean_ewald_nacl_atm_fcc_zyx-main \
  clean_ewald_nacl_atm_fcc_zxy-main \
  clean_ewald_nacl_atm_fcc_yxz-main \
  clean_ewald_nacl_atm_zyx-main \
  clean_ewald_nacl_atm_zxy-main \
  clean_ewald_nacl_atm_yzx-main \
  clean_ewald_nacl_atm_yxz-main \
  clean_ewald_nacl_atm_xzy-main \
  clean_ewald_nacl_atm_useortho-main \
  clean_gibbs_lj_mol-short \
  clean_gibbs_lj_atom-short \
  clean_gcmc_lj_tm-short \
  clean_npt_sds_in_water-short-abh-2017nov-xn \
  clean_npt_sds_in_water-long-abh-2017nov-xn \
  clean_orient_water_tip4p2005-short \
  clean_psmc_hd_nvt-short \
  clean_psmc_lj_npt-short \
  clean_psmc_hs_npt-short \
  clean_psmc_hs_nvt-short \
  clean_psmc_sync_mol-main \
  clean_psmc_sync_ewald_atm-main \
  clean_ewald_nacl_mol-main \
  clean_ewald_nacl_atm-main \
  clean_gcmc_lj-short-abw-2017jan-alpha \
  clean_gcmc_hs-short-abw-2017jan-alpha \
  clean_gcmc_co2_zeolite-short-abw-2017jan-alpha \
  clean_nist_spce_water-main \
  clean_npt_water_spce-short-abw-2017jan-alpha \
  clean_npt_lj-short-abw-2017jan-alpha \
  clean_npt_hs_ortho-short-abw-2017jan-alpha \
  clean_npt_hs_cubic-short-abw-2017jan-alpha \
  clean_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clean_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clean_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
  clean_slit2_ljw_2atms-short-abw-2017jan-alpha \
  clean_slit2_ljw_1atom-short-abw-2017jan-alpha \
  clean_slit1_ljw_1atom-short-abw-2017jan-alpha \
  clean_slit2_ljw_2atms-long-abw-2017jan-alpha \
  clean_slit2_ljw_1atom-long-abw-2017jan-alpha \
  clean_slit1_ljw_1atom-long-abw-2017jan-alpha \
  clean_slit_mfa2-short-abh-2017feb-alpha \
  clean_slit_mfa1-short-abh-2017feb-alpha \

# Cleaning targets
.PHONY: clean_nvt_paracetamol
clean_nvt_paracetamol: \
        clean_nvt_paracetamol_nonbrlist_clist_paratom-main \
        clean_nvt_paracetamol_nonbrlist_clist-main \
        clean_nvt_paracetamol_nonbrlist-main \
        clean_nvt_paracetamol_clist_paratom-main \
        clean_nvt_paracetamol_clist-main \
        clean_nvt_paracetamol_paratom_distewald-main \
        clean_nvt_paracetamol-main


# Cleaning targets
.PHONY: clean_ewald
clean_ewald: \
  clean_psmc_sync_ewald_atm-main \
  clean_ewald_nacl_mol-main \
  clean_ewald_nacl_atm-main \
  clean_gcmc_co2_zeolite-short-abw-2017jan-alpha \
  clean_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \

# Cleaning targets
.PHONY: clean_psmc
clean_psmc: \
  clean_psmc_hd_nvt-short \
  clean_psmc_lj_npt-short \
  clean_psmc_hs_npt-short \
  clean_psmc_hs_nvt-short \
  clean_psmc_sync_mol-main \
  clean_psmc_sync_ewald_atm-main \

# Cleaning targets
.PHONY: clean_gcmc
clean_gcmc: \
  clean_gcmc_lj-short-abw-2017jan-alpha \
  clean_gcmc_hs-short-abw-2017jan-alpha \
  clean_gcmc_co2_zeolite-short-abw-2017jan-alpha \

# Cleaning targets
.PHONY: clean_npts
clean_npts: \
  clean_nist_spce_water-main \
  clean_npt_water_spce-short-abw-2017jan-alpha \
  clean_npt_lj-short-abw-2017jan-alpha \
  clean_npt_hs_ortho-short-abw-2017jan-alpha \
  clean_npt_hs_cubic-short-abw-2017jan-alpha \
  clean_npt_sds_in_water-short-abh-2017nov-xn \
  clean_npt_sds_in_water-long-abh-2017nov-xn \

# Cleaning targets
.PHONY: clean_feds
clean_feds: \
  clean_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
  clean_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
  clean_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
  clean_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clean_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clean_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clean_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \

# Cleaning targets
.PHONY: clean_slit
clean_slit: \
  clean_slit2_ljw_2atms-short-abw-2017jan-alpha \
  clean_slit2_ljw_1atom-short-abw-2017jan-alpha \
  clean_slit1_ljw_1atom-short-abw-2017jan-alpha \
  clean_slit2_ljw_2atms-long-abw-2017jan-alpha \
  clean_slit2_ljw_1atom-long-abw-2017jan-alpha \
  clean_slit1_ljw_1atom-long-abw-2017jan-alpha \
  clean_slit_mfa2-short-abh-2017feb-alpha \
  clean_slit_mfa1-short-abh-2017feb-alpha \

# Cleaning targets
.PHONY: clear
clear: \
  clear_nvt_stillingerweber_diamond-main \
  clear_nvt_paracetamol_nonbrlist_clist_paratom-main \
  clear_nvt_paracetamol_nonbrlist_clist-main \
  clear_nvt_paracetamol_nonbrlist-main \
  clear_nvt_paracetamol_clist_paratom-main \
  clear_nvt_paracetamol_clist-main \
  clear_nvt_paracetamol_paratom_distewald-main \
  clear_static_paracetamol-main \
  clear_nvt_paracetamol-main \
  clear_gcmc_lj_tm_clist_ortho-main \
  clear_gibbs_lj_mol_clist_ortho-main \
  clear_gibbs_lj_atom_clist_ortho-main \
  clear_gcmc_lj_clist_ortho-main \
  clear_npt_lj_clist_ortho-main \
  clear_nbrlist_auto_psmc_nvt_spce_water-main \
  clear_nbrlist_auto_npt_spce_water-main \
  clear_nbrlist_auto_nvt_spce_water-main \
  clear_sgcmc_ising_atm-short \
  clear_ewald_nacl_atm_fcc_zyx-main \
  clear_ewald_nacl_atm_fcc_zxy-main \
  clear_ewald_nacl_atm_fcc_yxz-main \
  clear_ewald_nacl_atm_zyx-main \
  clear_ewald_nacl_atm_zxy-main \
  clear_ewald_nacl_atm_yzx-main \
  clear_ewald_nacl_atm_yxz-main \
  clear_ewald_nacl_atm_xzy-main \
  clear_ewald_nacl_atm_useortho-main \
  clear_gibbs_lj_mol-short \
  clear_gibbs_lj_atom-short \
  clear_gcmc_lj_tm-short \
  clear_npt_sds_in_water-short-abh-2017nov-xn \
  clear_npt_sds_in_water-long-abh-2017nov-xn \
  clear_orient_water_tip4p2005-short \
  clear_psmc_hd_nvt-short \
  clear_psmc_lj_npt-short \
  clear_psmc_hs_npt-short \
  clear_psmc_hs_nvt-short \
  clear_psmc_sync_mol-main \
  clear_psmc_sync_ewald_atm-main \
  clear_ewald_nacl_mol-main \
  clear_ewald_nacl_atm-main \
  clear_gcmc_lj-short-abw-2017jan-alpha \
  clear_gcmc_hs-short-abw-2017jan-alpha \
  clear_gcmc_co2_zeolite-short-abw-2017jan-alpha \
  clear_nist_spce_water-main \
  clear_npt_water_spce-short-abw-2017jan-alpha \
  clear_npt_lj-short-abw-2017jan-alpha \
  clear_npt_hs_ortho-short-abw-2017jan-alpha \
  clear_npt_hs_cubic-short-abw-2017jan-alpha \
  clear_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clear_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clear_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
  clear_slit2_ljw_2atms-short-abw-2017jan-alpha \
  clear_slit2_ljw_1atom-short-abw-2017jan-alpha \
  clear_slit1_ljw_1atom-short-abw-2017jan-alpha \
  clear_slit2_ljw_2atms-long-abw-2017jan-alpha \
  clear_slit2_ljw_1atom-long-abw-2017jan-alpha \
  clear_slit1_ljw_1atom-long-abw-2017jan-alpha \
  clear_slit_mfa2-short-abh-2017feb-alpha \
  clear_slit_mfa1-short-abh-2017feb-alpha \

# Cleaning targets
.PHONY: clear_nvt_paracetamol
clear_nvt_paracetamol: \
        clear_nvt_paracetamol_nonbrlist_clist_paratom-main \
        clear_nvt_paracetamol_nonbrlist_clist-main \
        clear_nvt_paracetamol_nonbrlist-main \
        clear_nvt_paracetamol_clist_paratom-main \
        clear_nvt_paracetamol_clist-main \
        clear_nvt_paracetamol_paratom_distewald-main \
        clear_nvt_paracetamol-main

#  targets
.PHONY: clear_ewald
clear_ewald: \
  clear_psmc_sync_ewald_atm-main \
  clear_ewald_nacl_mol-main \
  clear_ewald_nacl_atm-main \
  clear_gcmc_co2_zeolite-short-abw-2017jan-alpha \
  clear_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \

# Cleaning targets
.PHONY: clear_psmc
clear_psmc: \
  clear_psmc_hd_nvt-short \
  clear_psmc_lj_npt-short \
  clear_psmc_hs_npt-short \
  clear_psmc_hs_nvt-short \
  clear_psmc_sync_mol-main \
  clear_psmc_sync_ewald_atm-main \

# Cleaning targets
.PHONY: clear_gcmc
clear_gcmc: \
  clear_gcmc_lj-short-abw-2017jan-alpha \
  clear_gcmc_hs-short-abw-2017jan-alpha \
  clear_gcmc_co2_zeolite-short-abw-2017jan-alpha \

# Cleaning targets
.PHONY: clear_npts
clear_npts: \
  clear_nist_spce_water-main \
  clear_npt_water_spce-short-abw-2017jan-alpha \
  clear_npt_lj-short-abw-2017jan-alpha \
  clear_npt_hs_ortho-short-abw-2017jan-alpha \
  clear_npt_hs_cubic-short-abw-2017jan-alpha \
  clear_npt_sds_in_water-short-abh-2017nov-xn \
  clear_npt_sds_in_water-long-abh-2017nov-xn \

# Cleaning targets
.PHONY: clear_feds
clear_feds: \
  clear_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha \
  clear_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha \
  clear_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha \
  clear_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clear_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha \
  clear_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha \
  clear_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha \

# Cleaning targets
.PHONY: clear_slit
clear_slit: \
  clear_slit2_ljw_2atms-short-abw-2017jan-alpha \
  clear_slit2_ljw_1atom-short-abw-2017jan-alpha \
  clear_slit1_ljw_1atom-short-abw-2017jan-alpha \
  clear_slit2_ljw_2atms-long-abw-2017jan-alpha \
  clear_slit2_ljw_1atom-long-abw-2017jan-alpha \
  clear_slit1_ljw_1atom-long-abw-2017jan-alpha \
  clear_slit_mfa2-short-abh-2017feb-alpha \
  clear_slit_mfa1-short-abh-2017feb-alpha \


.PHONY: fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_003" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
clean_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
clear_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha: clean_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
	@if ls -d fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	    cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
update_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
pack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr3-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ./itr3-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
unpack_fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1mol-2atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_lj_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_003" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
clean_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
clear_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha: clean_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
	@if ls -d fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	    cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
update_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
pack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr3-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ./itr3-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
unpack_fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_lj_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_003" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
clean_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
clear_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha: clean_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
	@if ls -d fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	    cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
update_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
pack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr3-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ./itr3-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
unpack_fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/'"; \
	if cd fed_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2mol-1atms_mols-moves-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_lj_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_003" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
clean_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
clear_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha: clean_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
	@if ls -d fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	    cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
update_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
pack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr3-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ./itr3-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha
unpack_fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_1mol-2atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_ljsr_VdWdir_1mol_atms-itr3-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_003" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
clean_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
clear_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha: clean_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
	@if ls -d fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	    cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
update_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
pack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr3-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ./itr3-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha
unpack_fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_atom-moves-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_ljsr_VdWdir_2mol_atms-itr3-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_003" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
clean_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
clear_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha: clean_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
	@if ls -d fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	    cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
update_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/itr3-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
pack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr3-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ./itr3-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha
unpack_fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/'"; \
	if cd fed_ljsr/R-12_e1.0kT1K-d6.1A_Rcut20.0_2mol-1atms_mols-moves-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_ljsr_VdWdir_2mol_mols-itr3-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/FEDDAT.000_001" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or FEDDAT.000_001 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
clean_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/'"; \
	if cd fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
clear_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha: clean_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
	@if ls -d fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/'"; \
	    cd fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
update_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/'"; \
	if cd fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/itr1-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
pack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/'"; \
	if cd fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr1-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz ./itr1-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha
unpack_fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/'"; \
	if cd fed_ljr_coul/R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_ljrc_VdWdir_1mol_atms-itr1-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_hs_cubic-short-abw-2017jan-alpha
npt_hs_cubic-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " npt_hs_cubic-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in npt_hs_cubic/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd npt_hs_cubic/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_hs_cubic-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_npt_hs_cubic-short-abw-2017jan-alpha
clean_npt_hs_cubic-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'npt_hs_cubic-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_hs_cubic/short-abw-2017jan-alpha/'"; \
	if cd npt_hs_cubic/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_hs_cubic-short-abw-2017jan-alpha
clear_npt_hs_cubic-short-abw-2017jan-alpha: clean_npt_hs_cubic-short-abw-2017jan-alpha
	@if ls -d npt_hs_cubic/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_hs_cubic/npt_hs_cubic-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'npt_hs_cubic-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'npt_hs_cubic/short-abw-2017jan-alpha/'"; \
	    cd npt_hs_cubic/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv npt_hs_cubic/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'npt_hs_cubic-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_hs_cubic-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_hs_cubic-short-abw-2017jan-alpha
update_npt_hs_cubic-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_hs_cubic-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_hs_cubic/short-abw-2017jan-alpha/'"; \
	if cd npt_hs_cubic/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_hs_cubic-short-abw-2017jan-alpha
pack_npt_hs_cubic-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'npt_hs_cubic-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_hs_cubic/'"; \
	if cd npt_hs_cubic/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_hs_cubic-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_hs_cubic-short-abw-2017jan-alpha
unpack_npt_hs_cubic-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'npt_hs_cubic-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_hs_cubic/'"; \
	if cd npt_hs_cubic/ ; then \
	  echo ; \
	  if [ -e npt_hs_cubic-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf npt_hs_cubic-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'npt_hs_cubic-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_hs_ortho-short-abw-2017jan-alpha
npt_hs_ortho-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " npt_hs_ortho-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in npt_hs_ortho/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd npt_hs_ortho/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_hs_ortho-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_npt_hs_ortho-short-abw-2017jan-alpha
clean_npt_hs_ortho-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'npt_hs_ortho-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_hs_ortho/short-abw-2017jan-alpha/'"; \
	if cd npt_hs_ortho/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_hs_ortho-short-abw-2017jan-alpha
clear_npt_hs_ortho-short-abw-2017jan-alpha: clean_npt_hs_ortho-short-abw-2017jan-alpha
	@if ls -d npt_hs_ortho/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_hs_ortho/npt_hs_ortho-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'npt_hs_ortho-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'npt_hs_ortho/short-abw-2017jan-alpha/'"; \
	    cd npt_hs_ortho/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv npt_hs_ortho/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'npt_hs_ortho-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_hs_ortho-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_hs_ortho-short-abw-2017jan-alpha
update_npt_hs_ortho-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_hs_ortho-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_hs_ortho/short-abw-2017jan-alpha/'"; \
	if cd npt_hs_ortho/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_hs_ortho-short-abw-2017jan-alpha
pack_npt_hs_ortho-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'npt_hs_ortho-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_hs_ortho/'"; \
	if cd npt_hs_ortho/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_hs_ortho-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_hs_ortho-short-abw-2017jan-alpha
unpack_npt_hs_ortho-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'npt_hs_ortho-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_hs_ortho/'"; \
	if cd npt_hs_ortho/ ; then \
	  echo ; \
	  if [ -e npt_hs_ortho-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf npt_hs_ortho-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'npt_hs_ortho-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_lj-short-abw-2017jan-alpha
npt_lj-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " npt_lj-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in npt_lj/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd npt_lj/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_lj-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_npt_lj-short-abw-2017jan-alpha
clean_npt_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'npt_lj-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_lj/short-abw-2017jan-alpha/'"; \
	if cd npt_lj/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_lj-short-abw-2017jan-alpha
clear_npt_lj-short-abw-2017jan-alpha: clean_npt_lj-short-abw-2017jan-alpha
	@if ls -d npt_lj/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_lj/npt_lj-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'npt_lj-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'npt_lj/short-abw-2017jan-alpha/'"; \
	    cd npt_lj/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv npt_lj/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'npt_lj-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_lj-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_lj-short-abw-2017jan-alpha
update_npt_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_lj-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_lj/short-abw-2017jan-alpha/'"; \
	if cd npt_lj/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_lj-short-abw-2017jan-alpha
pack_npt_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'npt_lj-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_lj/'"; \
	if cd npt_lj/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_lj-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_lj-short-abw-2017jan-alpha
unpack_npt_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'npt_lj-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_lj/'"; \
	if cd npt_lj/ ; then \
	  echo ; \
	  if [ -e npt_lj-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf npt_lj-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'npt_lj-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_water_spce-short-abw-2017jan-alpha
npt_water_spce-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " npt_water_spce-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in npt_water_spce/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd npt_water_spce/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_water_spce-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_npt_water_spce-short-abw-2017jan-alpha
clean_npt_water_spce-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'npt_water_spce-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_water_spce/short-abw-2017jan-alpha/'"; \
	if cd npt_water_spce/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_water_spce-short-abw-2017jan-alpha
clear_npt_water_spce-short-abw-2017jan-alpha: clean_npt_water_spce-short-abw-2017jan-alpha
	@if ls -d npt_water_spce/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_water_spce/npt_water_spce-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'npt_water_spce-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'npt_water_spce/short-abw-2017jan-alpha/'"; \
	    cd npt_water_spce/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv npt_water_spce/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'npt_water_spce-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_water_spce-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_water_spce-short-abw-2017jan-alpha
update_npt_water_spce-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_water_spce-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'npt_water_spce/short-abw-2017jan-alpha/'"; \
	if cd npt_water_spce/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_water_spce-short-abw-2017jan-alpha
pack_npt_water_spce-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'npt_water_spce-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_water_spce/'"; \
	if cd npt_water_spce/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_water_spce-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_water_spce-short-abw-2017jan-alpha
unpack_npt_water_spce-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'npt_water_spce-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'npt_water_spce/'"; \
	if cd npt_water_spce/ ; then \
	  echo ; \
	  if [ -e npt_water_spce-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf npt_water_spce-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'npt_water_spce-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gcmc_co2_zeolite-short-abw-2017jan-alpha
gcmc_co2_zeolite-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " gcmc_co2_zeolite-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in gcmc_co2_zeolite/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd gcmc_co2_zeolite/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gcmc_co2_zeolite-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_gcmc_co2_zeolite-short-abw-2017jan-alpha
clean_gcmc_co2_zeolite-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'gcmc_co2_zeolite-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'gcmc_co2_zeolite/short-abw-2017jan-alpha/'"; \
	if cd gcmc_co2_zeolite/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gcmc_co2_zeolite-short-abw-2017jan-alpha
clear_gcmc_co2_zeolite-short-abw-2017jan-alpha: clean_gcmc_co2_zeolite-short-abw-2017jan-alpha
	@if ls -d gcmc_co2_zeolite/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e gcmc_co2_zeolite/gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'gcmc_co2_zeolite/short-abw-2017jan-alpha/'"; \
	    cd gcmc_co2_zeolite/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv gcmc_co2_zeolite/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gcmc_co2_zeolite-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gcmc_co2_zeolite-short-abw-2017jan-alpha
update_gcmc_co2_zeolite-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'gcmc_co2_zeolite-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'gcmc_co2_zeolite/short-abw-2017jan-alpha/'"; \
	if cd gcmc_co2_zeolite/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gcmc_co2_zeolite-short-abw-2017jan-alpha
pack_gcmc_co2_zeolite-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'gcmc_co2_zeolite-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'gcmc_co2_zeolite/'"; \
	if cd gcmc_co2_zeolite/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gcmc_co2_zeolite-short-abw-2017jan-alpha
unpack_gcmc_co2_zeolite-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'gcmc_co2_zeolite-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'gcmc_co2_zeolite/'"; \
	if cd gcmc_co2_zeolite/ ; then \
	  echo ; \
	  if [ -e gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'gcmc_co2_zeolite-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gcmc_hs-short-abw-2017jan-alpha
gcmc_hs-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " gcmc_hs-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in gcmc_hs/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd gcmc_hs/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gcmc_hs-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_gcmc_hs-short-abw-2017jan-alpha
clean_gcmc_hs-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'gcmc_hs-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'gcmc_hs/short-abw-2017jan-alpha/'"; \
	if cd gcmc_hs/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gcmc_hs-short-abw-2017jan-alpha
clear_gcmc_hs-short-abw-2017jan-alpha: clean_gcmc_hs-short-abw-2017jan-alpha
	@if ls -d gcmc_hs/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e gcmc_hs/gcmc_hs-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'gcmc_hs-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'gcmc_hs/short-abw-2017jan-alpha/'"; \
	    cd gcmc_hs/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv gcmc_hs/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'gcmc_hs-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gcmc_hs-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gcmc_hs-short-abw-2017jan-alpha
update_gcmc_hs-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'gcmc_hs-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'gcmc_hs/short-abw-2017jan-alpha/'"; \
	if cd gcmc_hs/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gcmc_hs-short-abw-2017jan-alpha
pack_gcmc_hs-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'gcmc_hs-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'gcmc_hs/'"; \
	if cd gcmc_hs/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gcmc_hs-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gcmc_hs-short-abw-2017jan-alpha
unpack_gcmc_hs-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'gcmc_hs-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'gcmc_hs/'"; \
	if cd gcmc_hs/ ; then \
	  echo ; \
	  if [ -e gcmc_hs-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf gcmc_hs-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'gcmc_hs-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gcmc_lj-short-abw-2017jan-alpha
gcmc_lj-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " gcmc_lj-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in gcmc_lj/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd gcmc_lj/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gcmc_lj-short-abw-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_gcmc_lj-short-abw-2017jan-alpha
clean_gcmc_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'gcmc_lj-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'gcmc_lj/short-abw-2017jan-alpha/'"; \
	if cd gcmc_lj/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gcmc_lj-short-abw-2017jan-alpha
clear_gcmc_lj-short-abw-2017jan-alpha: clean_gcmc_lj-short-abw-2017jan-alpha
	@if ls -d gcmc_lj/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e gcmc_lj/gcmc_lj-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'gcmc_lj-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'gcmc_lj/short-abw-2017jan-alpha/'"; \
	    cd gcmc_lj/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv gcmc_lj/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'gcmc_lj-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gcmc_lj-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gcmc_lj-short-abw-2017jan-alpha
update_gcmc_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'gcmc_lj-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'gcmc_lj/short-abw-2017jan-alpha/'"; \
	if cd gcmc_lj/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gcmc_lj-short-abw-2017jan-alpha
pack_gcmc_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'gcmc_lj-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'gcmc_lj/'"; \
	if cd gcmc_lj/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gcmc_lj-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gcmc_lj-short-abw-2017jan-alpha
unpack_gcmc_lj-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'gcmc_lj-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'gcmc_lj/'"; \
	if cd gcmc_lj/ ; then \
	  echo ; \
	  if [ -e gcmc_lj-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf gcmc_lj-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'gcmc_lj-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit1_ljw_1atom-long-abw-2017jan-alpha
slit1_ljw_1atom-long-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " slit1_ljw_1atom-long-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit1_ljw_1atom-long-abw-2017jan-alpha'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_slit1_ljw_1atom-long-abw-2017jan-alpha
clean_slit1_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'slit1_ljw_1atom-long-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_slit1_ljw_1atom-long-abw-2017jan-alpha
clear_slit1_ljw_1atom-long-abw-2017jan-alpha: clean_slit1_ljw_1atom-long-abw-2017jan-alpha
	@if ls -d slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/slit1_ljw_1atom-long-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'slit1_ljw_1atom-long-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/'"; \
	    cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'slit1_ljw_1atom-long-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit1_ljw_1atom-long-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit1_ljw_1atom-long-abw-2017jan-alpha
update_slit1_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit1_ljw_1atom-long-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/long-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit1_ljw_1atom-long-abw-2017jan-alpha
pack_slit1_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'slit1_ljw_1atom-long-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/ ; then \
	  echo ; \
	  if ls ./long-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit1_ljw_1atom-long-abw-2017jan-alpha.tgz ./long-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit1_ljw_1atom-long-abw-2017jan-alpha
unpack_slit1_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'slit1_ljw_1atom-long-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/ ; then \
	  echo ; \
	  if [ -e slit1_ljw_1atom-long-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf slit1_ljw_1atom-long-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'slit1_ljw_1atom-long-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit2_ljw_1atom-long-abw-2017jan-alpha
slit2_ljw_1atom-long-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " slit2_ljw_1atom-long-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit2_ljw_1atom-long-abw-2017jan-alpha'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_slit2_ljw_1atom-long-abw-2017jan-alpha
clean_slit2_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'slit2_ljw_1atom-long-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_slit2_ljw_1atom-long-abw-2017jan-alpha
clear_slit2_ljw_1atom-long-abw-2017jan-alpha: clean_slit2_ljw_1atom-long-abw-2017jan-alpha
	@if ls -d slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/slit2_ljw_1atom-long-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'slit2_ljw_1atom-long-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/'"; \
	    cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'slit2_ljw_1atom-long-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit2_ljw_1atom-long-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit2_ljw_1atom-long-abw-2017jan-alpha
update_slit2_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit2_ljw_1atom-long-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit2_ljw_1atom-long-abw-2017jan-alpha
pack_slit2_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'slit2_ljw_1atom-long-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if ls ./long-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit2_ljw_1atom-long-abw-2017jan-alpha.tgz ./long-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit2_ljw_1atom-long-abw-2017jan-alpha
unpack_slit2_ljw_1atom-long-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'slit2_ljw_1atom-long-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if [ -e slit2_ljw_1atom-long-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf slit2_ljw_1atom-long-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'slit2_ljw_1atom-long-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit2_ljw_2atms-long-abw-2017jan-alpha
slit2_ljw_2atms-long-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " slit2_ljw_2atms-long-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit2_ljw_2atms-long-abw-2017jan-alpha'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_slit2_ljw_2atms-long-abw-2017jan-alpha
clean_slit2_ljw_2atms-long-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'slit2_ljw_2atms-long-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_slit2_ljw_2atms-long-abw-2017jan-alpha
clear_slit2_ljw_2atms-long-abw-2017jan-alpha: clean_slit2_ljw_2atms-long-abw-2017jan-alpha
	@if ls -d slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/slit2_ljw_2atms-long-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'slit2_ljw_2atms-long-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/'"; \
	    cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'slit2_ljw_2atms-long-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit2_ljw_2atms-long-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit2_ljw_2atms-long-abw-2017jan-alpha
update_slit2_ljw_2atms-long-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit2_ljw_2atms-long-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/long-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit2_ljw_2atms-long-abw-2017jan-alpha
pack_slit2_ljw_2atms-long-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'slit2_ljw_2atms-long-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if ls ./long-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit2_ljw_2atms-long-abw-2017jan-alpha.tgz ./long-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit2_ljw_2atms-long-abw-2017jan-alpha
unpack_slit2_ljw_2atms-long-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'slit2_ljw_2atms-long-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if [ -e slit2_ljw_2atms-long-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf slit2_ljw_2atms-long-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'slit2_ljw_2atms-long-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit1_ljw_1atom-short-abw-2017jan-alpha
slit1_ljw_1atom-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " slit1_ljw_1atom-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit1_ljw_1atom-short-abw-2017jan-alpha'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_slit1_ljw_1atom-short-abw-2017jan-alpha
clean_slit1_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'slit1_ljw_1atom-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_slit1_ljw_1atom-short-abw-2017jan-alpha
clear_slit1_ljw_1atom-short-abw-2017jan-alpha: clean_slit1_ljw_1atom-short-abw-2017jan-alpha
	@if ls -d slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/slit1_ljw_1atom-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'slit1_ljw_1atom-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/'"; \
	    cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'slit1_ljw_1atom-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit1_ljw_1atom-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit1_ljw_1atom-short-abw-2017jan-alpha
update_slit1_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit1_ljw_1atom-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit1_ljw_1atom-short-abw-2017jan-alpha
pack_slit1_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'slit1_ljw_1atom-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit1_ljw_1atom-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit1_ljw_1atom-short-abw-2017jan-alpha
unpack_slit1_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'slit1_ljw_1atom-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit1_LJw/ ; then \
	  echo ; \
	  if [ -e slit1_ljw_1atom-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf slit1_ljw_1atom-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'slit1_ljw_1atom-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit2_ljw_1atom-short-abw-2017jan-alpha
slit2_ljw_1atom-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " slit2_ljw_1atom-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit2_ljw_1atom-short-abw-2017jan-alpha'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_slit2_ljw_1atom-short-abw-2017jan-alpha
clean_slit2_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'slit2_ljw_1atom-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_slit2_ljw_1atom-short-abw-2017jan-alpha
clear_slit2_ljw_1atom-short-abw-2017jan-alpha: clean_slit2_ljw_1atom-short-abw-2017jan-alpha
	@if ls -d slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/slit2_ljw_1atom-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'slit2_ljw_1atom-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/'"; \
	    cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'slit2_ljw_1atom-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit2_ljw_1atom-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit2_ljw_1atom-short-abw-2017jan-alpha
update_slit2_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit2_ljw_1atom-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit2_ljw_1atom-short-abw-2017jan-alpha
pack_slit2_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'slit2_ljw_1atom-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit2_ljw_1atom-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit2_ljw_1atom-short-abw-2017jan-alpha
unpack_slit2_ljw_1atom-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'slit2_ljw_1atom-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_1atom-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if [ -e slit2_ljw_1atom-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf slit2_ljw_1atom-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'slit2_ljw_1atom-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit2_ljw_2atms-short-abw-2017jan-alpha
slit2_ljw_2atms-short-abw-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " slit2_ljw_2atms-short-abw-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      if [ -f "ZDENSY.000" ]; then \
		echo "*** Check if ZDENSY.000 and benchmark/ZDENSY.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q ZDENSY.000 benchmark/ZDENSY.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit2_ljw_2atms-short-abw-2017jan-alpha'"; \
	      else \
		echo "*** test ZDENSY.000 not found - TEST INCOMPLETE - check input ***"; \
	      fi ;\
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_slit2_ljw_2atms-short-abw-2017jan-alpha
clean_slit2_ljw_2atms-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'slit2_ljw_2atms-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_slit2_ljw_2atms-short-abw-2017jan-alpha
clear_slit2_ljw_2atms-short-abw-2017jan-alpha: clean_slit2_ljw_2atms-short-abw-2017jan-alpha
	@if ls -d slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/slit2_ljw_2atms-short-abw-2017jan-alpha.tgz ]; then \
	    echo "Package 'slit2_ljw_2atms-short-abw-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/'"; \
	    cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; \
	  else \
	    echo "Package 'slit2_ljw_2atms-short-abw-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit2_ljw_2atms-short-abw-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit2_ljw_2atms-short-abw-2017jan-alpha
update_slit2_ljw_2atms-short-abw-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit2_ljw_2atms-short-abw-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/short-abw-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit2_ljw_2atms-short-abw-2017jan-alpha
pack_slit2_ljw_2atms-short-abw-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'slit2_ljw_2atms-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if ls ./short-abw-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit2_ljw_2atms-short-abw-2017jan-alpha.tgz ./short-abw-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit2_ljw_2atms-short-abw-2017jan-alpha
unpack_slit2_ljw_2atms-short-abw-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'slit2_ljw_2atms-short-abw-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/'"; \
	if cd slit_lj/LJ_e1.0kT1K-d3.0A_Rcut12.0_2atms-VdWdir-Slit2_LJw/ ; then \
	  echo ; \
	  if [ -e slit2_ljw_2atms-short-abw-2017jan-alpha.tgz ]; then \
	    tar -xvf slit2_ljw_2atms-short-abw-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'slit2_ljw_2atms-short-abw-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
clean_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
clear_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha: clean_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
	@if ls -d fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_hscn/hs20q20-val1-VdWdir/fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/'"; \
	    cd fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
update_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr1-abh-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
pack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr1-abh-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz ./itr1-abh-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha
unpack_fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr1-abh-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
clean_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
clear_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha: clean_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
	@if ls -d fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_hscn/hs20q20-val2-VdWdir/fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/'"; \
	    cd fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
update_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr1-abh-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
pack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr1-abh-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz ./itr1-abh-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha
unpack_fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr1-abh-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
clean_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
clear_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha: clean_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
	@if ls -d fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_hscn/hs20q20-val1-VdWdir/fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/'"; \
	    cd fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
update_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr5-abh-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
pack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr5-abh-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz ./itr5-abh-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha
unpack_fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr5-abh-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "FEDDAT.000_001" ]; then \
	      for file in FEDDAT.000_*; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
clean_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
clear_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha: clean_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
	@if ls -d fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_hscn/hs20q20-val2-VdWdir/fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/'"; \
	    cd fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
update_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr5-abh-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
pack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr5-abh-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz ./itr5-abh-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha
unpack_fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr5-abh-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
clean_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
clear_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha: clean_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
	@if ls -d fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_hscn/hs20q20-val1-VdWdir/fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/'"; \
	    cd fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
update_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/itr2-abh-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
pack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr2-abh-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz ./itr2-abh-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha
unpack_fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val1-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val1-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val1-itr2-abh-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha:
	@echo ;\
	echo "********************************************************************"; \
	echo " fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha "; \
	echo "********************************************************************"; \
	echo " in fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
clean_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: removing the data for test 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi ; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
clear_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha: clean_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
	@if ls -d fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e fed_hscn/hs20q20-val2-VdWdir/fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz ]; then \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/'"; \
	    cd fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
update_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha'"; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/itr2-abh-2017jan-alpha/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
pack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "Packing benchmark 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/ ; then \
	  echo ; \
	  if ls ./itr2-abh-2017jan-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz ./itr2-abh-2017jan-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha
unpack_fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha:
	@echo ; \
	echo "Unpacking benchmark 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha'" ; \
	echo ; \
	echo "Entering directory 'fed_hscn/hs20q20-val2-VdWdir/'"; \
	if cd fed_hscn/hs20q20-val2-VdWdir/ ; then \
	  echo ; \
	  if [ -e fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz ]; then \
	    tar -xvf fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz ; \
	  else \
	    echo "Package 'fed_hscn_VdWdir_hs20q20_val2-itr2-abh-2017jan-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm-main
ewald_nacl_atm-main:
	@echo ;\
	echo "********************************************************************"; \
	echo " ewald_nacl_atm-main "; \
	echo "********************************************************************"; \
	echo " in ewald_nacl_atm/main/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd ewald_nacl_atm/main/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_ewald_nacl_atm-main
clean_ewald_nacl_atm-main:
	@echo ; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm-main'"; \
	echo ; \
	echo "Entering directory 'ewald_nacl_atm/main/'"; \
	if cd ewald_nacl_atm/main/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm-main
clear_ewald_nacl_atm-main: clean_ewald_nacl_atm-main
	@if ls -d ewald_nacl_atm/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm/ewald_nacl_atm-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'ewald_nacl_atm/main/'"; \
	    cd ewald_nacl_atm/main/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv ewald_nacl_atm/main/; \
	  else \
	    echo "Package 'ewald_nacl_atm-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm-main
update_ewald_nacl_atm-main:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm-main'"; \
	echo ; \
	echo "Entering directory 'ewald_nacl_atm/main/'"; \
	if cd ewald_nacl_atm/main/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm-main
pack_ewald_nacl_atm-main:
	@echo ; \
	echo "Packing benchmark 'ewald_nacl_atm-main'" ; \
	echo ; \
	echo "Entering directory 'ewald_nacl_atm/'"; \
	if cd ewald_nacl_atm/ ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm-main
unpack_ewald_nacl_atm-main:
	@echo ; \
	echo "Unpacking benchmark 'ewald_nacl_atm-main'" ; \
	echo ; \
	echo "Entering directory 'ewald_nacl_atm/'"; \
	if cd ewald_nacl_atm/ ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_mol-main
ewald_nacl_mol-main:
	@echo ;\
	echo "********************************************************************"; \
	echo " ewald_nacl_mol-main "; \
	echo "********************************************************************"; \
	echo " in ewald_nacl_mol/main/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd ewald_nacl_mol/main/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_mol-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_ewald_nacl_mol-main
clean_ewald_nacl_mol-main:
	@echo ; \
	echo "WARNING: removing the data for test 'ewald_nacl_mol-main'"; \
	echo ; \
	echo "Entering directory 'ewald_nacl_mol/main/'"; \
	if cd ewald_nacl_mol/main/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_mol-main
clear_ewald_nacl_mol-main: clean_ewald_nacl_mol-main
	@if ls -d ewald_nacl_mol/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_mol/ewald_nacl_mol-main.tgz ]; then \
	    echo "Package 'ewald_nacl_mol-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'ewald_nacl_mol/main/'"; \
	    cd ewald_nacl_mol/main/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv ewald_nacl_mol/main/; \
	  else \
	    echo "Package 'ewald_nacl_mol-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_mol-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_mol-main
update_ewald_nacl_mol-main:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_mol-main'"; \
	echo ; \
	echo "Entering directory 'ewald_nacl_mol/main/'"; \
	if cd ewald_nacl_mol/main/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_mol-main
pack_ewald_nacl_mol-main:
	@echo ; \
	echo "Packing benchmark 'ewald_nacl_mol-main'" ; \
	echo ; \
	echo "Entering directory 'ewald_nacl_mol/'"; \
	if cd ewald_nacl_mol/ ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_mol-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_mol-main
unpack_ewald_nacl_mol-main:
	@echo ; \
	echo "Unpacking benchmark 'ewald_nacl_mol-main'" ; \
	echo ; \
	echo "Entering directory 'ewald_nacl_mol/'"; \
	if cd ewald_nacl_mol/ ; then \
	  echo ; \
	  if [ -e ewald_nacl_mol-main.tgz ]; then \
	    tar -xvf ewald_nacl_mol-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_mol-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: psmc_sync_ewald_atm-main
psmc_sync_ewald_atm-main:
	@echo ;\
	echo "********************************************************************"; \
	echo " psmc_sync_ewald_atm-main "; \
	echo "********************************************************************"; \
	echo " in psmc_sync_ewald_atm/main/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd psmc_sync_ewald_atm/main/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PSDATA.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ] && [ -f "input/CONFIG.1" ] && [ -f "input/CONFIG.2" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PSDATA.000" ] && [ -f "PTFILE.000" ]; then \
	      for file in PSDATA.000 PTFILE.000; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'psmc_sync_ewald_atm-main'"; \
	    else \
		echo "*** test OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_psmc_sync_ewald_atm-main
clean_psmc_sync_ewald_atm-main:
	@echo ; \
	echo "WARNING: removing the data for test 'psmc_sync_ewald_atm-main'"; \
	echo ; \
	echo "Entering directory 'psmc_sync_ewald_atm/main/'"; \
	if cd psmc_sync_ewald_atm/main/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_psmc_sync_ewald_atm-main
clear_psmc_sync_ewald_atm-main: clean_psmc_sync_ewald_atm-main
	@if ls -d psmc_sync_ewald_atm/main 2>/dev/null 1>/dev/null; then \
	  if [ -e psmc_sync_ewald_atm/psmc_sync_ewald_atm-main.tgz ]; then \
	    echo "Package 'psmc_sync_ewald_atm-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'psmc_sync_ewald_atm/main/'"; \
	    cd psmc_sync_ewald_atm/main/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv psmc_sync_ewald_atm/main/; \
	  else \
	    echo "Package 'psmc_sync_ewald_atm-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'psmc_sync_ewald_atm-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_psmc_sync_ewald_atm-main
update_psmc_sync_ewald_atm-main:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'psmc_sync_ewald_atm-main'"; \
	echo ; \
	echo "Entering directory 'psmc_sync_ewald_atm/main/'"; \
	if cd psmc_sync_ewald_atm/main/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f CONFIG.1 ] || [ ! -f CONFIG.2 ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ] || [ ! -f PSDATA.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###, PSDATA.000"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_psmc_sync_ewald_atm-main
pack_psmc_sync_ewald_atm-main:
	@echo ; \
	echo "Packing benchmark 'psmc_sync_ewald_atm-main'" ; \
	echo ; \
	echo "Entering directory 'psmc_sync_ewald_atm/'"; \
	if cd psmc_sync_ewald_atm/ ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf psmc_sync_ewald_atm-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_psmc_sync_ewald_atm-main
unpack_psmc_sync_ewald_atm-main:
	@echo ; \
	echo "Unpacking benchmark 'psmc_sync_ewald_atm-main'" ; \
	echo ; \
	echo "Entering directory 'psmc_sync_ewald_atm/'"; \
	if cd psmc_sync_ewald_atm/ ; then \
	  echo ; \
	  if [ -e psmc_sync_ewald_atm-main.tgz ]; then \
	    tar -xvf psmc_sync_ewald_atm-main.tgz ; \
	  else \
	    echo "Package 'psmc_sync_ewald_atm-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nist_spce_water-main
nist_spce_water-main:
	@echo ;\
	echo "********************************************************************"; \
	echo " nist_spce_water-main "; \
	echo "********************************************************************"; \
	echo " in nist_spce_water/main/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd nist_spce_water/main/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "*** Check if PTFILE.000 and benchmark/PTFILE.000 differ ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q PTFILE.000 benchmark/PTFILE.000 || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nist_spce_water-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_nist_spce_water-main
clean_nist_spce_water-main:
	@echo ; \
	echo "WARNING: removing the data for test 'nist_spce_water-main'"; \
	echo ; \
	echo "Entering directory 'nist_spce_water/main/'"; \
	if cd nist_spce_water/main/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nist_spce_water-main
clear_nist_spce_water-main: clean_nist_spce_water-main
	@if ls -d nist_spce_water/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nist_spce_water/nist_spce_water-main.tgz ]; then \
	    echo "Package 'nist_spce_water-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'nist_spce_water/main/'"; \
	    cd nist_spce_water/main/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv nist_spce_water/main/; \
	  else \
	    echo "Package 'nist_spce_water-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nist_spce_water-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nist_spce_water-main
update_nist_spce_water-main:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'nist_spce_water-main'"; \
	echo ; \
	echo "Entering directory 'nist_spce_water/main/'"; \
	if cd nist_spce_water/main/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nist_spce_water-main
pack_nist_spce_water-main:
	@echo ; \
	echo "Packing benchmark 'nist_spce_water-main'" ; \
	echo ; \
	echo "Entering directory 'nist_spce_water/'"; \
	if cd nist_spce_water/ ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nist_spce_water-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nist_spce_water-main
unpack_nist_spce_water-main:
	@echo ; \
	echo "Unpacking benchmark 'nist_spce_water-main'" ; \
	echo ; \
	echo "Entering directory 'nist_spce_water/'"; \
	if cd nist_spce_water/ ; then \
	  echo ; \
	  if [ -e nist_spce_water-main.tgz ]; then \
	    tar -xvf nist_spce_water-main.tgz ; \
	  else \
	    echo "Package 'nist_spce_water-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit_mfa1-short-abh-2017feb-alpha
slit_mfa1-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa1/short-abh-2017feb-alpha"; \
	echo "********************************************************************"; \
	echo " slit_mfa1-short-abh-2017feb-alpha "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit_mfa1-short-abh-2017feb-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_slit_mfa1-short-abh-2017feb-alpha
clean_slit_mfa1-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa1/short-abh-2017feb-alpha"; \
	echo "WARNING: removing the data for test 'slit_mfa1-short-abh-2017feb-alpha'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_slit_mfa1-short-abh-2017feb-alpha
clear_slit_mfa1-short-abh-2017feb-alpha: clean_slit_mfa1-short-abh-2017feb-alpha
	@echo ; \
	export DIR="slit_mfa1/short-abh-2017feb-alpha"; \
	if ls -d slit_mfa1/short-abh-2017feb-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_mfa1/slit_mfa1-short-abh-2017feb-alpha.tgz ]; then \
	    echo "Package 'slit_mfa1-short-abh-2017feb-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'slit_mfa1-short-abh-2017feb-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit_mfa1-short-abh-2017feb-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit_mfa1-short-abh-2017feb-alpha
update_slit_mfa1-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa1/short-abh-2017feb-alpha"; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit_mfa1-short-abh-2017feb-alpha'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit_mfa1-short-abh-2017feb-alpha
pack_slit_mfa1-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa1"; \
	echo "Packing benchmark 'slit_mfa1-short-abh-2017feb-alpha'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short-abh-2017feb-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit_mfa1-short-abh-2017feb-alpha.tgz ./short-abh-2017feb-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit_mfa1-short-abh-2017feb-alpha
unpack_slit_mfa1-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa1"; \
	echo "Unpacking benchmark 'slit_mfa1-short-abh-2017feb-alpha'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e slit_mfa1-short-abh-2017feb-alpha.tgz ]; then \
	    tar -xvf slit_mfa1-short-abh-2017feb-alpha.tgz ; \
	  else \
	    echo "Package 'slit_mfa1-short-abh-2017feb-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: slit_mfa2-short-abh-2017feb-alpha
slit_mfa2-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa2/short-abh-2017feb-alpha"; \
	echo "********************************************************************"; \
	echo " slit_mfa2-short-abh-2017feb-alpha "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'slit_mfa2-short-abh-2017feb-alpha'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_slit_mfa2-short-abh-2017feb-alpha
clean_slit_mfa2-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa2/short-abh-2017feb-alpha"; \
	echo "WARNING: removing the data for test 'slit_mfa2-short-abh-2017feb-alpha'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_slit_mfa2-short-abh-2017feb-alpha
clear_slit_mfa2-short-abh-2017feb-alpha: clean_slit_mfa2-short-abh-2017feb-alpha
	@echo ; \
	export DIR="slit_mfa2/short-abh-2017feb-alpha"; \
	if ls -d slit_mfa2/short-abh-2017feb-alpha 2>/dev/null 1>/dev/null; then \
	  if [ -e slit_mfa2/slit_mfa2-short-abh-2017feb-alpha.tgz ]; then \
	    echo "Package 'slit_mfa2-short-abh-2017feb-alpha.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'slit_mfa2-short-abh-2017feb-alpha.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'slit_mfa2-short-abh-2017feb-alpha' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_slit_mfa2-short-abh-2017feb-alpha
update_slit_mfa2-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa2/short-abh-2017feb-alpha"; \
	echo "WARNING: replacing benchmark with the latest data for test 'slit_mfa2-short-abh-2017feb-alpha'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_slit_mfa2-short-abh-2017feb-alpha
pack_slit_mfa2-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa2"; \
	echo "Packing benchmark 'slit_mfa2-short-abh-2017feb-alpha'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short-abh-2017feb-alpha/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf slit_mfa2-short-abh-2017feb-alpha.tgz ./short-abh-2017feb-alpha/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_slit_mfa2-short-abh-2017feb-alpha
unpack_slit_mfa2-short-abh-2017feb-alpha:
	@echo ; \
	export DIR="slit_mfa2"; \
	echo "Unpacking benchmark 'slit_mfa2-short-abh-2017feb-alpha'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e slit_mfa2-short-abh-2017feb-alpha.tgz ]; then \
	    tar -xvf slit_mfa2-short-abh-2017feb-alpha.tgz ; \
	  else \
	    echo "Package 'slit_mfa2-short-abh-2017feb-alpha.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: psmc_hs_nvt-short
psmc_hs_nvt-short:
	@echo ;\
	echo "********************************************************************"; \
	echo " psmc_hs_nvt-short "; \
	echo "********************************************************************"; \
	echo " in psmc_hs_nvt/short/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd psmc_hs_nvt/short/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PSDATA.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ] && [ -f "input/CONFIG.1" ] && [ -f "input/CONFIG.2" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PSDATA.000" ] && [ -f "PTFILE.000" ]; then \
	      for file in PSDATA.000 PTFILE.000; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'psmc_hs_nvt-short'"; \
	    else \
		echo "*** test OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_psmc_hs_nvt-short
clean_psmc_hs_nvt-short:
	@echo ; \
	echo "WARNING: removing the data for test 'psmc_hs_nvt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_hs_nvt/short/'"; \
	if cd psmc_hs_nvt/short/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_psmc_hs_nvt-short
clear_psmc_hs_nvt-short: clean_psmc_hs_nvt-short
	@if ls -d psmc_hs_nvt/short 2>/dev/null 1>/dev/null; then \
	  if [ -e psmc_hs_nvt/psmc_hs_nvt-short.tgz ]; then \
	    echo "Package 'psmc_hs_nvt-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'psmc_hs_nvt/short/'"; \
	    cd psmc_hs_nvt/short/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv psmc_hs_nvt/short/; \
	  else \
	    echo "Package 'psmc_hs_nvt-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'psmc_hs_nvt-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_psmc_hs_nvt-short
update_psmc_hs_nvt-short:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'psmc_hs_nvt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_hs_nvt/short/'"; \
	if cd psmc_hs_nvt/short/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f CONFIG.1 ] || [ ! -f CONFIG.2 ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ] || [ ! -f PSDATA.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###, PSDATA.000"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_psmc_hs_nvt-short
pack_psmc_hs_nvt-short:
	@echo ; \
	echo "Packing benchmark 'psmc_hs_nvt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_hs_nvt/'"; \
	if cd psmc_hs_nvt/ ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf psmc_hs_nvt-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_psmc_hs_nvt-short
unpack_psmc_hs_nvt-short:
	@echo ; \
	echo "Unpacking benchmark 'psmc_hs_nvt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_hs_nvt/'"; \
	if cd psmc_hs_nvt/ ; then \
	  echo ; \
	  if [ -e psmc_hs_nvt-short.tgz ]; then \
	    tar -xvf psmc_hs_nvt-short.tgz ; \
	  else \
	    echo "Package 'psmc_hs_nvt-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: psmc_hs_npt-short
psmc_hs_npt-short:
	@echo ;\
	echo "********************************************************************"; \
	echo " psmc_hs_npt-short "; \
	echo "********************************************************************"; \
	echo " in psmc_hs_npt/short/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd psmc_hs_npt/short/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PSDATA.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ] && [ -f "input/CONFIG.1" ] && [ -f "input/CONFIG.2" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PSDATA.000" ] && [ -f "PTFILE.000" ]; then \
	      for file in PSDATA.000 PTFILE.000; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'psmc_hs_npt-short'"; \
	    else \
		echo "*** test OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_psmc_hs_npt-short
clean_psmc_hs_npt-short:
	@echo ; \
	echo "WARNING: removing the data for test 'psmc_hs_npt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_hs_npt/short/'"; \
	if cd psmc_hs_npt/short/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_psmc_hs_npt-short
clear_psmc_hs_npt-short: clean_psmc_hs_npt-short
	@if ls -d psmc_hs_npt/short 2>/dev/null 1>/dev/null; then \
	  if [ -e psmc_hs_npt/psmc_hs_npt-short.tgz ]; then \
	    echo "Package 'psmc_hs_npt-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'psmc_hs_npt/short/'"; \
	    cd psmc_hs_npt/short/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv psmc_hs_npt/short/; \
	  else \
	    echo "Package 'psmc_hs_npt-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'psmc_hs_npt-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_psmc_hs_npt-short
update_psmc_hs_npt-short:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'psmc_hs_npt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_hs_npt/short/'"; \
	if cd psmc_hs_npt/short/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f CONFIG.1 ] || [ ! -f CONFIG.2 ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ] || [ ! -f PSDATA.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###, PSDATA.000"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_psmc_hs_npt-short
pack_psmc_hs_npt-short:
	@echo ; \
	echo "Packing benchmark 'psmc_hs_npt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_hs_npt/'"; \
	if cd psmc_hs_npt/ ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf psmc_hs_npt-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_psmc_hs_npt-short
unpack_psmc_hs_npt-short:
	@echo ; \
	echo "Unpacking benchmark 'psmc_hs_npt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_hs_npt/'"; \
	if cd psmc_hs_npt/ ; then \
	  echo ; \
	  if [ -e psmc_hs_npt-short.tgz ]; then \
	    tar -xvf psmc_hs_npt-short.tgz ; \
	  else \
	    echo "Package 'psmc_hs_npt-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: psmc_lj_npt-short
psmc_lj_npt-short:
	@echo ;\
	echo "********************************************************************"; \
	echo " psmc_lj_npt-short "; \
	echo "********************************************************************"; \
	echo " in psmc_lj_npt/short/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd psmc_lj_npt/short/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PSDATA.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ] && [ -f "input/CONFIG.1" ] && [ -f "input/CONFIG.2" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PSDATA.000" ] && [ -f "PTFILE.000" ]; then \
	      for file in PSDATA.000 PTFILE.000; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'psmc_lj_npt-short'"; \
	    else \
		echo "*** test OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_psmc_lj_npt-short
clean_psmc_lj_npt-short:
	@echo ; \
	echo "WARNING: removing the data for test 'psmc_lj_npt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_lj_npt/short/'"; \
	if cd psmc_lj_npt/short/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_psmc_lj_npt-short
clear_psmc_lj_npt-short: clean_psmc_lj_npt-short
	@if ls -d psmc_lj_npt/short 2>/dev/null 1>/dev/null; then \
	  if [ -e psmc_lj_npt/psmc_lj_npt-short.tgz ]; then \
	    echo "Package 'psmc_lj_npt-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'psmc_lj_npt/short/'"; \
	    cd psmc_lj_npt/short/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv psmc_lj_npt/short/; \
	  else \
	    echo "Package 'psmc_lj_npt-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'psmc_lj_npt-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_psmc_lj_npt-short
update_psmc_lj_npt-short:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'psmc_lj_npt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_lj_npt/short/'"; \
	if cd psmc_lj_npt/short/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f CONFIG.1 ] || [ ! -f CONFIG.2 ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ] || [ ! -f PSDATA.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###, PSDATA.000"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_psmc_lj_npt-short
pack_psmc_lj_npt-short:
	@echo ; \
	echo "Packing benchmark 'psmc_lj_npt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_lj_npt/'"; \
	if cd psmc_lj_npt/ ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf psmc_lj_npt-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_psmc_lj_npt-short
unpack_psmc_lj_npt-short:
	@echo ; \
	echo "Unpacking benchmark 'psmc_lj_npt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_lj_npt/'"; \
	if cd psmc_lj_npt/ ; then \
	  echo ; \
	  if [ -e psmc_lj_npt-short.tgz ]; then \
	    tar -xvf psmc_lj_npt-short.tgz ; \
	  else \
	    echo "Package 'psmc_lj_npt-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: psmc_hd_nvt-short
psmc_hd_nvt-short:
	@echo ;\
	echo "********************************************************************"; \
	echo " psmc_hd_nvt-short "; \
	echo "********************************************************************"; \
	echo " in psmc_hd_nvt/short/"; \
	echo ;\
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd psmc_hd_nvt/short/; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PSDATA.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ] && [ -f "input/CONFIG.1" ] && [ -f "input/CONFIG.2" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    $(EXE); \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000";\
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PSDATA.000" ] && [ -f "PTFILE.000" ]; then \
	      for file in PSDATA.000 PTFILE.000; do \
		echo "*** Check $${file} =?= benchmark/$${file} ***" ; \
		echo "(should be empty):" ; \
		echo "--------------------------------------------------------------------"; \
		diff -q $${file} benchmark/$${file} || echo "*** non-empty diff - TEST FAILED ***" ; \
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Extract from OUTPUT.000 for 'U_acc' - energy consistency checks" ; \
		echo "(should not contain exponents > E-10):" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" OUTPUT.000 ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'psmc_hd_nvt-short'"; \
	    else \
		echo "*** test OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	    fi ;\
	  else \
	    echo "*** benchmark OUTPUT.000, PSDATA.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	  fi ; \
	  echo ;\
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ;\
	echo ;\

.PHONY: clean_psmc_hd_nvt-short
clean_psmc_hd_nvt-short:
	@echo ; \
	echo "WARNING: removing the data for test 'psmc_hd_nvt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_hd_nvt/short/'"; \
	if cd psmc_hd_nvt/short/; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: clear_psmc_hd_nvt-short
clear_psmc_hd_nvt-short: clean_psmc_hd_nvt-short
	@if ls -d psmc_hd_nvt/short 2>/dev/null 1>/dev/null; then \
	  if [ -e psmc_hd_nvt/psmc_hd_nvt-short.tgz ]; then \
	    echo "Package 'psmc_hd_nvt-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory 'psmc_hd_nvt/short/'"; \
	    cd psmc_hd_nvt/short/; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv psmc_hd_nvt/short/; \
	  else \
	    echo "Package 'psmc_hd_nvt-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'psmc_hd_nvt-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_psmc_hd_nvt-short
update_psmc_hd_nvt-short:
	@echo ; \
	echo "WARNING: replacing benchmark with the latest data for test 'psmc_hd_nvt-short'"; \
	echo ; \
	echo "Entering directory 'psmc_hd_nvt/short/'"; \
	if cd psmc_hd_nvt/short/; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f CONFIG.1 ] || [ ! -f CONFIG.2 ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ] || [ ! -f PSDATA.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###, PSDATA.000"; \
	    echo ; \
	    exit ; \
	  elif [ -f FEDDAT.000 ] \
	    && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi; \
	echo ; \

.PHONY: pack_psmc_hd_nvt-short
pack_psmc_hd_nvt-short:
	@echo ; \
	echo "Packing benchmark 'psmc_hd_nvt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_hd_nvt/'"; \
	if cd psmc_hd_nvt/ ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf psmc_hd_nvt-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_psmc_hd_nvt-short
unpack_psmc_hd_nvt-short:
	@echo ; \
	echo "Unpacking benchmark 'psmc_hd_nvt-short'" ; \
	echo ; \
	echo "Entering directory 'psmc_hd_nvt/'"; \
	if cd psmc_hd_nvt/ ; then \
	  echo ; \
	  if [ -e psmc_hd_nvt-short.tgz ]; then \
	    tar -xvf psmc_hd_nvt-short.tgz ; \
	  else \
	    echo "Package 'psmc_hd_nvt-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: psmc_sync_mol-main
psmc_sync_mol-main:
	@echo ; \
	export DIR="psmc_sync_mol/main"; \
	echo "********************************************************************"; \
	echo " psmc_sync_mol-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'psmc_sync_mol-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_psmc_sync_mol-main
clean_psmc_sync_mol-main:
	@echo ; \
	export DIR="psmc_sync_mol/main"; \
	echo "WARNING: removing the data for test 'psmc_sync_mol-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_psmc_sync_mol-main
clear_psmc_sync_mol-main: clean_psmc_sync_mol-main
	@echo ; \
	export DIR="psmc_sync_mol/main"; \
	if ls -d psmc_sync_mol/main 2>/dev/null 1>/dev/null; then \
	  if [ -e psmc_sync_mol/psmc_sync_mol-main.tgz ]; then \
	    echo "Package 'psmc_sync_mol-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'psmc_sync_mol-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'psmc_sync_mol-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_psmc_sync_mol-main
update_psmc_sync_mol-main:
	@echo ; \
	export DIR="psmc_sync_mol/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'psmc_sync_mol-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_psmc_sync_mol-main
pack_psmc_sync_mol-main:
	@echo ; \
	export DIR="psmc_sync_mol"; \
	echo "Packing benchmark 'psmc_sync_mol-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf psmc_sync_mol-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_psmc_sync_mol-main
unpack_psmc_sync_mol-main:
	@echo ; \
	export DIR="psmc_sync_mol"; \
	echo "Unpacking benchmark 'psmc_sync_mol-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e psmc_sync_mol-main.tgz ]; then \
	    tar -xvf psmc_sync_mol-main.tgz ; \
	  else \
	    echo "Package 'psmc_sync_mol-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: orient_water_tip4p2005-short
orient_water_tip4p2005-short:
	@echo ; \
	export DIR="orient_water_tip4p2005/short"; \
	echo "********************************************************************"; \
	echo " orient_water_tip4p2005-short "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'orient_water_tip4p2005-short'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_orient_water_tip4p2005-short
clean_orient_water_tip4p2005-short:
	@echo ; \
	export DIR="orient_water_tip4p2005/short"; \
	echo "WARNING: removing the data for test 'orient_water_tip4p2005-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_orient_water_tip4p2005-short
clear_orient_water_tip4p2005-short: clean_orient_water_tip4p2005-short
	@echo ; \
	export DIR="orient_water_tip4p2005/short"; \
	if ls -d orient_water_tip4p2005/short 2>/dev/null 1>/dev/null; then \
	  if [ -e orient_water_tip4p2005/orient_water_tip4p2005-short.tgz ]; then \
	    echo "Package 'orient_water_tip4p2005-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'orient_water_tip4p2005-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'orient_water_tip4p2005-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_orient_water_tip4p2005-short
update_orient_water_tip4p2005-short:
	@echo ; \
	export DIR="orient_water_tip4p2005/short"; \
	echo "WARNING: replacing benchmark with the latest data for test 'orient_water_tip4p2005-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_orient_water_tip4p2005-short
pack_orient_water_tip4p2005-short:
	@echo ; \
	export DIR="orient_water_tip4p2005"; \
	echo "Packing benchmark 'orient_water_tip4p2005-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf orient_water_tip4p2005-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_orient_water_tip4p2005-short
unpack_orient_water_tip4p2005-short:
	@echo ; \
	export DIR="orient_water_tip4p2005"; \
	echo "Unpacking benchmark 'orient_water_tip4p2005-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e orient_water_tip4p2005-short.tgz ]; then \
	    tar -xvf orient_water_tip4p2005-short.tgz ; \
	  else \
	    echo "Package 'orient_water_tip4p2005-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_sds_in_water-long-abh-2017nov-xn
npt_sds_in_water-long-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water/long-abh-2017nov-xn"; \
	echo "********************************************************************"; \
	echo " npt_sds_in_water-long-abh-2017nov-xn "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input/output ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input/output ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input/output ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/PSDATA.000" ]; then \
		echo "==> Checking diff(s) for PSDATA.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "PSDATA.000" ]; then \
	        for file in PSDATA.[0-9][0-9][0-9]*; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test PSDATA.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/REVCON.000" ]; then \
		echo "==> Checking diff(s) for REVCON.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "REVCON.000" ]; then \
	        for file in REVCON.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test REVCON.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_sds_in_water-long-abh-2017nov-xn'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input/output ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_npt_sds_in_water-long-abh-2017nov-xn
clean_npt_sds_in_water-long-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water/long-abh-2017nov-xn"; \
	echo "WARNING: removing the data for test 'npt_sds_in_water-long-abh-2017nov-xn'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_sds_in_water-long-abh-2017nov-xn
clear_npt_sds_in_water-long-abh-2017nov-xn: clean_npt_sds_in_water-long-abh-2017nov-xn
	@echo ; \
	export DIR="npt_sds_in_water/long-abh-2017nov-xn"; \
	if ls -d npt_sds_in_water/long-abh-2017nov-xn 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_sds_in_water/npt_sds_in_water-long-abh-2017nov-xn.tgz ]; then \
	    echo "Package 'npt_sds_in_water-long-abh-2017nov-xn.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'npt_sds_in_water-long-abh-2017nov-xn.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_sds_in_water-long-abh-2017nov-xn' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_sds_in_water-long-abh-2017nov-xn
update_npt_sds_in_water-long-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water/long-abh-2017nov-xn"; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_sds_in_water-long-abh-2017nov-xn'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_sds_in_water-long-abh-2017nov-xn
pack_npt_sds_in_water-long-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water"; \
	echo "Packing benchmark 'npt_sds_in_water-long-abh-2017nov-xn'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./long-abh-2017nov-xn/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_sds_in_water-long-abh-2017nov-xn.tgz ./long-abh-2017nov-xn/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_sds_in_water-long-abh-2017nov-xn
unpack_npt_sds_in_water-long-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water"; \
	echo "Unpacking benchmark 'npt_sds_in_water-long-abh-2017nov-xn'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e npt_sds_in_water-long-abh-2017nov-xn.tgz ]; then \
	    tar -xvf npt_sds_in_water-long-abh-2017nov-xn.tgz ; \
	  else \
	    echo "Package 'npt_sds_in_water-long-abh-2017nov-xn.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_sds_in_water-short-abh-2017nov-xn
npt_sds_in_water-short-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water/short-abh-2017nov-xn"; \
	echo "********************************************************************"; \
	echo " npt_sds_in_water-short-abh-2017nov-xn "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input/output ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input/output ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input/output ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/PSDATA.000" ]; then \
		echo "==> Checking diff(s) for PSDATA.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "PSDATA.000" ]; then \
	        for file in PSDATA.[0-9][0-9][0-9]*; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test PSDATA.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/REVCON.000" ]; then \
		echo "==> Checking diff(s) for REVCON.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "REVCON.000" ]; then \
	        for file in REVCON.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test REVCON.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_sds_in_water-short-abh-2017nov-xn'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input/output ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_npt_sds_in_water-short-abh-2017nov-xn
clean_npt_sds_in_water-short-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water/short-abh-2017nov-xn"; \
	echo "WARNING: removing the data for test 'npt_sds_in_water-short-abh-2017nov-xn'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_sds_in_water-short-abh-2017nov-xn
clear_npt_sds_in_water-short-abh-2017nov-xn: clean_npt_sds_in_water-short-abh-2017nov-xn
	@echo ; \
	export DIR="npt_sds_in_water/short-abh-2017nov-xn"; \
	if ls -d npt_sds_in_water/short-abh-2017nov-xn 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_sds_in_water/npt_sds_in_water-short-abh-2017nov-xn.tgz ]; then \
	    echo "Package 'npt_sds_in_water-short-abh-2017nov-xn.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'npt_sds_in_water-short-abh-2017nov-xn.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_sds_in_water-short-abh-2017nov-xn' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_sds_in_water-short-abh-2017nov-xn
update_npt_sds_in_water-short-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water/short-abh-2017nov-xn"; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_sds_in_water-short-abh-2017nov-xn'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_sds_in_water-short-abh-2017nov-xn
pack_npt_sds_in_water-short-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water"; \
	echo "Packing benchmark 'npt_sds_in_water-short-abh-2017nov-xn'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short-abh-2017nov-xn/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_sds_in_water-short-abh-2017nov-xn.tgz ./short-abh-2017nov-xn/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_sds_in_water-short-abh-2017nov-xn
unpack_npt_sds_in_water-short-abh-2017nov-xn:
	@echo ; \
	export DIR="npt_sds_in_water"; \
	echo "Unpacking benchmark 'npt_sds_in_water-short-abh-2017nov-xn'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e npt_sds_in_water-short-abh-2017nov-xn.tgz ]; then \
	    tar -xvf npt_sds_in_water-short-abh-2017nov-xn.tgz ; \
	  else \
	    echo "Package 'npt_sds_in_water-short-abh-2017nov-xn.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gcmc_lj_tm-short
gcmc_lj_tm-short:
	@echo ; \
	export DIR="gcmc_lj_tm/short"; \
	echo "********************************************************************"; \
	echo " gcmc_lj_tm-short "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input/output ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input/output ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input/output ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/PSDATA.000" ]; then \
		echo "==> Checking diff(s) for PSDATA.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "PSDATA.000" ]; then \
	        for file in PSDATA.[0-9][0-9][0-9]*; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test PSDATA.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/REVCON.000" ]; then \
		echo "==> Checking diff(s) for REVCON.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "REVCON.000" ]; then \
	        for file in REVCON.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test REVCON.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gcmc_lj_tm-short'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input/output ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gcmc_lj_tm-short
clean_gcmc_lj_tm-short:
	@echo ; \
	export DIR="gcmc_lj_tm/short"; \
	echo "WARNING: removing the data for test 'gcmc_lj_tm-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gcmc_lj_tm-short
clear_gcmc_lj_tm-short: clean_gcmc_lj_tm-short
	@echo ; \
	export DIR="gcmc_lj_tm/short"; \
	if ls -d gcmc_lj_tm/short 2>/dev/null 1>/dev/null; then \
	  if [ -e gcmc_lj_tm/gcmc_lj_tm-short.tgz ]; then \
	    echo "Package 'gcmc_lj_tm-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gcmc_lj_tm-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gcmc_lj_tm-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gcmc_lj_tm-short
update_gcmc_lj_tm-short:
	@echo ; \
	export DIR="gcmc_lj_tm/short"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gcmc_lj_tm-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gcmc_lj_tm-short
pack_gcmc_lj_tm-short:
	@echo ; \
	export DIR="gcmc_lj_tm"; \
	echo "Packing benchmark 'gcmc_lj_tm-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gcmc_lj_tm-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gcmc_lj_tm-short
unpack_gcmc_lj_tm-short:
	@echo ; \
	export DIR="gcmc_lj_tm"; \
	echo "Unpacking benchmark 'gcmc_lj_tm-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gcmc_lj_tm-short.tgz ]; then \
	    tar -xvf gcmc_lj_tm-short.tgz ; \
	  else \
	    echo "Package 'gcmc_lj_tm-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gibbs_lj_atom-short
gibbs_lj_atom-short:
	@echo ; \
	export DIR="gibbs_lj_atom/short"; \
	echo "********************************************************************"; \
	echo " gibbs_lj_atom-short "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input/output ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input/output ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input/output ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/PSDATA.000" ]; then \
		echo "==> Checking diff(s) for PSDATA.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "PSDATA.000" ]; then \
	        for file in PSDATA.[0-9][0-9][0-9]*; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test PSDATA.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/REVCON.000" ]; then \
		echo "==> Checking diff(s) for REVCON.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "REVCON.000" ]; then \
	        for file in REVCON.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test REVCON.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gibbs_lj_atom-short'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input/output ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gibbs_lj_atom-short
clean_gibbs_lj_atom-short:
	@echo ; \
	export DIR="gibbs_lj_atom/short"; \
	echo "WARNING: removing the data for test 'gibbs_lj_atom-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gibbs_lj_atom-short
clear_gibbs_lj_atom-short: clean_gibbs_lj_atom-short
	@echo ; \
	export DIR="gibbs_lj_atom/short"; \
	if ls -d gibbs_lj_atom/short 2>/dev/null 1>/dev/null; then \
	  if [ -e gibbs_lj_atom/gibbs_lj_atom-short.tgz ]; then \
	    echo "Package 'gibbs_lj_atom-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gibbs_lj_atom-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gibbs_lj_atom-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gibbs_lj_atom-short
update_gibbs_lj_atom-short:
	@echo ; \
	export DIR="gibbs_lj_atom/short"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gibbs_lj_atom-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gibbs_lj_atom-short
pack_gibbs_lj_atom-short:
	@echo ; \
	export DIR="gibbs_lj_atom"; \
	echo "Packing benchmark 'gibbs_lj_atom-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gibbs_lj_atom-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gibbs_lj_atom-short
unpack_gibbs_lj_atom-short:
	@echo ; \
	export DIR="gibbs_lj_atom"; \
	echo "Unpacking benchmark 'gibbs_lj_atom-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gibbs_lj_atom-short.tgz ]; then \
	    tar -xvf gibbs_lj_atom-short.tgz ; \
	  else \
	    echo "Package 'gibbs_lj_atom-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gibbs_lj_mol-short
gibbs_lj_mol-short:
	@echo ; \
	export DIR="gibbs_lj_mol/short"; \
	echo "********************************************************************"; \
	echo " gibbs_lj_mol-short "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input/output ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input/output ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input/output ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/PSDATA.000" ]; then \
		echo "==> Checking diff(s) for PSDATA.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "PSDATA.000" ]; then \
	        for file in PSDATA.[0-9][0-9][0-9]*; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test PSDATA.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/REVCON.000" ]; then \
		echo "==> Checking diff(s) for REVCON.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "REVCON.000" ]; then \
	        for file in REVCON.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test REVCON.000 not found - TEST FAILED - check input/output ***"; \
		  exit 4 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gibbs_lj_mol-short'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input/output ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gibbs_lj_mol-short
clean_gibbs_lj_mol-short:
	@echo ; \
	export DIR="gibbs_lj_mol/short"; \
	echo "WARNING: removing the data for test 'gibbs_lj_mol-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gibbs_lj_mol-short
clear_gibbs_lj_mol-short: clean_gibbs_lj_mol-short
	@echo ; \
	export DIR="gibbs_lj_mol/short"; \
	if ls -d gibbs_lj_mol/short 2>/dev/null 1>/dev/null; then \
	  if [ -e gibbs_lj_mol/gibbs_lj_mol-short.tgz ]; then \
	    echo "Package 'gibbs_lj_mol-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gibbs_lj_mol-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gibbs_lj_mol-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gibbs_lj_mol-short
update_gibbs_lj_mol-short:
	@echo ; \
	export DIR="gibbs_lj_mol/short"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gibbs_lj_mol-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gibbs_lj_mol-short
pack_gibbs_lj_mol-short:
	@echo ; \
	export DIR="gibbs_lj_mol"; \
	echo "Packing benchmark 'gibbs_lj_mol-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gibbs_lj_mol-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gibbs_lj_mol-short
unpack_gibbs_lj_mol-short:
	@echo ; \
	export DIR="gibbs_lj_mol"; \
	echo "Unpacking benchmark 'gibbs_lj_mol-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gibbs_lj_mol-short.tgz ]; then \
	    tar -xvf gibbs_lj_mol-short.tgz ; \
	  else \
	    echo "Package 'gibbs_lj_mol-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_useortho-main
ewald_nacl_atm_useortho-main:
	@echo ; \
	export DIR="ewald_nacl_atm_useortho/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_useortho-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_useortho-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_useortho-main
clean_ewald_nacl_atm_useortho-main:
	@echo ; \
	export DIR="ewald_nacl_atm_useortho/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_useortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_useortho-main
clear_ewald_nacl_atm_useortho-main: clean_ewald_nacl_atm_useortho-main
	@echo ; \
	export DIR="ewald_nacl_atm_useortho/main"; \
	if ls -d ewald_nacl_atm_useortho/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_useortho/ewald_nacl_atm_useortho-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_useortho-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_useortho-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_useortho-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_useortho-main
update_ewald_nacl_atm_useortho-main:
	@echo ; \
	export DIR="ewald_nacl_atm_useortho/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_useortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_useortho-main
pack_ewald_nacl_atm_useortho-main:
	@echo ; \
	export DIR="ewald_nacl_atm_useortho"; \
	echo "Packing benchmark 'ewald_nacl_atm_useortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_useortho-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_useortho-main
unpack_ewald_nacl_atm_useortho-main:
	@echo ; \
	export DIR="ewald_nacl_atm_useortho"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_useortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_useortho-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_useortho-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_useortho-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_xzy-main
ewald_nacl_atm_xzy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_xzy/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_xzy-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_xzy-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_xzy-main
clean_ewald_nacl_atm_xzy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_xzy/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_xzy-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_xzy-main
clear_ewald_nacl_atm_xzy-main: clean_ewald_nacl_atm_xzy-main
	@echo ; \
	export DIR="ewald_nacl_atm_xzy/main"; \
	if ls -d ewald_nacl_atm_xzy/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_xzy/ewald_nacl_atm_xzy-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_xzy-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_xzy-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_xzy-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_xzy-main
update_ewald_nacl_atm_xzy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_xzy/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_xzy-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_xzy-main
pack_ewald_nacl_atm_xzy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_xzy"; \
	echo "Packing benchmark 'ewald_nacl_atm_xzy-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_xzy-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_xzy-main
unpack_ewald_nacl_atm_xzy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_xzy"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_xzy-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_xzy-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_xzy-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_xzy-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_yxz-main
ewald_nacl_atm_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yxz/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_yxz-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_yxz-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_yxz-main
clean_ewald_nacl_atm_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yxz/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_yxz-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_yxz-main
clear_ewald_nacl_atm_yxz-main: clean_ewald_nacl_atm_yxz-main
	@echo ; \
	export DIR="ewald_nacl_atm_yxz/main"; \
	if ls -d ewald_nacl_atm_yxz/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_yxz/ewald_nacl_atm_yxz-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_yxz-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_yxz-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_yxz-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_yxz-main
update_ewald_nacl_atm_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yxz/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_yxz-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_yxz-main
pack_ewald_nacl_atm_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yxz"; \
	echo "Packing benchmark 'ewald_nacl_atm_yxz-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_yxz-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_yxz-main
unpack_ewald_nacl_atm_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yxz"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_yxz-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_yxz-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_yxz-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_yxz-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_yzx-main
ewald_nacl_atm_yzx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yzx/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_yzx-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_yzx-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_yzx-main
clean_ewald_nacl_atm_yzx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yzx/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_yzx-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_yzx-main
clear_ewald_nacl_atm_yzx-main: clean_ewald_nacl_atm_yzx-main
	@echo ; \
	export DIR="ewald_nacl_atm_yzx/main"; \
	if ls -d ewald_nacl_atm_yzx/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_yzx/ewald_nacl_atm_yzx-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_yzx-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_yzx-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_yzx-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_yzx-main
update_ewald_nacl_atm_yzx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yzx/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_yzx-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_yzx-main
pack_ewald_nacl_atm_yzx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yzx"; \
	echo "Packing benchmark 'ewald_nacl_atm_yzx-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_yzx-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_yzx-main
unpack_ewald_nacl_atm_yzx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_yzx"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_yzx-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_yzx-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_yzx-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_yzx-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_zxy-main
ewald_nacl_atm_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zxy/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_zxy-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_zxy-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_zxy-main
clean_ewald_nacl_atm_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zxy/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_zxy-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_zxy-main
clear_ewald_nacl_atm_zxy-main: clean_ewald_nacl_atm_zxy-main
	@echo ; \
	export DIR="ewald_nacl_atm_zxy/main"; \
	if ls -d ewald_nacl_atm_zxy/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_zxy/ewald_nacl_atm_zxy-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_zxy-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_zxy-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_zxy-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_zxy-main
update_ewald_nacl_atm_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zxy/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_zxy-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_zxy-main
pack_ewald_nacl_atm_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zxy"; \
	echo "Packing benchmark 'ewald_nacl_atm_zxy-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_zxy-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_zxy-main
unpack_ewald_nacl_atm_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zxy"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_zxy-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_zxy-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_zxy-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_zxy-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_zyx-main
ewald_nacl_atm_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zyx/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_zyx-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_zyx-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_zyx-main
clean_ewald_nacl_atm_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zyx/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_zyx-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_zyx-main
clear_ewald_nacl_atm_zyx-main: clean_ewald_nacl_atm_zyx-main
	@echo ; \
	export DIR="ewald_nacl_atm_zyx/main"; \
	if ls -d ewald_nacl_atm_zyx/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_zyx/ewald_nacl_atm_zyx-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_zyx-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_zyx-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_zyx-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_zyx-main
update_ewald_nacl_atm_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zyx/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_zyx-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_zyx-main
pack_ewald_nacl_atm_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zyx"; \
	echo "Packing benchmark 'ewald_nacl_atm_zyx-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_zyx-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_zyx-main
unpack_ewald_nacl_atm_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_zyx"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_zyx-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_zyx-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_zyx-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_zyx-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_fcc_yxz-main
ewald_nacl_atm_fcc_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_yxz/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_fcc_yxz-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_fcc_yxz-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_fcc_yxz-main
clean_ewald_nacl_atm_fcc_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_yxz/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_fcc_yxz-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_fcc_yxz-main
clear_ewald_nacl_atm_fcc_yxz-main: clean_ewald_nacl_atm_fcc_yxz-main
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_yxz/main"; \
	if ls -d ewald_nacl_atm_fcc_yxz/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_fcc_yxz/ewald_nacl_atm_fcc_yxz-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_fcc_yxz-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_fcc_yxz-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_fcc_yxz-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_fcc_yxz-main
update_ewald_nacl_atm_fcc_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_yxz/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_fcc_yxz-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_fcc_yxz-main
pack_ewald_nacl_atm_fcc_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_yxz"; \
	echo "Packing benchmark 'ewald_nacl_atm_fcc_yxz-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_fcc_yxz-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_fcc_yxz-main
unpack_ewald_nacl_atm_fcc_yxz-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_yxz"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_fcc_yxz-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_fcc_yxz-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_fcc_yxz-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_fcc_yxz-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_fcc_zxy-main
ewald_nacl_atm_fcc_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zxy/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_fcc_zxy-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_fcc_zxy-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_fcc_zxy-main
clean_ewald_nacl_atm_fcc_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zxy/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_fcc_zxy-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_fcc_zxy-main
clear_ewald_nacl_atm_fcc_zxy-main: clean_ewald_nacl_atm_fcc_zxy-main
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zxy/main"; \
	if ls -d ewald_nacl_atm_fcc_zxy/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_fcc_zxy/ewald_nacl_atm_fcc_zxy-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_fcc_zxy-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_fcc_zxy-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_fcc_zxy-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_fcc_zxy-main
update_ewald_nacl_atm_fcc_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zxy/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_fcc_zxy-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_fcc_zxy-main
pack_ewald_nacl_atm_fcc_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zxy"; \
	echo "Packing benchmark 'ewald_nacl_atm_fcc_zxy-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_fcc_zxy-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_fcc_zxy-main
unpack_ewald_nacl_atm_fcc_zxy-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zxy"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_fcc_zxy-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_fcc_zxy-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_fcc_zxy-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_fcc_zxy-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: ewald_nacl_atm_fcc_zyx-main
ewald_nacl_atm_fcc_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zyx/main"; \
	echo "********************************************************************"; \
	echo " ewald_nacl_atm_fcc_zyx-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'ewald_nacl_atm_fcc_zyx-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_ewald_nacl_atm_fcc_zyx-main
clean_ewald_nacl_atm_fcc_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zyx/main"; \
	echo "WARNING: removing the data for test 'ewald_nacl_atm_fcc_zyx-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_ewald_nacl_atm_fcc_zyx-main
clear_ewald_nacl_atm_fcc_zyx-main: clean_ewald_nacl_atm_fcc_zyx-main
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zyx/main"; \
	if ls -d ewald_nacl_atm_fcc_zyx/main 2>/dev/null 1>/dev/null; then \
	  if [ -e ewald_nacl_atm_fcc_zyx/ewald_nacl_atm_fcc_zyx-main.tgz ]; then \
	    echo "Package 'ewald_nacl_atm_fcc_zyx-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'ewald_nacl_atm_fcc_zyx-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'ewald_nacl_atm_fcc_zyx-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_ewald_nacl_atm_fcc_zyx-main
update_ewald_nacl_atm_fcc_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zyx/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'ewald_nacl_atm_fcc_zyx-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_ewald_nacl_atm_fcc_zyx-main
pack_ewald_nacl_atm_fcc_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zyx"; \
	echo "Packing benchmark 'ewald_nacl_atm_fcc_zyx-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf ewald_nacl_atm_fcc_zyx-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_ewald_nacl_atm_fcc_zyx-main
unpack_ewald_nacl_atm_fcc_zyx-main:
	@echo ; \
	export DIR="ewald_nacl_atm_fcc_zyx"; \
	echo "Unpacking benchmark 'ewald_nacl_atm_fcc_zyx-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e ewald_nacl_atm_fcc_zyx-main.tgz ]; then \
	    tar -xvf ewald_nacl_atm_fcc_zyx-main.tgz ; \
	  else \
	    echo "Package 'ewald_nacl_atm_fcc_zyx-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: sgcmc_ising_atm-short
sgcmc_ising_atm-short:
	@echo ; \
	export DIR="sgcmc_ising_atm/short"; \
	echo "********************************************************************"; \
	echo " sgcmc_ising_atm-short "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'sgcmc_ising_atm-short'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_sgcmc_ising_atm-short
clean_sgcmc_ising_atm-short:
	@echo ; \
	export DIR="sgcmc_ising_atm/short"; \
	echo "WARNING: removing the data for test 'sgcmc_ising_atm-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_sgcmc_ising_atm-short
clear_sgcmc_ising_atm-short: clean_sgcmc_ising_atm-short
	@echo ; \
	export DIR="sgcmc_ising_atm/short"; \
	if ls -d sgcmc_ising_atm/short 2>/dev/null 1>/dev/null; then \
	  if [ -e sgcmc_ising_atm/sgcmc_ising_atm-short.tgz ]; then \
	    echo "Package 'sgcmc_ising_atm-short.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'sgcmc_ising_atm-short.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'sgcmc_ising_atm-short' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_sgcmc_ising_atm-short
update_sgcmc_ising_atm-short:
	@echo ; \
	export DIR="sgcmc_ising_atm/short"; \
	echo "WARNING: replacing benchmark with the latest data for test 'sgcmc_ising_atm-short'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_sgcmc_ising_atm-short
pack_sgcmc_ising_atm-short:
	@echo ; \
	export DIR="sgcmc_ising_atm"; \
	echo "Packing benchmark 'sgcmc_ising_atm-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./short/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf sgcmc_ising_atm-short.tgz ./short/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_sgcmc_ising_atm-short
unpack_sgcmc_ising_atm-short:
	@echo ; \
	export DIR="sgcmc_ising_atm"; \
	echo "Unpacking benchmark 'sgcmc_ising_atm-short'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e sgcmc_ising_atm-short.tgz ]; then \
	    tar -xvf sgcmc_ising_atm-short.tgz ; \
	  else \
	    echo "Package 'sgcmc_ising_atm-short.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nbrlist_auto_nvt_spce_water-main
nbrlist_auto_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_nvt_spce_water/main"; \
	echo "********************************************************************"; \
	echo " nbrlist_auto_nvt_spce_water-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nbrlist_auto_nvt_spce_water-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nbrlist_auto_nvt_spce_water-main
clean_nbrlist_auto_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_nvt_spce_water/main"; \
	echo "WARNING: removing the data for test 'nbrlist_auto_nvt_spce_water-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nbrlist_auto_nvt_spce_water-main
clear_nbrlist_auto_nvt_spce_water-main: clean_nbrlist_auto_nvt_spce_water-main
	@echo ; \
	export DIR="nbrlist_auto_nvt_spce_water/main"; \
	if ls -d nbrlist_auto_nvt_spce_water/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nbrlist_auto_nvt_spce_water/nbrlist_auto_nvt_spce_water-main.tgz ]; then \
	    echo "Package 'nbrlist_auto_nvt_spce_water-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nbrlist_auto_nvt_spce_water-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nbrlist_auto_nvt_spce_water-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nbrlist_auto_nvt_spce_water-main
update_nbrlist_auto_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_nvt_spce_water/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nbrlist_auto_nvt_spce_water-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nbrlist_auto_nvt_spce_water-main
pack_nbrlist_auto_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_nvt_spce_water"; \
	echo "Packing benchmark 'nbrlist_auto_nvt_spce_water-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nbrlist_auto_nvt_spce_water-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nbrlist_auto_nvt_spce_water-main
unpack_nbrlist_auto_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_nvt_spce_water"; \
	echo "Unpacking benchmark 'nbrlist_auto_nvt_spce_water-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nbrlist_auto_nvt_spce_water-main.tgz ]; then \
	    tar -xvf nbrlist_auto_nvt_spce_water-main.tgz ; \
	  else \
	    echo "Package 'nbrlist_auto_nvt_spce_water-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nbrlist_auto_npt_spce_water-main
nbrlist_auto_npt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_npt_spce_water/main"; \
	echo "********************************************************************"; \
	echo " nbrlist_auto_npt_spce_water-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nbrlist_auto_npt_spce_water-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nbrlist_auto_npt_spce_water-main
clean_nbrlist_auto_npt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_npt_spce_water/main"; \
	echo "WARNING: removing the data for test 'nbrlist_auto_npt_spce_water-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nbrlist_auto_npt_spce_water-main
clear_nbrlist_auto_npt_spce_water-main: clean_nbrlist_auto_npt_spce_water-main
	@echo ; \
	export DIR="nbrlist_auto_npt_spce_water/main"; \
	if ls -d nbrlist_auto_npt_spce_water/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nbrlist_auto_npt_spce_water/nbrlist_auto_npt_spce_water-main.tgz ]; then \
	    echo "Package 'nbrlist_auto_npt_spce_water-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nbrlist_auto_npt_spce_water-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nbrlist_auto_npt_spce_water-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nbrlist_auto_npt_spce_water-main
update_nbrlist_auto_npt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_npt_spce_water/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nbrlist_auto_npt_spce_water-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nbrlist_auto_npt_spce_water-main
pack_nbrlist_auto_npt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_npt_spce_water"; \
	echo "Packing benchmark 'nbrlist_auto_npt_spce_water-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nbrlist_auto_npt_spce_water-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nbrlist_auto_npt_spce_water-main
unpack_nbrlist_auto_npt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_npt_spce_water"; \
	echo "Unpacking benchmark 'nbrlist_auto_npt_spce_water-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nbrlist_auto_npt_spce_water-main.tgz ]; then \
	    tar -xvf nbrlist_auto_npt_spce_water-main.tgz ; \
	  else \
	    echo "Package 'nbrlist_auto_npt_spce_water-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nbrlist_auto_psmc_nvt_spce_water-main
nbrlist_auto_psmc_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_psmc_nvt_spce_water/main"; \
	echo "********************************************************************"; \
	echo " nbrlist_auto_psmc_nvt_spce_water-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***" ; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nbrlist_auto_psmc_nvt_spce_water-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nbrlist_auto_psmc_nvt_spce_water-main
clean_nbrlist_auto_psmc_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_psmc_nvt_spce_water/main"; \
	echo "WARNING: removing the data for test 'nbrlist_auto_psmc_nvt_spce_water-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nbrlist_auto_psmc_nvt_spce_water-main
clear_nbrlist_auto_psmc_nvt_spce_water-main: clean_nbrlist_auto_psmc_nvt_spce_water-main
	@echo ; \
	export DIR="nbrlist_auto_psmc_nvt_spce_water/main"; \
	if ls -d nbrlist_auto_psmc_nvt_spce_water/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nbrlist_auto_psmc_nvt_spce_water/nbrlist_auto_psmc_nvt_spce_water-main.tgz ]; then \
	    echo "Package 'nbrlist_auto_psmc_nvt_spce_water-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nbrlist_auto_psmc_nvt_spce_water-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nbrlist_auto_psmc_nvt_spce_water-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nbrlist_auto_psmc_nvt_spce_water-main
update_nbrlist_auto_psmc_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_psmc_nvt_spce_water/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nbrlist_auto_psmc_nvt_spce_water-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nbrlist_auto_psmc_nvt_spce_water-main
pack_nbrlist_auto_psmc_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_psmc_nvt_spce_water"; \
	echo "Packing benchmark 'nbrlist_auto_psmc_nvt_spce_water-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nbrlist_auto_psmc_nvt_spce_water-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nbrlist_auto_psmc_nvt_spce_water-main
unpack_nbrlist_auto_psmc_nvt_spce_water-main:
	@echo ; \
	export DIR="nbrlist_auto_psmc_nvt_spce_water"; \
	echo "Unpacking benchmark 'nbrlist_auto_psmc_nvt_spce_water-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nbrlist_auto_psmc_nvt_spce_water-main.tgz ]; then \
	    tar -xvf nbrlist_auto_psmc_nvt_spce_water-main.tgz ; \
	  else \
	    echo "Package 'nbrlist_auto_psmc_nvt_spce_water-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: npt_lj_clist_ortho-main
npt_lj_clist_ortho-main:
	@echo ; \
	export DIR="npt_lj_clist_ortho/main"; \
	echo "********************************************************************"; \
	echo " npt_lj_clist_ortho-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'npt_lj_clist_ortho-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_npt_lj_clist_ortho-main
clean_npt_lj_clist_ortho-main:
	@echo ; \
	export DIR="npt_lj_clist_ortho/main"; \
	echo "WARNING: removing the data for test 'npt_lj_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_npt_lj_clist_ortho-main
clear_npt_lj_clist_ortho-main: clean_npt_lj_clist_ortho-main
	@echo ; \
	export DIR="npt_lj_clist_ortho/main"; \
	if ls -d npt_lj_clist_ortho/main 2>/dev/null 1>/dev/null; then \
	  if [ -e npt_lj_clist_ortho/npt_lj_clist_ortho-main.tgz ]; then \
	    echo "Package 'npt_lj_clist_ortho-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'npt_lj_clist_ortho-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'npt_lj_clist_ortho-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_npt_lj_clist_ortho-main
update_npt_lj_clist_ortho-main:
	@echo ; \
	export DIR="npt_lj_clist_ortho/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'npt_lj_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_npt_lj_clist_ortho-main
pack_npt_lj_clist_ortho-main:
	@echo ; \
	export DIR="npt_lj_clist_ortho"; \
	echo "Packing benchmark 'npt_lj_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf npt_lj_clist_ortho-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_npt_lj_clist_ortho-main
unpack_npt_lj_clist_ortho-main:
	@echo ; \
	export DIR="npt_lj_clist_ortho"; \
	echo "Unpacking benchmark 'npt_lj_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e npt_lj_clist_ortho-main.tgz ]; then \
	    tar -xvf npt_lj_clist_ortho-main.tgz ; \
	  else \
	    echo "Package 'npt_lj_clist_ortho-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gcmc_lj_clist_ortho-main
gcmc_lj_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_clist_ortho/main"; \
	echo "********************************************************************"; \
	echo " gcmc_lj_clist_ortho-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gcmc_lj_clist_ortho-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gcmc_lj_clist_ortho-main
clean_gcmc_lj_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_clist_ortho/main"; \
	echo "WARNING: removing the data for test 'gcmc_lj_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gcmc_lj_clist_ortho-main
clear_gcmc_lj_clist_ortho-main: clean_gcmc_lj_clist_ortho-main
	@echo ; \
	export DIR="gcmc_lj_clist_ortho/main"; \
	if ls -d gcmc_lj_clist_ortho/main 2>/dev/null 1>/dev/null; then \
	  if [ -e gcmc_lj_clist_ortho/gcmc_lj_clist_ortho-main.tgz ]; then \
	    echo "Package 'gcmc_lj_clist_ortho-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gcmc_lj_clist_ortho-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gcmc_lj_clist_ortho-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gcmc_lj_clist_ortho-main
update_gcmc_lj_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_clist_ortho/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gcmc_lj_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gcmc_lj_clist_ortho-main
pack_gcmc_lj_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_clist_ortho"; \
	echo "Packing benchmark 'gcmc_lj_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gcmc_lj_clist_ortho-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gcmc_lj_clist_ortho-main
unpack_gcmc_lj_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_clist_ortho"; \
	echo "Unpacking benchmark 'gcmc_lj_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gcmc_lj_clist_ortho-main.tgz ]; then \
	    tar -xvf gcmc_lj_clist_ortho-main.tgz ; \
	  else \
	    echo "Package 'gcmc_lj_clist_ortho-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gibbs_lj_atom_clist_ortho-main
gibbs_lj_atom_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_atom_clist_ortho/main"; \
	echo "********************************************************************"; \
	echo " gibbs_lj_atom_clist_ortho-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gibbs_lj_atom_clist_ortho-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gibbs_lj_atom_clist_ortho-main
clean_gibbs_lj_atom_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_atom_clist_ortho/main"; \
	echo "WARNING: removing the data for test 'gibbs_lj_atom_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gibbs_lj_atom_clist_ortho-main
clear_gibbs_lj_atom_clist_ortho-main: clean_gibbs_lj_atom_clist_ortho-main
	@echo ; \
	export DIR="gibbs_lj_atom_clist_ortho/main"; \
	if ls -d gibbs_lj_atom_clist_ortho/main 2>/dev/null 1>/dev/null; then \
	  if [ -e gibbs_lj_atom_clist_ortho/gibbs_lj_atom_clist_ortho-main.tgz ]; then \
	    echo "Package 'gibbs_lj_atom_clist_ortho-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gibbs_lj_atom_clist_ortho-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gibbs_lj_atom_clist_ortho-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gibbs_lj_atom_clist_ortho-main
update_gibbs_lj_atom_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_atom_clist_ortho/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gibbs_lj_atom_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gibbs_lj_atom_clist_ortho-main
pack_gibbs_lj_atom_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_atom_clist_ortho"; \
	echo "Packing benchmark 'gibbs_lj_atom_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gibbs_lj_atom_clist_ortho-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gibbs_lj_atom_clist_ortho-main
unpack_gibbs_lj_atom_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_atom_clist_ortho"; \
	echo "Unpacking benchmark 'gibbs_lj_atom_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gibbs_lj_atom_clist_ortho-main.tgz ]; then \
	    tar -xvf gibbs_lj_atom_clist_ortho-main.tgz ; \
	  else \
	    echo "Package 'gibbs_lj_atom_clist_ortho-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gibbs_lj_mol_clist_ortho-main
gibbs_lj_mol_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_mol_clist_ortho/main"; \
	echo "********************************************************************"; \
	echo " gibbs_lj_mol_clist_ortho-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gibbs_lj_mol_clist_ortho-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gibbs_lj_mol_clist_ortho-main
clean_gibbs_lj_mol_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_mol_clist_ortho/main"; \
	echo "WARNING: removing the data for test 'gibbs_lj_mol_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gibbs_lj_mol_clist_ortho-main
clear_gibbs_lj_mol_clist_ortho-main: clean_gibbs_lj_mol_clist_ortho-main
	@echo ; \
	export DIR="gibbs_lj_mol_clist_ortho/main"; \
	if ls -d gibbs_lj_mol_clist_ortho/main 2>/dev/null 1>/dev/null; then \
	  if [ -e gibbs_lj_mol_clist_ortho/gibbs_lj_mol_clist_ortho-main.tgz ]; then \
	    echo "Package 'gibbs_lj_mol_clist_ortho-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gibbs_lj_mol_clist_ortho-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gibbs_lj_mol_clist_ortho-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gibbs_lj_mol_clist_ortho-main
update_gibbs_lj_mol_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_mol_clist_ortho/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gibbs_lj_mol_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gibbs_lj_mol_clist_ortho-main
pack_gibbs_lj_mol_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_mol_clist_ortho"; \
	echo "Packing benchmark 'gibbs_lj_mol_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gibbs_lj_mol_clist_ortho-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gibbs_lj_mol_clist_ortho-main
unpack_gibbs_lj_mol_clist_ortho-main:
	@echo ; \
	export DIR="gibbs_lj_mol_clist_ortho"; \
	echo "Unpacking benchmark 'gibbs_lj_mol_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gibbs_lj_mol_clist_ortho-main.tgz ]; then \
	    tar -xvf gibbs_lj_mol_clist_ortho-main.tgz ; \
	  else \
	    echo "Package 'gibbs_lj_mol_clist_ortho-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: gcmc_lj_tm_clist_ortho-main
gcmc_lj_tm_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_tm_clist_ortho/main"; \
	echo "********************************************************************"; \
	echo " gcmc_lj_tm_clist_ortho-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'gcmc_lj_tm_clist_ortho-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_gcmc_lj_tm_clist_ortho-main
clean_gcmc_lj_tm_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_tm_clist_ortho/main"; \
	echo "WARNING: removing the data for test 'gcmc_lj_tm_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_gcmc_lj_tm_clist_ortho-main
clear_gcmc_lj_tm_clist_ortho-main: clean_gcmc_lj_tm_clist_ortho-main
	@echo ; \
	export DIR="gcmc_lj_tm_clist_ortho/main"; \
	if ls -d gcmc_lj_tm_clist_ortho/main 2>/dev/null 1>/dev/null; then \
	  if [ -e gcmc_lj_tm_clist_ortho/gcmc_lj_tm_clist_ortho-main.tgz ]; then \
	    echo "Package 'gcmc_lj_tm_clist_ortho-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'gcmc_lj_tm_clist_ortho-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'gcmc_lj_tm_clist_ortho-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_gcmc_lj_tm_clist_ortho-main
update_gcmc_lj_tm_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_tm_clist_ortho/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'gcmc_lj_tm_clist_ortho-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_gcmc_lj_tm_clist_ortho-main
pack_gcmc_lj_tm_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_tm_clist_ortho"; \
	echo "Packing benchmark 'gcmc_lj_tm_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf gcmc_lj_tm_clist_ortho-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_gcmc_lj_tm_clist_ortho-main
unpack_gcmc_lj_tm_clist_ortho-main:
	@echo ; \
	export DIR="gcmc_lj_tm_clist_ortho"; \
	echo "Unpacking benchmark 'gcmc_lj_tm_clist_ortho-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e gcmc_lj_tm_clist_ortho-main.tgz ]; then \
	    tar -xvf gcmc_lj_tm_clist_ortho-main.tgz ; \
	  else \
	    echo "Package 'gcmc_lj_tm_clist_ortho-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol-main
nvt_paracetamol-main:
	@echo ; \
	export DIR="nvt_paracetamol/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol-main
clean_nvt_paracetamol-main:
	@echo ; \
	export DIR="nvt_paracetamol/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol-main
clear_nvt_paracetamol-main: clean_nvt_paracetamol-main
	@echo ; \
	export DIR="nvt_paracetamol/main"; \
	if ls -d nvt_paracetamol/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol/nvt_paracetamol-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol-main
update_nvt_paracetamol-main:
	@echo ; \
	export DIR="nvt_paracetamol/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol-main
pack_nvt_paracetamol-main:
	@echo ; \
	export DIR="nvt_paracetamol"; \
	echo "Packing benchmark 'nvt_paracetamol-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol-main
unpack_nvt_paracetamol-main:
	@echo ; \
	export DIR="nvt_paracetamol"; \
	echo "Unpacking benchmark 'nvt_paracetamol-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol-main.tgz ]; then \
	    tar -xvf nvt_paracetamol-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: static_paracetamol-main
static_paracetamol-main:
	@echo ; \
	export DIR="static_paracetamol/main"; \
	echo "********************************************************************"; \
	echo " static_paracetamol-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'static_paracetamol-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_static_paracetamol-main
clean_static_paracetamol-main:
	@echo ; \
	export DIR="static_paracetamol/main"; \
	echo "WARNING: removing the data for test 'static_paracetamol-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_static_paracetamol-main
clear_static_paracetamol-main: clean_static_paracetamol-main
	@echo ; \
	export DIR="static_paracetamol/main"; \
	if ls -d static_paracetamol/main 2>/dev/null 1>/dev/null; then \
	  if [ -e static_paracetamol/static_paracetamol-main.tgz ]; then \
	    echo "Package 'static_paracetamol-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'static_paracetamol-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'static_paracetamol-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_static_paracetamol-main
update_static_paracetamol-main:
	@echo ; \
	export DIR="static_paracetamol/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'static_paracetamol-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_static_paracetamol-main
pack_static_paracetamol-main:
	@echo ; \
	export DIR="static_paracetamol"; \
	echo "Packing benchmark 'static_paracetamol-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf static_paracetamol-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_static_paracetamol-main
unpack_static_paracetamol-main:
	@echo ; \
	export DIR="static_paracetamol"; \
	echo "Unpacking benchmark 'static_paracetamol-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e static_paracetamol-main.tgz ]; then \
	    tar -xvf static_paracetamol-main.tgz ; \
	  else \
	    echo "Package 'static_paracetamol-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol_paratom_distewald-main
nvt_paracetamol_paratom_distewald-main:
	@echo ; \
	export DIR="nvt_paracetamol_paratom_distewald/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol_paratom_distewald-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol_paratom_distewald-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol_paratom_distewald-main
clean_nvt_paracetamol_paratom_distewald-main:
	@echo ; \
	export DIR="nvt_paracetamol_paratom_distewald/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol_paratom_distewald-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol_paratom_distewald-main
clear_nvt_paracetamol_paratom_distewald-main: clean_nvt_paracetamol_paratom_distewald-main
	@echo ; \
	export DIR="nvt_paracetamol_paratom_distewald/main"; \
	if ls -d nvt_paracetamol_paratom_distewald/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol_paratom_distewald/nvt_paracetamol_paratom_distewald-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol_paratom_distewald-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol_paratom_distewald-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol_paratom_distewald-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol_paratom_distewald-main
update_nvt_paracetamol_paratom_distewald-main:
	@echo ; \
	export DIR="nvt_paracetamol_paratom_distewald/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol_paratom_distewald-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol_paratom_distewald-main
pack_nvt_paracetamol_paratom_distewald-main:
	@echo ; \
	export DIR="nvt_paracetamol_paratom_distewald"; \
	echo "Packing benchmark 'nvt_paracetamol_paratom_distewald-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol_paratom_distewald-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol_paratom_distewald-main
unpack_nvt_paracetamol_paratom_distewald-main:
	@echo ; \
	export DIR="nvt_paracetamol_paratom_distewald"; \
	echo "Unpacking benchmark 'nvt_paracetamol_paratom_distewald-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol_paratom_distewald-main.tgz ]; then \
	    tar -xvf nvt_paracetamol_paratom_distewald-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol_paratom_distewald-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol_clist-main
nvt_paracetamol_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol_clist-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol_clist-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol_clist-main
clean_nvt_paracetamol_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol_clist-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol_clist-main
clear_nvt_paracetamol_clist-main: clean_nvt_paracetamol_clist-main
	@echo ; \
	export DIR="nvt_paracetamol_clist/main"; \
	if ls -d nvt_paracetamol_clist/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol_clist/nvt_paracetamol_clist-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol_clist-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol_clist-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol_clist-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol_clist-main
update_nvt_paracetamol_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol_clist-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol_clist-main
pack_nvt_paracetamol_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist"; \
	echo "Packing benchmark 'nvt_paracetamol_clist-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol_clist-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol_clist-main
unpack_nvt_paracetamol_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist"; \
	echo "Unpacking benchmark 'nvt_paracetamol_clist-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol_clist-main.tgz ]; then \
	    tar -xvf nvt_paracetamol_clist-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol_clist-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol_clist_paratom-main
nvt_paracetamol_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist_paratom/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol_clist_paratom-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol_clist_paratom-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol_clist_paratom-main
clean_nvt_paracetamol_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist_paratom/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol_clist_paratom-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol_clist_paratom-main
clear_nvt_paracetamol_clist_paratom-main: clean_nvt_paracetamol_clist_paratom-main
	@echo ; \
	export DIR="nvt_paracetamol_clist_paratom/main"; \
	if ls -d nvt_paracetamol_clist_paratom/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol_clist_paratom/nvt_paracetamol_clist_paratom-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol_clist_paratom-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol_clist_paratom-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol_clist_paratom-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol_clist_paratom-main
update_nvt_paracetamol_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist_paratom/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol_clist_paratom-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol_clist_paratom-main
pack_nvt_paracetamol_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist_paratom"; \
	echo "Packing benchmark 'nvt_paracetamol_clist_paratom-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol_clist_paratom-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol_clist_paratom-main
unpack_nvt_paracetamol_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_clist_paratom"; \
	echo "Unpacking benchmark 'nvt_paracetamol_clist_paratom-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol_clist_paratom-main.tgz ]; then \
	    tar -xvf nvt_paracetamol_clist_paratom-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol_clist_paratom-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol_nonbrlist-main
nvt_paracetamol_nonbrlist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol_nonbrlist-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol_nonbrlist-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol_nonbrlist-main
clean_nvt_paracetamol_nonbrlist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol_nonbrlist-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol_nonbrlist-main
clear_nvt_paracetamol_nonbrlist-main: clean_nvt_paracetamol_nonbrlist-main
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist/main"; \
	if ls -d nvt_paracetamol_nonbrlist/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol_nonbrlist/nvt_paracetamol_nonbrlist-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol_nonbrlist-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol_nonbrlist-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol_nonbrlist-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol_nonbrlist-main
update_nvt_paracetamol_nonbrlist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol_nonbrlist-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol_nonbrlist-main
pack_nvt_paracetamol_nonbrlist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist"; \
	echo "Packing benchmark 'nvt_paracetamol_nonbrlist-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol_nonbrlist-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol_nonbrlist-main
unpack_nvt_paracetamol_nonbrlist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist"; \
	echo "Unpacking benchmark 'nvt_paracetamol_nonbrlist-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol_nonbrlist-main.tgz ]; then \
	    tar -xvf nvt_paracetamol_nonbrlist-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol_nonbrlist-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol_nonbrlist_clist-main
nvt_paracetamol_nonbrlist_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol_nonbrlist_clist-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol_nonbrlist_clist-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol_nonbrlist_clist-main
clean_nvt_paracetamol_nonbrlist_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol_nonbrlist_clist-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol_nonbrlist_clist-main
clear_nvt_paracetamol_nonbrlist_clist-main: clean_nvt_paracetamol_nonbrlist_clist-main
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist/main"; \
	if ls -d nvt_paracetamol_nonbrlist_clist/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol_nonbrlist_clist/nvt_paracetamol_nonbrlist_clist-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol_nonbrlist_clist-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol_nonbrlist_clist-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol_nonbrlist_clist-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol_nonbrlist_clist-main
update_nvt_paracetamol_nonbrlist_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol_nonbrlist_clist-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol_nonbrlist_clist-main
pack_nvt_paracetamol_nonbrlist_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist"; \
	echo "Packing benchmark 'nvt_paracetamol_nonbrlist_clist-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol_nonbrlist_clist-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol_nonbrlist_clist-main
unpack_nvt_paracetamol_nonbrlist_clist-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist"; \
	echo "Unpacking benchmark 'nvt_paracetamol_nonbrlist_clist-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol_nonbrlist_clist-main.tgz ]; then \
	    tar -xvf nvt_paracetamol_nonbrlist_clist-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol_nonbrlist_clist-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_paracetamol_nonbrlist_clist_paratom-main
nvt_paracetamol_nonbrlist_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist_paratom/main"; \
	echo "********************************************************************"; \
	echo " nvt_paracetamol_nonbrlist_clist_paratom-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_paracetamol_nonbrlist_clist_paratom-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_paracetamol_nonbrlist_clist_paratom-main
clean_nvt_paracetamol_nonbrlist_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist_paratom/main"; \
	echo "WARNING: removing the data for test 'nvt_paracetamol_nonbrlist_clist_paratom-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_paracetamol_nonbrlist_clist_paratom-main
clear_nvt_paracetamol_nonbrlist_clist_paratom-main: clean_nvt_paracetamol_nonbrlist_clist_paratom-main
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist_paratom/main"; \
	if ls -d nvt_paracetamol_nonbrlist_clist_paratom/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_paracetamol_nonbrlist_clist_paratom/nvt_paracetamol_nonbrlist_clist_paratom-main.tgz ]; then \
	    echo "Package 'nvt_paracetamol_nonbrlist_clist_paratom-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_paracetamol_nonbrlist_clist_paratom-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_paracetamol_nonbrlist_clist_paratom-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_paracetamol_nonbrlist_clist_paratom-main
update_nvt_paracetamol_nonbrlist_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist_paratom/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_paracetamol_nonbrlist_clist_paratom-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_paracetamol_nonbrlist_clist_paratom-main
pack_nvt_paracetamol_nonbrlist_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist_paratom"; \
	echo "Packing benchmark 'nvt_paracetamol_nonbrlist_clist_paratom-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_paracetamol_nonbrlist_clist_paratom-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_paracetamol_nonbrlist_clist_paratom-main
unpack_nvt_paracetamol_nonbrlist_clist_paratom-main:
	@echo ; \
	export DIR="nvt_paracetamol_nonbrlist_clist_paratom"; \
	echo "Unpacking benchmark 'nvt_paracetamol_nonbrlist_clist_paratom-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_paracetamol_nonbrlist_clist_paratom-main.tgz ]; then \
	    tar -xvf nvt_paracetamol_nonbrlist_clist_paratom-main.tgz ; \
	  else \
	    echo "Package 'nvt_paracetamol_nonbrlist_clist_paratom-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \


.PHONY: nvt_stillingerweber_diamond-main
nvt_stillingerweber_diamond-main:
	@echo ; \
	export DIR="nvt_stillingerweber_diamond/main"; \
	echo "********************************************************************"; \
	echo " nvt_stillingerweber_diamond-main "; \
	echo "********************************************************************"; \
	echo " in $${DIR}"; \
	echo ; \
	export PATH="$$(pwd)/../bin:${PATH}"; \
	if cd "$${DIR}"; then \
	  if [ -f "benchmark/OUTPUT.000" ] && [ -f "benchmark/PTFILE.000" ]; then \
	    if [ -f "input/FIELD" ] && [ -f "input/CONFIG" ] && [ -f "input/CONTROL" ]; then \
	        cp -fv input/* ./ ; \
	    else \
		echo "*** benchmark INPUT files not found - TEST SKIPPED ***"; \
	        exit; \
	    fi ;\
	    echo ; \
	    echo "Running DLMONTE. Here are the error messages to the terminal:"; \
	    echo "--------------------------------------------------------------------"; \
	    echo "$(EXE)" ;\
	    "$(EXE)" ; \
	    echo "--------------------------------------------------------------------"; \
	    grep "normal exit" "OUTPUT.000"; \
	    echo "--------------------------------------------------------------------"; \
	    echo ; \
	    if [ -f "OUTPUT.000" ] && [ -f "PTFILE.000" ]; then \
		echo "==> Checking diff(s) for PTFILE.* (should be empty) <==" ; \
		echo ; \
	        for file in PTFILE.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	      if [ -f "benchmark/RDFDAT.000" ]; then \
		echo "==> Checking diff(s) for RDFDAT.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "RDFDAT.000" ]; then \
	          for file in RDFDAT.[0-9][0-9][0-9]; do \
		    diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	          done; \
		  echo ; \
	        else \
		  echo "*** test RDFDAT.000 not found - TEST FAILED - check input ***"; \
		  exit 1 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/ZDENSY.000" ]; then \
		echo "==> Checking diff(s) for ZDENSY.* (should be empty) <==" ; \
		echo ; \
	        if [ -f "ZDENSY.000" ]; then \
	        for file in ZDENSY.[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test ZDENSY.000 not found - TEST FAILED - check input ***"; \
		  exit 2 ; \
	        fi; \
	      fi; \
	      if [ -f "benchmark/FEDDAT.000_001" ]; then \
		echo "==> Checking diff(s) for FEDDAT.*_* (should be empty) <==" ; \
		echo ; \
	        if [ -f "FEDDAT.000_001" ]; then \
	        for file in FEDDAT.[0-9][0-9][0-9]_[0-9][0-9][0-9]; do \
		  diff -q "$${file}" "benchmark/$${file}" || echo "*** non-empty diff - TEST FAILED ***"; \
	        done; \
		echo ; \
	        else \
		  echo "*** test FEDDAT.000_001 not found - TEST FAILED - check input ***"; \
		  exit 3 ; \
	        fi; \
	      fi; \
	      for file in OUTPUT.[0-9][0-9][0-9]; do \
		echo "Rolling energy diff(s) in $${file} - watch out for exponents > E-10:" ; \
		echo "--------------------------------------------------------------------"; \
	        grep "U_acc" "$${file}" ;\
		echo "--------------------------------------------------------------------"; \
		echo ; \
	      done; \
		echo "Compare the CPU time spent (<) vs the benchmark (>):" ; \
		echo "--------------------------------------------------------------------"; \
		diff OUTPUT.000 benchmark/OUTPUT.000 | grep "total elapsed time";\
		echo "--------------------------------------------------------------------"; \
		echo ; \
		echo "COMPLETED TEST 'nvt_stillingerweber_diamond-main'"; \
	    else \
		echo "*** test OUTPUT.000 or PTFILE.000 not found - TEST FAILED - check input ***"; \
	        exit -2 ; \
	    fi; \
	  else \
	    echo "*** benchmark OUTPUT.000 or PTFILE.000 not found - TEST SKIPPED ***"; \
	    exit -1 ; \
	  fi ; \
	  echo ; \
	  echo "Returning"; \
	  cd - ; \
	fi ; \
	echo ; \
	echo ; \

.PHONY: clean_nvt_stillingerweber_diamond-main
clean_nvt_stillingerweber_diamond-main:
	@echo ; \
	export DIR="nvt_stillingerweber_diamond/main"; \
	echo "WARNING: removing the data for test 'nvt_stillingerweber_diamond-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if ls ./*.[0-9][0-9][0-9]* 2>/dev/null 1>/dev/null; then \
	    for file in *.[0-9][0-9][0-9]* ; do \
	      if [ -f "$$file" ]; then \
	        rm -fv $$file ; \
	      fi ; \
	    done ; \
	  else \
	    echo "Nothing to clean here - skipping"; \
	  fi; \
	  if [ -f FEDDAT.000 ]; then rm -fv FEDDAT.[0-9][0-9][0-9]; fi; \
	  if [ -f FIELD ];      then rm -fv FIELD; fi; \
	  if [ -f CONFIG ];     then rm -fv CONFIG*; fi; \
	  if [ -f CONTROL ];    then rm -fv CONTROL; fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: clear_nvt_stillingerweber_diamond-main
clear_nvt_stillingerweber_diamond-main: clean_nvt_stillingerweber_diamond-main
	@echo ; \
	export DIR="nvt_stillingerweber_diamond/main"; \
	if ls -d nvt_stillingerweber_diamond/main 2>/dev/null 1>/dev/null; then \
	  if [ -e nvt_stillingerweber_diamond/nvt_stillingerweber_diamond-main.tgz ]; then \
	    echo "Package 'nvt_stillingerweber_diamond-main.tgz' found - safe to remove all"; \
	    echo ; \
	    echo "Entering directory '$${DIR}'"; \
	    cd "$${DIR}"; \
	    echo ; \
	    rm -frv ./* ; \
	    echo ; \
	    echo "Returning"; \
	    cd -; \
	    rm -frv "$${DIR}"; \
	  else \
	    echo "Package 'nvt_stillingerweber_diamond-main.tgz' not found - first pack (or remove manually)"; \
	  fi; \
	else \
	    echo "Nothing to clear for test 'nvt_stillingerweber_diamond-main' - skipping"; \
	fi; \
	echo ; \

.PHONY: update_nvt_stillingerweber_diamond-main
update_nvt_stillingerweber_diamond-main:
	@echo ; \
	export DIR="nvt_stillingerweber_diamond/main"; \
	echo "WARNING: replacing benchmark with the latest data for test 'nvt_stillingerweber_diamond-main'"; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}"; then \
	  echo ; \
	  if [ ! -f CONTROL ] || [ ! -f CONFIG ] || [ ! -f FIELD ]; then \
	    echo "FAILURE: one of the input files not found! check for FIELD, CONFIG[.#], CONTROL"; \
	    echo ; \
	    exit 1 ; \
	  elif [ ! -f OUTPUT.000 ] || [ ! -f REVCON.000 ] || [ ! -f PTFILE.000 ]; then \
	    echo "FAILURE: one of the output files not found! check for OUTPUT.###, REVCON.###, PTFILE.###"; \
	    echo ; \
	    exit 2 ; \
	  elif [ -f FEDDAT.000 ] \
	  && ! ls ./FEDDAT.[0-9][0-9][0-9]_* 2>/dev/null 1>/dev/null; then \
	    echo "FAILURE: input file FEDDAT.000 is found but no output FEDDAT.###_### file(s)!"; \
	    echo ; \
	    exit 3 ; \
	  fi ; \
	  mv -fv CONTROL input/; \
	  mv -fv CONFIG* input/; \
	  mv -fv FIELD   input/; \
	  if [ -f FEDDAT.000 ]; then mv -fv FEDDAT.[0-9][0-9][0-9] input/; fi; \
	  for file in *.[0-9][0-9][0-9]* ; do \
	    if [ -f "$$file" ]; then \
	      mv -fv $$file benchmark/; \
	    fi ; \
	  done ; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: pack_nvt_stillingerweber_diamond-main
pack_nvt_stillingerweber_diamond-main:
	@echo ; \
	export DIR="nvt_stillingerweber_diamond"; \
	echo "Packing benchmark 'nvt_stillingerweber_diamond-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if ls ./main/* 2>/dev/null 1>/dev/null; then \
	    tar -cvzf nvt_stillingerweber_diamond-main.tgz ./main/* --remove-files ; \
	  else \
	    echo "Nothing to pack (packed already?) - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

.PHONY: unpack_nvt_stillingerweber_diamond-main
unpack_nvt_stillingerweber_diamond-main:
	@echo ; \
	export DIR="nvt_stillingerweber_diamond"; \
	echo "Unpacking benchmark 'nvt_stillingerweber_diamond-main'" ; \
	echo ; \
	echo "Entering directory '$${DIR}'"; \
	if cd "$${DIR}" ; then \
	  echo ; \
	  if [ -e nvt_stillingerweber_diamond-main.tgz ]; then \
	    tar -xvf nvt_stillingerweber_diamond-main.tgz ; \
	  else \
	    echo "Package 'nvt_stillingerweber_diamond-main.tgz' not found - skipping"; \
	  fi; \
	  echo ; \
	  echo "Returning"; \
	  cd -; \
	fi ; \
	echo ; \

