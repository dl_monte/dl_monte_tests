DL_MONTE test case - 'Auto' neighbour list for SPC/E water in NVT ensemble with PSMC


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

Here we test the neighbour list 'auto' functionality in DL_MONTE. A short simulation is performed using
neighbour lists, and the resulting trajectory is compared against that from an identical simulation which
does not use neighbour lists. The two trajectories should be identical - down to the last digit in the
floating point numbers. If the trajectories are not identical then, assuming that the trajectory obtained
without neighbour lists is correct, there may be a bug in the neighbour list functionality. 

The directory 'nolist' contains the DL_MONTE input and output files corresponding to the simulation with
no neighbour list.

Note that the simulations performed here should be long enough that any 'obvious' bugs in the neighbour
list functionality could be caught. Simulations which are very short may not catch any bugs;
the nature of the neighbour list functionality in DL_MONTE means that it is hard, perhaps
impossible, to engineer a test lasting, say, a few seconds which will catch bugs in the neighbour 
lists. To elaborate, the neighbour lists are always reset at the start of the simulation, and hence the 
simulation must be sufficiently long that atoms move far enough from their starting positions to 
trigger neighbour lists to be reset.


