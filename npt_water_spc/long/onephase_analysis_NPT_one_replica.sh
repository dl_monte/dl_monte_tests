
# Analyses data from YAMLDATA.000 file from a water NPT simulation, and calculates the density and mean energy
# - adding tail corrections.
#
# Argument 1: Number of particles
# Argument 2: equiltime (units of output frequency to YAMLDATA.000)
# Argument 3: blocksize (units of output frequency to YAMLDATA.000)

rm -f analysis_yaml.awk.tmp
rm -f replica_analysis.dat

cat > analysis_yaml.awk.tmp << _EOF_
BEGIN{ 

    # User input...

    # Constants for tail corrections for LJ part
    epsilon=0.650
    sigma=3.166
    rc=9.0
    T=298.15

    # Ensemble details
    N=$1 #
    beta=1.0/(0.0083144621*T) # 0.0083144621 is k_B in kJ/(mol K)
 
    equiltime=$2     # in units of output frequency to YAMLDATA.000
    blocksize=$3

    # For internal use...

    pi=atan2(0,-1)
    tailconst=8*pi*epsilon*sigma**3*((sigma/rc)**9/3-(sigma/rc)**3)/3


    # The number of phase 1 data points
    hist1=0 
    # The corrected count for phase 1 (after unfolding the bias)
    counts1=0 

    frameno = 0

    energysum1=0.0
    volsum1=0.0

    postequilframe = 0
}

# The part of the script for parsing the file
\$1=="timestamp:" {timestep=\$2}
\$1=="energy:" {energytot=\$2}
\$1=="volume:" {volume=\$2}

# What to do with data from each timestep (after equilibration period)
(NR-3)%8==0 && NR> 3 {
     
     frameno = frameno + 1

     if(frameno > equiltime){

        # Get the tail corrections and weight for the data point
        Etail = tailconst*N*N/volume
        weight = exp(-beta*Etail)
 
        # Update counts and sums - adding the tail corrections to the energy
        counts1=counts1+weight
        hist1=hist1+1
        energysum1=energysum1+weight*(energytot+Etail)
        volsum1=volsum1+volume*weight

        postequilframe++

     }

     if(postequilframe==blocksize){

         # Output the counts for each phase, the corrected counts, the free energy difference
         # ( \Delta F/(Nk_BT) ), the mean volume of each phase, and the mean energies for each phase
         # But only output if enough data points have been sampled in each phase
         vol = volsum1/(counts1*N)    # volume per particle - A^3
         density = 18.01528/(0.6022*vol)    # density gcm^-3 
         printf "%17.16e %17.16e %17.16e %17.16e\n", hist1, counts1, density, energysum1/(counts1*N)
 
         hist1=0 
         counts1=0 
         energysum1=0.0
         volsum1=0.0
         postequilframe = 0

     }

}

_EOF_



# Cycle over replicas to analyse (some may not be viable because the melted)
# Combine data from, e.g., run3/replica_1/PSDATA.000 and run3_contd/replica_1/PSDATA.000 into one
# data series
    
awk -f analysis_yaml.awk.tmp YAMLDATA.000 >> replica_analysis.dat 2> ${0}.err


# Calculate the mean and standard error of the various things from'replica_analysis.dat'
awk '{sum=sum+$3; sum2=sum2+$3*$3; count=count+1} END{mean=sum/count; if(count>1){var=(sum2/count-mean*mean)*count/(count-1); sterr=sqrt(var)/sqrt(count)}; printf "Density gcm^-3 (blocks,mean,stderr): %3d, %17.15e, %17.15e\n",count,mean,sterr}' replica_analysis.dat 2>> ${0}.err
awk '{sum=sum+$4; sum2=sum2+$4*$4; count=count+1} END{mean=sum/count; if(count>1){var=(sum2/count-mean*mean)*count/(count-1); sterr=sqrt(var)/sqrt(count)}; printf "Energy per molecule (blocks,mean,stderr): %3d, %17.15e, %17.15e\n",count,mean,sterr}' replica_analysis.dat 2>> ${0}.err

rm -f analysis_yaml.awk.tmp
#rm -f replica_analysis.dat

echo "Done"

