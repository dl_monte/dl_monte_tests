rm -f replica_analysis.dat

for i in `seq 1 64`; do

  echo "Analysing replica ${i}..."
  awk 'BEGIN{
         hist1=0;
         hist2=0;
         counts1=0;
         counts2=0;
       }; 
       NR>15000 {
         if($2==1){
           counts1=counts1+exp($5);
           hist1=hist1+1
         };
         if($2==2){
           counts2=counts2+exp($5);
           hist2=hist2+1}
         } 
       END{
         print hist1,hist2,counts1,counts2,-log(counts1/counts2)/(0.00001*216)
       }' replica_$i/PSDATA.000 >> replica_analysis.dat

done

# Calculate the mean and standard error of the 5th token (the free energy difference) in 
# 'replica_analysis.dat'
awk '{sum=sum+$5; sum2=sum2+$5*$5; count=count+1} END{mean=sum/count; var=(sum2/count-mean*mean)*count/(count-1); sterr=sqrt(var)/sqrt(count); print count,mean,sterr}' replica_analysis.dat

