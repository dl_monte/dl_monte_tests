DL_MONTE test case - PSMC simulation to calculate hcp-fcc free energy difference in NPT ensemble


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

This directory contains test cases for DL_MONTE in which the the free energy difference between
the hcp and fcc phases for the hard-sphere solid are calculated using phase-switch Monte Carlo
(PSMC) in the NPT ensemble. A 216-atom unit cell is used, at P*D^3/(k_B*T)=14.58, where D is the
hard-sphere diameter. Note that isotropic (for the orthorhombic cell) volume moves are used. One
analogous calculation has been performed in the literature, in S. Bridgwater & D. Quigley, 
Physical Review E 90, 063313 (2014). The "216 P=14.58 (iso)" row of Table 1 in that study gives
the result, and compares it to that of A. D. Bruce, A. N. Jackson, G. J. Ackland & 
N. B. Wilding, Physical Review E 61, 906 (2000). However A. D. Bruce et al. use anisotropic
moves. Furthermore the result A. D. Bruce et al. obtained is not that quoted in Table 1. Hence
I'm not sure if the "216 P=14.58 (iso)" row in Table 1 is correct. Thus no exact like-for-like
comparison with the literature is possible for this test. My own code ('monteswitch',
v0.9.9, available at https://github.com/tomlunderwood/monteswitch) yielded a free energy
difference of 123(6) (per particle, hcp relative to fcc, in units of 10^{-5}*k_B*T) for isotropic
moves. This will be the value I will compare DL_MONTE against. (Note that my own code yielded
117(9) for anisotropic moves, which compares well with the literature value of 113(4) obtained
by Bruce et al. using anisotropic moves).


PHILOSOPHY

Input files are provided for two simulations: a long simulation and a short one. The long
simulation gathers enough statistics to verify that DL_MONTE yields the correct free energy
difference. Actually, the long simulation is not enough in itself: results from many independent
long simulations must be combined to yield the free energy to a precision comparable to the
results in the literature (see below).

The long simulation takes a long time to run, and hence is unsuitable and excessive to use for 
the purposes of regression testing. For this reason there is also a short simulation. 
The short simulation is the same as the long simulation, except that it is of a much shorter 
length: it should take ~ seconds to run. The trajectory from this simulation will exactly 
match that of the start of the long simulation (assuming there are no really bad bugs present 
in the version of DL_MONTE under consideration). Hence the short simulation can be used to 
verify that the version of DL_MONTE under consideration would, if the long simulation were 
used, reproduce the required result, without actually performing the long simulation. With 
this in mind, the procedure for testing - starting 'from nothing', which will not be the 
case most of the time (see below) - is as follows:

1) Perform the long simulation for the version of DL_MONTE under consideration.
2) Assuming DL_MONTE passes the test, i.e., the long simulation produces the desired results, 
   perform the short simulation, and compare the output files for both simulations to confirm
   that the short simulation gives exactly the same trajectory as at the start of the long simulation.
3) If this is the case, then the short simulation is a suitable regression test, and the output
   files of the short simulation are the benchmark. After every change to DL_MONTE the short
   simulation should be performed, and the resulting output files should be compared with the
   benchmark output files. If they do not match then the recent change in DL_MONTE has broken something
   - the test is failed. 

Note that by 'match' I mean that the trajectories should match: differences to the output format 
are unimportant. However, for ease of comparision one would probably like to use 'diff' to 
compare the output files from the short simulation for the potentially 'unstable' version of 
DL_MONTE to the benchmark output files. This would necessitate making new benchmark output files 
every time DL_MONTE is altered such that the format of the output is changed for use in testing
future versions of DL_MONTE (until another change in the output format). 

Eventually a change in DL_MONTE will be made which expectedly results in a new trajectory compared
to the benchmark output files. I define this as a 'major' change. This could be, e.g., a change to 
how the hard-sphere potential is implemented. In this case the existing benchmark is unsuitable, 
and one must start again 'from nothing', i.e., perform the long simulation to ensure that the 
version of DL_MONTE under consideration is accurate, and then perform the short simulation to
generate output files which will act as the benchmark for future 'minor' changes, i.e., changes
which do not alter the trajectory. To summarise: the long simulation must be used as a test case
after all major changes, and the short simulation is suitable as a test case after all minor
changes. 


INCLUDED BENCHMARKS AND RESULTS

The directories 'long' and 'short' contain the imput files for the long and short simulations
respectively. The the benchmark files, to which the output of future versions of DL_MONTE should be 
compared to assess the version's fidelity, can be found in the directory 'short/benchmarks'.

We performed long simulations using a version of DL_MONTE from Nov 2017 - specifically 
a local version by AB before it was pushed to the master branch, similar to commit 
da9463dc9885da125f58153632595fbd999b21bb. A single long simulation is not enough to yield the 
free energy difference to a precision comparable to the results in the literature. I performed
64 long simulations and pooled the results to obtain a final free energy difference of 135(5),
which agrees with the literature. (Note that the uncertainty is the standard error of the mean).
The script used to launch the 64 simulations on the Bath HPC facility Balena is included in the 
'long' directory ('jobscript.slurm'), along with the input files. The time taken for all 
simulations to complete was 2 days 8 hours; for the node on Balena on which the simulation was 
run, all 16 cores were used until the simulations were complete. Hence the simulations correspond 
to 848 core-hours used. Running the script 'analysis.sh' in the directory in which the simulations 
were performed (i.e. where 'jobscript.slurm') was called analyses the data via block averaging 
and outputs the free energy difference and its uncertainty (phase 1 relative to 2, per particle, 
units of k_B*T*10^{-5}). Note that random seeds were used when running the simulations. The 
seeds realised were as follows:

replica_1: random number seeds (i,j,k,l) =     35   123    33    88 (for workgroup    0)
replica_2: random number seeds (i,j,k,l) =      2     3     6    55 (for workgroup    0)
replica_3: random number seeds (i,j,k,l) =    122   167    82     6 (for workgroup    0)
replica_4: random number seeds (i,j,k,l) =    126    87   104    19 (for workgroup    0)
replica_5: random number seeds (i,j,k,l) =     82    57    46   162 (for workgroup    0)
replica_6: random number seeds (i,j,k,l) =    137   121    23    75 (for workgroup    0)
replica_7: random number seeds (i,j,k,l) =     55   123     1   162 (for workgroup    0)
replica_8: random number seeds (i,j,k,l) =     62    45   120     9 (for workgroup    0)
replica_9: random number seeds (i,j,k,l) =      9    73   123   143 (for workgroup    0)
replica_10: random number seeds (i,j,k,l) =     74    63    34    48 (for workgroup    0)
replica_11: random number seeds (i,j,k,l) =    132    27     4   115 (for workgroup    0)
replica_12: random number seeds (i,j,k,l) =     13   157    83    32 (for workgroup    0)
replica_13: random number seeds (i,j,k,l) =    139   137   175   133 (for workgroup    0)
replica_14: random number seeds (i,j,k,l) =     49    39   131    52 (for workgroup    0)
replica_15: random number seeds (i,j,k,l) =    148    41    16   151 (for workgroup    0)
replica_16: random number seeds (i,j,k,l) =     22   107    40    70 (for workgroup    0)
replica_17: random number seeds (i,j,k,l) =      2     3     6   136 (for workgroup    0)
replica_18: random number seeds (i,j,k,l) =    105    63    29    25 (for workgroup    0)
replica_19: random number seeds (i,j,k,l) =     30   159   142   112 (for workgroup    0)
replica_20: random number seeds (i,j,k,l) =     51    59   161   115 (for workgroup    0)
replica_21: random number seeds (i,j,k,l) =    154    67   172   130 (for workgroup    0)
replica_22: random number seeds (i,j,k,l) =     85    21     5   151 (for workgroup    0)
replica_23: random number seeds (i,j,k,l) =     23   151    91   125 (for workgroup    0)
replica_24: random number seeds (i,j,k,l) =      4    13    52   133 (for workgroup    0)
replica_25: random number seeds (i,j,k,l) =    102   157   172    89 (for workgroup    0)
replica_26: random number seeds (i,j,k,l) =     17    95    13     4 (for workgroup    0)
replica_27: random number seeds (i,j,k,l) =    155    19    97   142 (for workgroup    0)
replica_28: random number seeds (i,j,k,l) =    162    95    82   158 (for workgroup    0)
replica_29: random number seeds (i,j,k,l) =    168   111   136    16 (for workgroup    0)
replica_30: random number seeds (i,j,k,l) =    102   157   172   103 (for workgroup    0)
replica_31: random number seeds (i,j,k,l) =    110    65    30   163 (for workgroup    0)
replica_32: random number seeds (i,j,k,l) =     30   159   142     4 (for workgroup    0)
replica_33: random number seeds (i,j,k,l) =    106    95   102    17 (for workgroup    0)
replica_34: random number seeds (i,j,k,l) =    165     5   113    40 (for workgroup    0)
replica_35: random number seeds (i,j,k,l) =     76     5    24   138 (for workgroup    0)
replica_36: random number seeds (i,j,k,l) =    163    63   123    56 (for workgroup    0)
replica_37: random number seeds (i,j,k,l) =     82    57    46   153 (for workgroup    0)
replica_38: random number seeds (i,j,k,l) =    106    95   102    17 (for workgroup    0)
replica_39: random number seeds (i,j,k,l) =     34    55    90   123 (for workgroup    0)
replica_40: random number seeds (i,j,k,l) =    156   151    60    76 (for workgroup    0)
replica_41: random number seeds (i,j,k,l) =    176     7   164   114 (for workgroup    0)
replica_42: random number seeds (i,j,k,l) =     20    25   144   145 (for workgroup    0)
replica_43: random number seeds (i,j,k,l) =    115   117   105    71 (for workgroup    0)
replica_44: random number seeds (i,j,k,l) =     45    23   145    10 (for workgroup    0)
replica_45: random number seeds (i,j,k,l) =     41    39   175    24 (for workgroup    0)
replica_46: random number seeds (i,j,k,l) =    100   111    64    92 (for workgroup    0)
replica_47: random number seeds (i,j,k,l) =     84    31   112    76 (for workgroup    0)
replica_48: random number seeds (i,j,k,l) =     73    95   171   101 (for workgroup    0)
replica_49: random number seeds (i,j,k,l) =    119   159    53    84 (for workgroup    0)
replica_50: random number seeds (i,j,k,l) =     23   151    91   130 (for workgroup    0)
replica_51: random number seeds (i,j,k,l) =    129   137    51    67 (for workgroup    0)
replica_52: random number seeds (i,j,k,l) =    157   107    67   104 (for workgroup    0)
replica_53: random number seeds (i,j,k,l) =     29   101    81     3 (for workgroup    0)
replica_54: random number seeds (i,j,k,l) =    120    41   114    85 (for workgroup    0)
replica_55: random number seeds (i,j,k,l) =     14     5    70   166 (for workgroup    0)
replica_56: random number seeds (i,j,k,l) =     70    25   148    62 (for workgroup    0)
replica_57: random number seeds (i,j,k,l) =     46   113    36    47 (for workgroup    0)
replica_58: random number seeds (i,j,k,l) =     27   169   113    46 (for workgroup    0)
replica_59: random number seeds (i,j,k,l) =    143    15     9   162 (for workgroup    0)
replica_60: random number seeds (i,j,k,l) =    104    33    50   132 (for workgroup    0)
replica_61: random number seeds (i,j,k,l) =    124   123   122     1 (for workgroup    0)
replica_62: random number seeds (i,j,k,l) =     81    73    39   127 (for workgroup    0)
replica_63: random number seeds (i,j,k,l) =     88     3    86   152 (for workgroup    0)
replica_64: random number seeds (i,j,k,l) =    137   121    23    59 (for workgroup    0)

The short test is essentially a very short version of the long simulation, but with the default seed,
and the benchmarks were generated using the executable corresponding to the aforementioned commit. 

