/////////////////////////////////////////////////////////////////////////
//                                                                     //
// DL_MONTE-2.0 test suite - experimental tools for manipulating tests //
//                                                                     //
// AUTHOR - Dr Andrey Brukhno (DL/STFC), Aug 2016 - Nov 2017           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


===================
UPDATING TEST-SUITE
-------------------

Although the test-suite has been updated regularly with new test cases 
and refreshed benchmarks (the last major update was made on 19 Feb 2018), 
it's been a while since we revised the directory structure and naming 
convention(s). As a matter of fact, there has been no consesnsus, nor 
a discussion, as to how to name new directories and/or targets in Makefile(s). 
As a result, due to the fact that at earlier stages Makefile's were created 
(and appended with additional targets) in yet non-standardised ways, but also 
the test creation tools were developed in an ad hoc manner, we have come now 
to a state where all the existing tests and benchmarks appear to be up to date 
but are contained in subdirectories with misleading outdated names and same  
is true for the targets in the main Makefile.

Therefore, it's time to upgrade both the tools used for test manipulation, 
and also the entire test-suite. This upgrade procedure should start with 
devising workable, convinient and sufficiently flexible conventions for the 
directory structure and target names, and should aim at standardising and 
automating the ways of manipulation (creating and updating) of test cases. 
Ideally, the test-building scripts and make-targets should be made sufficient 
so as to eliminate the need of manual editing or semi-manual manipulation 
(e.g. with 'sed') of the test-suite contents.

Meanwhile, I will be collecting new (as well as duplicate) test cases within
a subdirectory 'test-suite/upgrade'...


===================
DIRECTORY STRUCTURE
-------------------

In order to facilitate the creation and manipulation of DL_MONTE-2 tests 
data, and particularly to automate the process of benchmarking on different 
platforms, a script is provided for generating and extending Makefile(s), 
where test targets are based on two main templates (more can be added later). 

The following structure is assumed within the main test directory (./):

./ = <path_to_tests> - working directory for dealing with tests

./makefiles/   - storage for templates and scripts

./logs/        - storage for previous test reports, i.e. log-files

./<test_path>/<test_subdir>/input/       - test input  files storage
./<test_path>/<test_subdir>/benchmark/   - test output files storage

where 

<test_path>     - the main directory for a particular test case
<test_subdir>   - an subdirectory for a variant of the same test

NOTE: By default, <test_subdir> (and target in Makefile) would be 'main'
----- (see the workflow description below) 


====================
SCRIPT 'build-test.sh' - building directory tree and Makefile(s)
--------------------

The script accepts three parameters: <test_path> [<test_subdir> [<test_name>]]
only the first of which is compulsory, whereas the third should only be given
along with the second (but the second parameter can be used without the third).
Obviously, when only <test_path> is given, then the above NOTE is at operation.

The third parameter is only necessary when directory <test_path> includes one 
or more subdirectories, implying extra branching of the test directory tree.
(see 'fed_lj' cases for example)

The script will expect the test input files (CONTROL, CONFIG, FIELD and possibly 
FEDDAT.000) initially located in <test_path>. One can also copy here the output 
files if they have been created elsewhere, e.g. on a cluster. If a minimal set 
of output files (OUTPUT.000, PTFILE.000 and REVCON.000) is not found in <test_path>, 
the script will launch a DL_MONTE run by employing the executable specified by 
variable 'EXE' within the script - this creates the benchmark data. 

That is, one has to amend variable 'EXE' every time the benchmarking executable 
name is altered. It is, though, more convenient to update the file under the same name.
By default, EXE="$HOME/bin/DLMONTE-PRL-BENCH.X"

HINT: simply create a soft link to the desired executable:

ln -s  <PATH_TO_DLMONTE_EXECUTABLE> $HOME/bin/DLMONTE-PRL-BENCH.X

If the minimal set of output files has been found in <test_path> (e.g. after 
the benchmark run), the script will create 'input' and 'benchmark' subdirectories 
in <test_path>/<test_subdir>/.

Then, the input files used will be copied into '<test_path>/<test_subdir>/input/', 
and the output data will be *moved* into '<test_path>/<test_subdir>/benchmark/'.

If all of the above was done successfully, the last task the script performs is 
creating the corresponding target in Makefile named as 
(depending on the number of parameters given) 

'<test_path>-<test_subdir>' (two parameters were fed to the script)

or

'<test_name>-<test_subdir>' (three parameters were fed to the script)

Prior to altering Makefile its current copy is saved as Makefile.bak.

=============================
MAKEFILE concept & generation
-----------------------------

Update: 

Makefile-template-all is suitable for tests of any functionality up to now (incl. PSMC)

Details:

The initial Makefile is a copy of Makefile-header found in './makefiles/'

The targets are generated by copying a template called Makefile-template, of which 
there are (currently) two versions: Makefile-temp-gen and Makefile-temp-fed found 
in './makefiles/'. The former is the general template and the latter is the template 
to be used for test cases involving FED methods. The only difference between these 
is that the FED version relies on the contents of FEDDAT.000_003 file for doing 
the 'diff' comparison of the test and benchmark data, instead of PTFILE.000 that 
is used normally (in Makefile-temp-gen).

All the targets are templated within these two files, and the 'build-test.sh' script 
merely substitutes the correct target name and directory structure where necessary.

It is advisable, before using the script to check the contents of the two files:
Makefile (or Makefile-header), and Makefile-template (or Makefile-temp-gen vs 
Makefile-temp-fed), in order to make sure the correct versions are in place.

The current auto-generated targets are (extract from Makefile-header):

# Generic targets
.PHONY: tests (master target - all test targets are added to this one)
tests: \

# Cleaning targets (removing latest test data but keeping benchmarks)
.PHONY: clean
clean: \

# Clearing targets (first clean and then remove previously packed benchmarks)
.PHONY: clear
clear: \

# Updating targets (updating benchmarks with latest test data)
.PHONY: update
update: \

# Packing targets (packing non-empty target directories as <target-name>.tgz)
.PHONY: pack
pack: \

# Unpacking targets (unpacking target directories from existing <target-name>.tgz)
.PHONY: unpack
unpack: \

whereas extra targets may be handy, such as (extract from current master Makefile):

# Main tests - include only appropriate ('make main') - to be altered manually
.PHONY: main
main:  \

# Short tests - include only appropriate ('make short') - to be altered manually
.PHONY: short
short: \

# Short NPT tests - include only appropriate ('make npts') - created manually
.PHONY: npts
npts: \

# Short FED tests - include only appropriate ('make feds') - created manually
.PHONY: feds
feds: \

# Short FED tests - include only appropriate ('make slit') - created manually
.PHONY: slit
slit: \

====================
Two basic strategies - working with Makefile(s):
--------------------

1. Generating a separate Makefile for a specific set of test cases, which can 
be interlinked by, for example, the use of the same ensemble or associated in 
any other way. One possibility would be to gather benchmarking targets by the 
platform (CPU type or architecture or, more generally, system environment) where 
the benchmark runs are carried out.

2. Using and constantly adding new targets to the same (master) Makefile. 
This is how we used to deal with the tests, and perhaps we will be still doing so.
One can, of course, do both...

NOTE1: Currently both new and old target subdirectories are kept in 'tests-new',
------ so that we can use the old Makefile(s) by copying them from 'makefiles/'

NOTE2: Whatever strategy is employed with the new Makefile(s), the state to be 
------ pushed to the repository is when ALL target subdirectories are PACKED!
       Use 'make unpack' or 'make unpack_<name>' (see the target names).
       But, first of all, you will need to create your own benchmarks!

Happy testing! ;~)

