DL_MONTE test case - PSMC simulation of 2 identical phases to test Ewald summation


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

This directory contains a test which verifies that the Ewald summation is implemented correctly
in PSMC. The two phases here are identical, and each phase consists of two hard-sphere atoms with
opposite charges. Volume, atom translation and phase-switch moves are enabled. Given that the
phases are identical, the energies of both boxes in the simulation should always be identical.
Equivalently the value of the phase-switch order parameter should always be zero. Furthermore 
phase-switch moves should always be accepted - because the energy cost is always zero (and 
biasing is disabled for the test). Note that this test does not check that the aforementioned 
moves and the Ewald summation are correct, merely that the PSMC code implements the Ewald
summation code correctly.

