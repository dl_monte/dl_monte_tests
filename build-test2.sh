#!/bin/bash

##############################################
#                                            #
# Simple workaround for building DL_MONTE-2  #
# new tests & including them in Makefile(s)  #
#                                            #
# Author: Andrey Brukhno (c) August 2016     #
#                                            #
##############################################

#if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ]; then
if [ "$@" == "" ] || [ "$1" == "-h" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ]; then
cat <<EOF

////////////////////////////////////////////////////////////////////////
//                                                                    //
// DL_MONTE-2 test building script, version 2.02 (alpha)              //
// Author - Dr Andrey Brukhno (DL/STFC), August 2016                  //
//                                                                    //
////////////////////////////////////////////////////////////////////////

 - Generates a test case (input & benchmark) based on three arguments -

===========
Main usage: 
===========

./${0##*/} <path> [<subdir> [<name>]]

<path>   = main directory contatining the input (& possibly the output)
<subdir> = test subdirectory for 'input' & 'benchmark' to be placed in
<name>   = base name of the test, in case <path> is too long (optional)
           both <path> and <subdir> parameters must be present then

A target will be appended to Makefile: either 'path-subdir' or 'name-subdir'

At least three DL_MONTE-2 input files are to be found by <path>: CONTROL, CONFIG, FIELD [& FEDDAT.###]

NOTE1: <path> may include subdirectories in which case
       slashes '/' are replaced by underscores '_' in the target name
       however one can use <name> in order to shorten the target name 

NOTE2: DL_MONTE benchmark executable (path/name) is set within the script!
       Check & amend variable 'EXE' if needed!

==========
Otherwise:
==========

./${0##*/}
./${0##*/} -h
./${0##*/} --help

- output this help message

EOF
   exit 0
fi

# HINT: one can use older versions/executables for retrospective benchmarking

#EXE="DLMONTE-1.012c-PRL-VdW_dir-2016Apr03-ABhomePC.X"

#EXE="DLMONTE-201-PRL-VDW_dir-Mar2016-ABworkPC.X"
#EXE="DLMONTE-2.01-PRL_VDW_dir-2016Apr03-ABhomePC.X"
#EXE="DLMONTE-2.01-PRL-VDW_dir-2016Apr29-ABhomePC.X"
#EXE="DLMONTE-201-May2016-RP-PRL-VDW_dir.X"
#EXE="DLMONTE-202-June2016-PRL-VDW_dir.X"
#EXE="DLMONTE-2.03-PRL-VDW_dir-2017Feb08.X"

# HINT: simply create a soft link to the desired executable:

EXE="$HOME/bin/DLMONTE-PRL-BENCH.X"

#EXE="mpirun -n 4 $HOME/bin/DLMONTE-PRL-BENCH.X"

echo
echo "///////////////////////////////////////////////////////////"
echo "//                                                       //"
echo "// DL_MONTE-2 test building script, version 2.02 (alpha) //"
echo "// Author - Dr Andrey Brukhno (DL/STFC), August 2016     //"
echo "//                                                       //"
echo "///////////////////////////////////////////////////////////"
echo

#MHEAD="Makefile-header"
#MTEMP="Makefile-template"
if (( $# < 3 )) ; then

    echo
    echo "Error: too few arguments..."
    echo

    exit -1

fi

if [ ! -f "Makefile" ] ; then

  if [ -f "Makefile-header" ] ; then

    cp Makefile-header Makefile

  elif [ ! -f "Makefile-header" ] ; then

        echo
        echo "Neither Makefile nor Makefile-header found ..."
        echo "HINT: create & copy appropriate Makefile-header -> Makefile"
        echo

        exit 1

  fi 

fi

MTEMP="$1"; shift

if [ ! -f "${MTEMP}" ] ; then

        echo
        echo "Template file '${MTEMP}' not found ..."
        echo "HINT: create & copy appropriate template -> Makefile-template"
        echo

        exit 2

fi

if [ -d "$1" ] ; then 

    TARGET="$2"
    [[ ${TARGET} == "" ]] && TARGET="main"

    ROOT="$1"

    [[ ${ROOT: -1} == "/" ]] && ROOT="${ROOT: : -1}"

    NAME="${ROOT}"
    [[ $3 == "" ]] || NAME="$3"

    [[ ${NAME: -1} == "/" ]] && NAME="${NAME: : -1}"

    TNAME="${NAME}"
    [[ ${TARGET} == "" ]] || TNAME="${NAME}-${TARGET}"

    [[ ${TNAME: -1} == "/" ]] && TNAME="${TNAME: : -1}"

    TNAME="$(echo ${TNAME} | sed 's|\.\/||g' | sed 's|\/\/|_|g' | sed 's|\/|_|g')"

    #TNAME="$(echo "${TNAME}" | sed -r 's|\.\/||g')"
    #TNAME="$(echo "${TNAME}" | sed -r 's/\/+/_/g')"

    RETVAL=$(grep "${TNAME}" Makefile)

    if [[ $RETVAL == "" ]] ; then

      echo "Working on new test target '${TNAME}'"
      echo "to be stored in '${ROOT}/${TARGET}'"
      echo

      if ls "${ROOT}/${TARGET}"/* 2>/dev/null 1>/dev/null; then

        #echo "Found NON-EMPTY directory '${ROOT}/${TARGET}' - using the data for target '${TNAME}' ..."
        echo "Found NON-EMPTY directory '${ROOT}/${TARGET}' - quiting..."
        echo
        exit 0

      fi

    else

        echo "Found target '${TNAME}' in Makefile - quiting..."
        echo
        exit 0

    fi

    if [ ! -e "${ROOT}/FIELD" ] ; then
      echo "Could not find ${ROOT}/FIELD - quiting..."
      echo
      exit 0
    fi

    if [ ! -e "${ROOT}/CONFIG" ] ; then
      echo "Could not find ${ROOT}/CONFIG - quiting..."
      echo
      exit 0
    fi

    if [ ! -e "${ROOT}/CONTROL" ] ; then
      echo "Could not find ${ROOT}/CONTROL - quiting..."
      echo
      exit 0
    fi

    if [ -e "${ROOT}/OUTPUT.000" ] && [ -e "${ROOT}/PTFILE.000" ] && [ -e "${ROOT}/REVCON.000" ] ; then

      RETVAL=$(grep "normal exit" "${ROOT}/OUTPUT.000")

      if [[ $RETVAL == "" ]] ; then

        echo
        echo "Found an incomplete simulation data set for the new benchmark - quiting..."
        echo "HINT: remove the incomplete data set leaving only the input files & rerun!"
        echo
        exit 0

      fi

      echo "Found a complete simulation data set ready for the new benchmark - building..."
      echo

    else

      echo "Missing a complete simulation data set - rerunning the benchmark job for '${TNAME}' ..."
      echo
      echo "${EXE}" ;\
      echo

      cd "${ROOT}/"

      ${EXE}

      RETVAL=$?

      cd -

      if [ $RETVAL -ne 0 ] ; then

        echo
        echo "FAILED to complete the simulation job successfully - quiting..."
        echo "HINT: remove the incomplete data set leaving only the input files & rerun!"
        echo
        exit 0

      fi

      if [ ! -e "${ROOT}/OUTPUT.000" ] || [ ! -e "${ROOT}/PTFILE.000" ] || [ ! -e "${ROOT}/REVCON.000" ] ; then

        echo
        echo "FAILED to generate one of the simulation data files (OUTPUT/PTFILE/REVCON) - quiting..."
        echo "HINT: remove the incomplete data set leaving only the input files & rerun!"
        echo
        exit 0

      fi

      RETVAL=$(grep "normal exit" "${ROOT}/OUTPUT.000")

      if [[ $RETVAL == "" ]] ; then

        echo
        echo "WARNING: check if the simulation job exited normally - quiting..."
        echo
        exit 0

      fi

    fi

    if [ -f "${ROOT}/${TARGET}" ] ; then
      echo "Found '${ROOT}/${TARGET}' but it's a file (not a directory) - quiting..."
      echo
      exit 0
    fi

    #[[ -d "${ROOT}/${TARGET}" ]] || mkdir "${ROOT}/${TARGET}"
    if [ -d "${ROOT}/${TARGET}" ] ; then 

      if ls "${ROOT}/${TARGET}"/* 2>/dev/null 1>/dev/null; then

        echo "WARNING: directory '${ROOT}/${TARGET}' is NOT EMPTY - replacing its contents..."
        echo

      fi

    else

      mkdir "${ROOT}/${TARGET}"

    fi

    [[ -d "${ROOT}/${TARGET}/input" ]] || mkdir "${ROOT}/${TARGET}/input"

    cp -fv "${ROOT}"/CONFIG* "${ROOT}/${TARGET}/input/"
    cp -fv "${ROOT}/CONTROL" "${ROOT}/${TARGET}/input/"
    cp -fv "${ROOT}/FIELD"   "${ROOT}/${TARGET}/input/"

    [[ -f "${ROOT}/FEDDAT.000" ]] && cp -fv "${ROOT}"/FEDDAT.??? "${ROOT}/${TARGET}/input/"

    echo

    [[ -d "${ROOT}/${TARGET}/benchmark" ]] || mkdir "${ROOT}/${TARGET}/benchmark"

    mv -fv "${ROOT}"/*.[0-9][0-9][0-9]* "${ROOT}/${TARGET}/benchmark/"

    #[[ ${TARGET} == "" ]] || cp -fv "${ROOT}"/${TARGET}/input/* "${ROOT}/${TARGET}/"

    echo

    sed "s|PLACE|${ROOT}|g" "${MTEMP}" > "${MTEMP}1"
    sed "s|TEST-CASE|${TNAME}|g" "${MTEMP}1" > "${MTEMP}2"
    sed "s|CASE|${TARGET}|g" "${MTEMP}2" > "${MTEMP}1"
    sed "s|\/\/|\/|g" "${MTEMP}1" > "${MTEMP}2"

    sed '/^tests:/ s/$/'"\n        ${TNAME}"' \\/' Makefile > Makefile-new1
    sed '/^clean:/ s/$/'"\n  clean_${TNAME}"' \\/' Makefile-new1 > Makefile-new2
    sed '/^clear:/ s/$/'"\n  clear_${TNAME}"' \\/' Makefile-new2 > Makefile-new1
    sed '/^update:/ s/$/'"\n update_${TNAME}"' \\/' Makefile-new1 > Makefile-new2
    sed '/^pack:/ s/$/'"\n   pack_${TNAME}"' \\/' Makefile-new2 > Makefile-new1
    sed '/^unpack:/ s/$/'"\n unpack_${TNAME}"' \\/' Makefile-new1 > Makefile-new2

    [[ -f Makefile ]] && mv Makefile Makefile.bak

    cat Makefile-new2 "${MTEMP}2" > Makefile

    rm -f "${MTEMP}"? Makefile-new?

    echo
    echo "ALL DONE for new test target '${TNAME}' (in '${ROOT}/${TARGET}')"
    echo
    exit 0

else

    echo "Directory ${ROOT} not found - quiting..."
    echo
    exit 0

fi


