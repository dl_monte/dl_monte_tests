DL_MONTE test case - NVT simulation of paracetamol - 'no neighbour list', 'paratom' and 'clist' branches


AUTHOR

Tom L Underwood


GENERAL DESCRIPTION

This directory contains a test for an NVT simulation of paracetamol at 300K and a certain density. The
test is identical to the 'nvt_paracetamol' test case, except it utilises a different way of calculating
the energy - as described in the title of this test. The trajectory obtained using this way of calculating
the energy should be identical to that obtained using the 'nvt_paracetamol' default approach. The benchmark 
here reflects this fact. See 'nvt_paracetamol' for further details.

Note that the DL_MONTE exectutable used to generate all of the aforementioned output files was compiled
using the './build: SRL dir gfortran' command.

