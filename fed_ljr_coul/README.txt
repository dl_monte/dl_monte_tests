DL_MONTE-2.0 test case - FED for two ions interacting via repulsive LJ (~ r^-12) shoulder and Coulomb potentials

Approximate reproduction of long-range electrostatic attraction and short-range soft-core repulsion by potential 
of mean-force (PMF/POMF) between two oppositely charged particles, using free energy difference Wang-Landau method
in NVT simulation.


AUTHOR

Andrey V. Brukhno


PHILOSOPHY

Evaluating free energy difference(s) (FED) in Monte Carlo simulation is a powerful advanced simulation approach
that allows for obtaining crucial information about both the underlying free energy landscape(s) and thermodynamic 
behaviour(s) of the system under study. One particularly interesting FED profile is the Potential of Mean-Force (PMF 
or POMF), W(r) = -kT ln{g(r)}, where g(r) is a specifically renormalised pair correlation function, the so-called 
radial distribution function (RDF). Both PMF and RDF can be defined for a pair of interacting particles or groups 
thereof (in the latter case separation, r, is defined for the corresponding centers of mass, COM:s). 
Clearly, since W(r) tends to zero as the separation increases infinitely, PMF is essentially the work done upon 
bringing the interacting pair at a given finite separation r. 

For a complex system obtaining g(r), and generally W(r), in simulation is often hindered by poor sampling of regions 
in the configuration space that are high in energy and/or low in entropy (i.e. rarely sampled substates). FED MC schemes 
help to resolve, or at least relieve, this problem by introducing a "smart bias" in configuration or thermodynamic state 
sampling so that all relevant substates are sampled more or less uniformly. To this end, the perfect biasing potential 
along r would be -W(r), if it was known in advance (which is generally not the case). Therefore, normally a FED/PMF 
evaluation is started with some initial guess for the biasing potential (often merely setting it to zero in the entire 
reaction-coordinate range), which is then refined in the course of simulation by using some sort of automated feedback-
update scheme.


TEST DESCRIPTION

This test set is to validate the FED routines implemented in DL_MONTE-2.0 by evaluating W(r) for two ions in dielectric 
continuum (corresponding to water, epsilon_r = 78.8). That is, the setup with two ions in sufficiently large simulation 
box approximates the conditions of infinite dilution, under which W(r) -> u(r) as rho -> 0, allowing for numerically 
reproducing the underlying combination of potentials.

The general simulation setup is as follows:

Two structureless oppositely charged particles interacting via *repulsive* LJ (~ r^-12) and attractive Coulomb potentials,

u(r) / kT = epsilon (sigma/r)^12 + (q_1 q_2)/(r 4 pi epsilon_0 epsilon_r kT).

epsilon = 1.00 (reduced units; 1325601.636 kJ/mol at T = 300.0 K, see FIELD file)
sigma   = 3.00 Angstrom
r_cut   = 20.0 Angstrom (cutoff distance for truncated-and-shifted LJ)

q_1 = -q_2 = 1 electron charge
epsilon_r = 78.8

cubic V = L^3 
where L = 60.0 Angstrom 

The setup is such that initially the ions are separated by Bjerrum length 
r_B = (q_1 q_2)/(4 pi epsilon_0 epsilon_r kT) which equals 7.06853 Angstrom at T = 300 K.

Two identical test cases are provided with the same FIELD, CONFIG and CONTROL files in the following directories, 

R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWdir/
R-12_e1.0kT300K-d3.0A_Rcut20.0_eps78.8-VdWint/

the only difference being that the benchmarking data were produced with either directly calculated or interpolated 
(tabulated internally) r^-12 potnetial (see the endings of the directory names). 

In the first case (*VdWdir) the test results after 2 and 8 million MC cycles are provided in subdirectories 'short' and 'long', 
respectively. In the second case (*VdWint) the 'long' simulation data are only present. Although the output figures cannot 
coincide exactly between the two cases, the same FED data are obtained (which must be reproducable!). 

Note that the setup for the short and long simulation runs is identical except the number of MC cycles, so the obtained 
FED data are also the same, up to the third FED iteration.


PERFORMING TESTS

Test runs can be carried out manually on the command line from within each directory.
See the provided CONTROL files for the default length of test runs - either 2 or 16 million MC cycles;
and amend 'steps <frequency>' directive, if necessary.

Three short test case prepared for autoimatic running is specified, under mnemonic notation 'fed_ljr_coul-VdWdir', 
in Makefile found in the (outer) directory 'tests'.

In order to run the short test use the following command (from within 'tests' directory):

make fed_ljr_coul-VdWdir

In order to perform this test along with other FED tests use the following command:

make fed_lj-short

In order to run all short tests available:

make short

